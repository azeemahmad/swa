<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Welcome in Admin Panel
    </div>
    <!-- Default to the left -->
    <strong>Copyright © 2018 <a href="{{url('/')}}" target="_blank">SWA</a>.</strong> All rights reserved.
</footer>