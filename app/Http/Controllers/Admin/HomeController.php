<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Gufy\PdfToHtml\Pdf;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.index');

    }

    public function profile()
    {
        return view('admin.profile');
    }

    public function saveprofile(Request $request)
    {
        $user_list = Auth::user();

        if ($request['password'] == null && $request['password_confirmation'] == null && $request['old_password'] == null) {
            $this->validate($request, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,' . Auth::user()->id,
            ]);
        } else {
            if (!Hash::check($request['old_password'], $user_list->password)) {
                return redirect()->back()->with('error_password', 'Old Password not Match from Database!');
            }

            $this->validate($request, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,' . Auth::user()->id,
                'password' => 'required|string|min:6|confirmed|different:old_password',
                'old_password' => 'required|string|min:6'
            ]);
        }
        $data = $request->all();

        if ($request['password'] == null && $request['password_confirmation'] == null && $request['old_password'] == null) {
            if ($request->hasFile('image')) {
                $filename = $this->getFileName($request->image);
                $request->image->move(base_path('public/images/profile_image'), $filename);

                $user_list->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'image' => $filename
                ]);
            } else {
                $user_list->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                ]);
            }

        } else {

            if ($request->hasFile('image')) {
                $filename = $this->getFileName($request->image);
                $request->image->move(base_path('public/images/profile_image'), $filename);

                $user_list->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'image' => $filename
                ]);

            } else {
                $user_list->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password'])
                ]);
            }
        }
        return redirect('/admin/profile')->with('flash_message', 'Profile updated Successfully!');
    }
    protected function getFileName($file)
    {
        return str_random(32) . '.' . $file->extension();
    }


}
