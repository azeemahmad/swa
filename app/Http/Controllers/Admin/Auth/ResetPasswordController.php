<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';
    protected function guard()
    {
        return Auth::guard('web');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function showResetForm($id)
    {
        $client_id = base64_decode($id);

        $client = User::find($client_id);
        $email=$client->email;
        if($client){
            $user = DB::table('password_resets')->where('email', $email)->first();
            if($user) {
                return view('admin.auth.passwords.reset', compact('client', 'email','user'));
            }
            else{
                return redirect()->back()->with('status','Something Wrong !');
            }
        }
        else{
            return redirect()->back()->with('status','Something Wrong !');
        }

    }
    public function reset(Request $request,$id){

        $this->validate($request, [
            'email' => 'required|exists:users,email|exists:password_resets,email',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $data=$request->all();
        $client_id = base64_decode($id);
        $client = User::find($client_id);
        if($client) {
            $client->password = bcrypt($data['password']);
            $client->save();
            DB::delete('delete from password_resets where email = ?',[$client->email]);
            $this->guard()->login($client);
            return redirect($this->redirectTo);
        }
        else{
            return redirect()->back()->with('status','Something Wrong !');
        }

    }

//    public function showResetForm(Request $request, $token = null)
//    {
//        return view('admin.auth.password.reset')->with(
//            ['token' => $token, 'email' => $request->email]
//        );
//    }
}
