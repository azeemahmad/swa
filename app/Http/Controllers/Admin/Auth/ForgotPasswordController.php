<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\User;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function showLinkRequestForm(){
        return view('admin.auth.passwords.email');
    }

    public function sendResetLinkEmail(Request $request){
        $this->validate($request, [
            'email' => 'required|email',
        ]);

        $user = User::where('email',$request->email)->first();


        // dd($dataCheck);
        if($user) {
            $uderid = base64_encode($user->id);
            $data = array();
            $data['link'] = env('APP_URL').'/admin/password/reset/'.$uderid;
            $data['email'] = $user->email;
            $data['token']=$request['_token'];
            View::share('data', $data);
            //send email to user start here

            Mail::send('admin.auth.passwords.forgotpassword', $data, function ($message) use ($data) {

                $message->from('admin@fundsmap.com', 'SWA - Forgot Password');

                $message->to($data['email'])->subject('SWA Forgot Password.');


            });
            $user = DB::table('password_resets')->where('email', $data['email'])->first();
            if(!$user) {
                DB::table('password_resets')->insert(
                    ['email' => $data['email'],
                        'token' => $data['token'],
                        'created_at' => date('Y-m-d H:i:s')
                    ]
                );
            }
            return redirect()->back()->with('status','Please check your email and click the link !');
        }
        else{
            return redirect()->back()->with('status','Email does not Exit !');
        }
    }
    protected function resetNotifier()
    {
        //
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function broker()
    {
        return Password::broker('users');
    }
}
