<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\User;


class LoginController extends Controller
{
    //Trait
    use AuthenticatesUsers;
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    //protected $redirectTo = '/';

    //Custom guard for seller
    protected function guard()
    {
        return Auth::guard('web');
    }


    //Shows seller login form
    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    public function postlogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $remember_me = $request->has('remember') ? true : false;
        $admin = User::where('email', $request->email)->first();


            if (!$admin) {
                session()->flash('error_message', 'Email Id is not registered with us');
                return redirect('/admin/login')->with('failed_message','Invalid Email !');

            } else if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $remember_me)) {
                $this->guard()->login($admin);
                return redirect('/admin/home');
            } else {
                session()->flash('error_message', 'Incorrect Password');
                return redirect('/admin/login')->with('failed_message','Password wrong !');;

            }


    }

    public function logout(Request $request)
    {

        $this->guard()->logout();
        $request->session()->flush();
        return redirect('/admin/login')->with('flash_message', 'Logout Successfully !');
    }


}
