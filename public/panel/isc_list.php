<?php session_start();
//var_dump(!empty($_SESSION['panel']['adm_email']));die;
if(!empty($_SESSION['panel']['adm_email'])) {
  include("includes/db_config.php");
  ?>
  <!DOCTYPE html>
  <!--
  This is a starter template page. Use this page to start your new project from
  scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SWA Display Panel</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
  page. However, you can choose any other skin. Make sure you
  apply the skin class to the body tag so the changes take effect.
-->
<link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <?php include_once 'includes/header.php'; ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

      <!-- sidebar: style can be found in sidebar.less -->
      <?php include_once 'includes/sidebar.php'; ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Members Data
        </h1>
      </section>
      <?php
      $sql = "SELECT * FROM `isc_orders` ord
                LEFT JOIN `isc_order_details` dts ON dts.order_id = ord.id
                WHERE `order_status` = 'Success'
                ORDER BY dts.order_id DESC";
      // var_dump($sql);//die;
      $result = mysqli_query($db, $sql);
      ?>

      <!-- Main content -->
      <section class="content">
        <!-- Your Page Content Here -->
        <div class="row">
          <div class="col-md-12">
            <table class="table table-condensed display" id="myTable">
              <thead>
                <tr>
                  <th>Mem. No</th>
                  <th>Type</th>
                  <th>Name</th>
                  <th>Order id</th>
                  <th>Reg. Date</th>
                  <th>Amount</th>
                  <th>Mob. Num.</th>
                  <th>Email</th>
                  <th>CCAvenue. Reff. No</th>
                  <th>Institute Name</th>
                  <th>Student Proof</th>
                </tr>
              </thead>
              <tbody>
                <?php while ($row = mysqli_fetch_array($result)) {
                        if($row['user_type'] == 1)
                            $type = 'Member';
                        else if($row['user_type'] == 2)
                            $type = 'Non-Member';
                        else if($row['user_type'] == 3)
                            $type = 'Student';
                        else
                            $type = '';
                    ?>

                  <tr>
                    <td><?php echo $row['member_id']; ?></td>
                    <td><?php echo $type; ?></td>
                    <td><?php echo $row['billing_name']; ?></td>
                    <td><?php echo 'ISC_'.$row['order_id']; ?></td>
                    <td><?php echo date("d-m-Y", strtotime($row['created_at']) ); ?></td>
                    <td><?php echo $row['grand_total']; ?></td>
                    <td><?php echo $row['billing_tel']; ?></td>
                    <td><?php echo $row['billing_email']; ?></td>
                    <td><?php echo $row['tracking_id']; ?></td>
                    <td><?php echo $row['student_institute_name']; ?></td>
                    <?php if($row['student_proof'] != null && $row['student_proof'] != '' ) { ?>
                        <td><a href="http://swaindia.org/<?php echo $row['student_proof']; ?>" target="_blank">View</a></td>
                    <?php } else { ?>
                        <td>-</td>
                    <?php } ?>
                  </tr>
                <?php } ?>

              </tbody>
            </table>
          </div>
        </div>
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include_once 'includes/footer.php'; ?>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Create the tabs -->
    </aside><!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div><!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 2.1.4 -->
  <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
  <!-- Bootstrap 3.3.5 -->
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/app.min.js"></script>
  <script src="http://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>
  <script src="http://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
  <script src="http://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
  <script src="http://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="http://cdn.datatables.net/buttons/1.2.1/js/buttons.html5.min.js"></script>
  <script>
  $(document).ready( function () {


    $('#myTable').DataTable({
      dom: 'Bfrtip',
      buttons: ['excel'],
      "order": [[ 0, "desc" ]]
    });

  });
  </script>
</body>
</html>
<?php
}
else {
  header('Location: index.php');
}
?>
