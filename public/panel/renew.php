<?php session_start();
//var_dump(!empty($_SESSION['panel']['adm_email']));die;
if(!empty($_SESSION['panel']['adm_email'])) {
  include("includes/db_config.php");
  ?>
  <!DOCTYPE html>
  <!--
  This is a starter template page. Use this page to start your new project from
  scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SWA Display Panel</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
  page. However, you can choose any other skin. Make sure you
  apply the skin class to the body tag so the changes take effect.
-->
<link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <?php include_once 'includes/header.php'; ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

      <!-- sidebar: style can be found in sidebar.less -->
      <?php include_once 'includes/sidebar.php'; ?>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Members Data
        </h1>
      </section>
      <?php
      if (!empty($_POST))
      {
        $fromdata = $_POST['fromdate'];
        $todate = $_POST['todate'];


        $fromdata_db = date('Y-m-d', strtotime($fromdata));
        $todate_db = date('Y-m-d', strtotime($todate));

        $sql = "SELECT mem.reg_no,mem.name, usr.email, mem.mobileno, mem.add1,mem.add2,mem.city,mem.pin,mem.state , ty.type , mr.*
        FROM `fwa_users` usr
        INNER JOIN `fwa_members` mem ON mem.reg_no = usr.reg_no
        INNER JOIN `mem_type` ty ON  usr.type_id = ty.type_id
        INNER JOIN `members_renew_record` mr ON  mr.reg_no = mem.reg_no
        WHERE Date(mr.created_at) BETWEEN Date('".$fromdata_db."') AND Date('".$todate_db."')
        ORDER BY mr.created_at DESC";
      }
      else {
        $sql = "SELECT mem.reg_no,mem.name, usr.email, mem.mobileno, mem.add1,mem.add2,mem.city,mem.pin,mem.state , ty.type , mr.*
        FROM `fwa_users` usr
        INNER JOIN `fwa_members` mem ON mem.reg_no = usr.reg_no
        INNER JOIN `mem_type` ty ON  usr.type_id = ty.type_id
        INNER JOIN `members_renew_record` mr ON  mr.reg_no = mem.reg_no
        ORDER BY mr.created_at DESC";
      }
      // var_dump($sql);//die;
      $result = mysqli_query($db, $sql);
      ?>

      <!-- Main content -->
      <section class="content">
        <!-- Your Page Content Here -->
        <div class="row">
          <form action="renew.php" method="POST" onsubmit="return validate()">
          <div class="col-md-3">
            <p>From: <input type="text" name="fromdate" id="fromdate" size="30" value="<?php if(!empty($fromdata)) { echo $fromdata; } ?>" autocomplete="off"></p>
          </div>
          <div class="col-md-3">
            <p>To: <input type="text" name="todate" id="todate" size="30" value="<?php if(!empty($todate)) { echo $todate; } ?>" autocomplete="off"></p>
          </div>
          <div class="col-md-3">
            <input type="submit" value="Go">
            <input type="button" value="Reset" onclick="window.location.href='renew.php';">
          </div>

          <div class="col-md-3">
          </div>
        </form>
          <div class="col-md-12">
            <table class="table table-condensed display" id="myTable">
              <thead>
                <tr>
                  <th>Sr No</th>
                  <th>Membership Number</th>
                  <th>Name</th>
                  <th>Order id</th>
                  <th>Date Of Renewal</th>
                  <th>Renewal Period</th>
                  <th>Late Penalty Charges</th>
                  <th>New Card Charge</th>
                  <th>New Card Postage Charge</th>
                  <th>Grand Total</th>
                </tr>
              </thead>
              <tbody>
                <?php while ($row = mysqli_fetch_array($result)) { ?>

                  <tr>
                    <td><?php echo $row['id']; ?></td>
                    <td><?php echo $row['reg_no']; ?></td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['order_id']; ?></td>
                    <td><?php echo date("d-m-Y", strtotime($row['created_at']) ); ?></td>
                    <td><?php echo $row['renew_duration']; ?></td>
                    <td><?php echo $row['late_penalty_charge']; ?></td>
                    <td><?php echo $row['new_card_charge']; ?></td>
                    <td><?php echo $row['new_card_post_charge']; ?></td>
                    <td><?php echo $row['grand_total']; ?></td>
                  </tr>
                <?php } ?>

              </tbody>
            </table>
          </div>
        </div>
      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include_once 'includes/footer.php'; ?>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Create the tabs -->
    </aside><!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div><!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 2.1.4 -->
  <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
  <!-- Bootstrap 3.3.5 -->
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/app.min.js"></script>
  <script src="http://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js"></script>
  <script src="http://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
  <script src="http://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
  <script src="http://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="http://cdn.datatables.net/buttons/1.2.1/js/buttons.html5.min.js"></script>
  <script>
  $(document).ready( function () {

    $( "#todate,#fromdate").datepicker({
      dateFormat: "dd-mm-yy"
    });

    $('#myTable').DataTable({
      dom: 'Bfrtip',
      buttons: ['excel'],
      "order": [[ 0, "desc" ]]
    });

  });

  function validate(){
    var fromdate = $.trim($('#fromdate').val());
    var todate = $.trim($('#todate').val());

    var frm = fromdate.split("-");
    var frdt = new Date(frm[2], frm[1] - 1, frm[0]);

    var tod = todate.split("-");
    var todt = new Date(tod[2], tod[1] - 1, tod[0]);


    console.log();

    if(fromdate == '' || todate == '')
    {
      alert('from and to dates are required');
      return false;
    }
    else if(frdt.getTime() > todt.getTime())
    {
      alert('From date should be smaller than to date');
      return false;
    }
    else {
      return true;
    }

  }
  </script>
</body>
</html>
<?php
}
else {
  header('Location: index.php');
}
?>
