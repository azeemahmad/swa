<?php session_start();
      if(isset($_SESSION['is_login']) &&  $_SESSION['is_login'] == 'true')
      {
         header('Location: dashboard.php');
      }
      else{
?>
<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href=""/>
      <title>Sign Up</title>
      <link href="css/custom.css" rel="stylesheet" type="text/css">
      <link href="css/color.css" rel="stylesheet" type="text/css">
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="css/responsive.css" rel="stylesheet" type="text/css">
      <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
      <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
      <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style>
      .page404, .comming-soon {
    background: url(../images/loginbanner.jpg) no-repeat top center;
    background-size: cover;
    padding: 60px 0;
    width: 100%;
    float: left;
    text-align: center;
}
.cp-login-box, .cp-reg-box {
    margin: 0 auto;
    background: #fff;
    border-radius: 2px;
    border: 1px solid #eeeeee;
    text-align: left;
    padding: 50px;Become a SWA member,to know more follow this link
}
#reg_msg{
  font-size: 15px;
}
.foot-note{
  clear: both;
  padding: 30px 0 0 0;
  font-size: 11px;
}
      </style>
   </head>
   <body class="inner-page">
      <div id="wrapper">
         <?php include_once('header.php'); ?>
         <div id="cp-content-wrap" class="page404 cp-login-page">
            <div class="container">
               <div class="row">
                  <div class="col-md-6 col-md-offset-3 main-login-div">
                     <div class="cp-login-box sub-login-div">
                      <span id="reg_msg"></span>
                      <!-- <h4 class="text-center">Sign Up</h4> -->
                        <form action="javascript:void(0)" id="ref_frm">
                           <ul>
                              <li>
                                 <input type="text" id="reg_no" class="form-control" placeholder="*SWA Membership Number">
                              </li>
                              <!-- <li>
                                 <input type="text" id="name" class="form-control" placeholder="Name">
                              </li>
                              <li>
                                 <input type="text" id="email" class="form-control" placeholder="Email ID">
                              </li>
                              <li>
                                 <input type="text" id="dob" class="form-control" placeholder="Date of Birth">
                              </li> -->
                            <!--   <li>
                                 <input type="text" id="mobile" class="form-control" placeholder="Mobile No.">
                              </li> -->
                              <li class="cp-login-buttons">
                                 <button id="reg_btn" type="submit" style="background: #000;">Sign Up</button>
                                <!--  <a href="login.php" class="singup-btn">Sign In</a>  -->
                              </li>
                           </ul>
                        </form>
                        <form action="javascript:void(0)" class="hidden"  id="code_frm">
                           <ul>
                              <li>
                                 <input type="text" id="reg_no_sec" class="form-control" disabled placeholder="SWA Membership Number">
                              </li>
                              <li>
                                 <input type="text" id="securecode" class="form-control" placeholder="Secure Code">
                              </li>
                              <li class="cp-login-buttons">
                                 <button id="securecode_reg" type="submit" style="background: #000;">Proceed</button>
                                <!--  <a href="login.php" class="singup-btn">Sign In</a>  -->
                              </li>
                           </ul>
                        </form>
                        <div class="foot-note">
                           <span>*Learn how to become a SWA member, <a href="membership.php">here</a></span>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="col-md-6 main-login-div">
                     <div class="sub-login-div">
                        <ol class="rule-list">
                           <li>Login using your 6 digit FWA Registration No.(FWA Memebership No, The one that is given on your card ) In case the number is short of 6 digits please prefix zero(es) to make it 6 digit.(eg if your Reg No is 123 enter 000123)</li>
                           <li>If you have not registered your Email Id with the FWA then you will need to do that before you try to register your work. To register your Email, please visit <a href="javascript:void(0)">"Register Email ID"</a>.</li>
                           <li>Check your Email and note down your password. Login to the FWA site with this new password. We advise you change the password immediately after you login for the first time.</li>
                           <li>Once logged in you will be taken to Member’s Dashboard where you will find Registration and other Options.</li>
                        </ol>
                     </div>
                  </div> -->
               </div>
            </div>
         </div>
        <!--  <div id="cp-content-wrap" class="page404 cp-login-page">
            <div class="container">
               <div class="row">
                  <div class="col-md-12">

                  </div>
               </div>
            </div>
         </div>  -->
         <?php include_once('footer.php'); ?>
      </div>
      <script src="js/jquery-1.11.3.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <script src="js/jquery.bxslider.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/custom.js"></script>
      <script type="text/javascript">
         $( "#dob" ).datepicker({
            dateFormat: "dd-mm-yy",
            maxDate: new Date()
         });
      </script>
   </body>
</html>
<?php } ?>
