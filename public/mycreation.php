<?php session_start();
//echo "<pre>";print_r($_SESSION);die;
      if(isset($_SESSION['is_login']) &&  $_SESSION['is_login'] == 'true')
      {

        if($_SESSION['exp_flag'] == 1)
        {
          header('Location: dashboard.php');
        }
      include_once('includes/config.php');
         ?>

<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href=""/>
      <title>Film Writers Association | My Creations</title>
      <link href="css/custom.css" rel="stylesheet" type="text/css">
      <link href="css/color.css" rel="stylesheet" type="text/css">
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="css/responsive.css" rel="stylesheet" type="text/css">
      <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
      <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
      <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body class="inner-page">
      <div id="wrapper" class="inside-menu">
         <?php include_once('header.php'); ?>
         <div id="cp-content-wrap" class="page404 cp-login-page">
            <div class="container">
               <div class="row">
                  <div class="col-md-12 main-reg-div">
                     <div class="cp-reg-box sub-reg-div">
                       <!-- <h5 class="sub-hd">WELCOME  <?php //echo $_SESSION['name'];?></h5> -->


                  <?php
                  $sql = "SELECT distinct sr.ord_id, sr.date , sr.title, sr.co_authors,stype, tr.trackingid  FROM `scr_reg` sr
                          left join `s_tran` tr on tr.orderid = sr.ord_id
                          where sr.reg_no ='".$_SESSION['username']."'";
                      $result =  mysqli_query($db,$sql);
                      if(mysqli_num_rows($result)>0)
                      {


                      $i = 1;
                    echo "<table class='table table-hover'>
                            <thead>
                              <tr>
                              <th>Sr.No.</th>
                              <th>Script</th>
                              <th>Type</th>
                              <th>Date</th>
                              <th>Co Author(s)</th>
                              <th>Order Id</th>
                              <th>Reference No.<br>
                              (CC Avenue)
                              </th>
                              </tr>
                            </thead>
                          <tbody>";
                     while($row = mysqli_fetch_array($result))
                      {

                        echo "<tr>
                          <td>".$i."</td>
                          <td>".$row['title']."</td>
                          <td>".$row['stype']."</td>
                          <td>".date('d/m/Y',strtotime($row['date']))."</td>
                          <td>".$row['co_authors']."</td>
                          <td>".$row['ord_id']."</td>
                          <td>".$row['trackingid']."</td>
                        </tr>";
                         $i = $i+1;
                     }
                        echo "</tbody></table>";
                   }
                  else
                  {
                  echo "No registred script to display.......";
                  }

?>


                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include_once('footer.php'); ?>
      </div>
      <script src="js/jquery-1.11.3.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/jquery.bxslider.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/custom.js"></script>
   </body>
</html>
<?php }
   else{
      header('Location: login.php');
 } ?>
