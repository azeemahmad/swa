<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href=""/>
      <title> | FAQs</title>
      <link href="css/custom.css" rel="stylesheet" type="text/css">
      <link href="css/color.css" rel="stylesheet" type="text/css">
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="css/responsive.css" rel="stylesheet" type="text/css">
      <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
      <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
      <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style>
         /**
         * Tabs
         */
         .tabs {
         display: flex;
         flex-wrap: wrap; // make sure it wraps
         }
         .tabs label {
         order: 1; // Put the labels first
         display: block;
         padding: 1rem 2rem;
         margin-right: 0.2rem;
         cursor: pointer;
         background: #eff0f2;
         font-weight: normal;
         transition: background ease 0.2s;
         border-bottom: 2px solid #000;
         }
         .tabs .tab {
         order: 99; // Put the tabs last
         flex-grow: 1;
         width: 100%;
         display: none;
         padding: 1rem;
         background: #fff;
         }
         .tabs input[type="radio"] {
         display: none;
         }
         .tabs input[type="radio"]:checked + label {
         background: #000;
         color: #fff;
         border-bottom: 2px solid #000;
         }
         .tabs input[type="radio"]:checked + label + .tab {
         display: block;
         }
         @media (max-width: 45em) {
         .tabs .tab,
         .tabs label {
         order: initial;
         }
         .tabs label {
         width: 100%;
         margin-right: 0;
         margin-top: 0.2rem;
         }
         }
         .mem{margin-left: 35px;}
         /*  #filters {
         margin:1%;
         padding:0;
         list-style:none;
         }
         #filters li {
         float:left;
         }
         #filters li span {
         display: block;
         padding:5px 20px;
         text-decoration:none;
         color:#666;
         cursor: pointer;
         }
         #filters li span.active {
         background: #e95a44;
         color:#fff;
         }
         #portfoliolist .portfolio {
         -webkit-box-sizing: border-box;
         -moz-box-sizing: border-box;
         -o-box-sizing: border-box;
         width:23%;
         margin:1%;
         display:none;
         float:left;
         overflow:hidden;
         }
         .portfolio-wrapper {
         overflow:hidden;
         position: relative !important;
         background: #666;
         cursor:pointer;
         }
         .portfolio img {
         max-width:100%;
         position: relative;
         top:0;
         -webkit-transition: all 600ms cubic-bezier(0.645, 0.045, 0.355, 1);
         transition:         all 600ms cubic-bezier(0.645, 0.045, 0.355, 1);
         }
         .portfolio .label {
         position: absolute;
         width: 100%;
         height:40px;
         bottom:-40px;
         -webkit-transition: all 300ms cubic-bezier(0.645, 0.045, 0.355, 1);
         transition:         all 300ms cubic-bezier(0.645, 0.045, 0.355, 1);
         }
         .portfolio .label-bg {
         background: #e95a44;
         width: 100%;
         height:100%;
         position: absolute;
         top:0;
         left:0;
         }
         .portfolio .label-text {
         color:#fff;
         position: relative;
         z-index:500;
         padding:5px 8px;
         }
         .portfolio .text-category {
         display:block;
         font-size:9px;
         }
         .portfolio:hover .label {
         bottom:0;
         }
         .portfolio:hover img {
         top:-30px;
         }  */
      </style>
   </head>
   <body class="inner-page">
      <div id="wrapper">
         <?php include_once('header.php'); ?>
         <div id="cp-content-wrap">
            <div class="banner_inner">
               <img src="images/bg.jpg">
            </div>
            <div class="cp_our-story-section">
               <div class="container">
                  <div class="row">
                     <div class="col-md-12">
                        <h1 class="title bold text-center">
                           FAQs
                        </h1>
                        <!-- <div class="cp-acticle-box">
                           <p>Most of your Questions are answered here. So if you are facing a problem, please check here before writing to us. Chances are, you will get a solution faster than waiting for your reply.</p>
                           <h5><strong>This FAQ may appear too long to read, but you may jump straightway to your specific query for a quicker solution</strong></h5>
                              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                  <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                      <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <strong class="qa">Q.</strong>During registration when I go to payment option, I complete the payment formalities but after that nothing, even though my account is debited? /
                                        <strong class="qa">Q.</strong>I get some <strong>TCPDF</strong> Error during registration and my registration doesn’t go beyond that.
                                      </a>
                                    </h4>
                                  </div>
                                  <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                      <p><strong class="qa">A.</strong> If your money was deducted but script was not registered then please write to <a href="mailto:filmwritersassociation@gmail.com">filmwritersassociation@gmail.com</a> giving you transaction no and other details like script name & your membership number. We will cancel your transaction and money will be credited to your account.These problems are caused due to the pdf you uploaded for registration. The pdf converter you have used might have different compression ratio which is not compatible with our online system. This happens with most online free pdf convertors. Better is to use the inbuilt pdf converters that come with MSWord or Mac's Word official software. They have standard compression ratios and our site is designed for that. We have also observed that a few members recurrently keep getting this error because they use Free Scriptwriting Software (eg. Celtx) and PDF Saving facility provided by those software. A better method could be to save your script as a doc file and then open it in MS Word (or Mac Word) and save there as PDF</p>
                                    </div>
                                  </div>
                                </div>
                                <div class="panel panel-default">
                                  <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <strong class="qa">Q.</strong> General Precautions to avoid Errors. Always keep cleaning the cache memory and temporary files from your browser memory. Our Website works best with Internet Explorer 9 and above / <strong class="qa">Q.</strong> Problem with Apostrophes. I usually find registrations perfectly smooth, I follow all instructions properly and keep my browser clean. Yet now I am not able to register my script and I don’t even get any error message. It just sends me out of the system during transaction processing.
                                      </a>
                                    </h4>
                                  </div>
                                  <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                     <p><strong class="qa">A.</strong>Our System doesn’t recognize apostrophes in the Script Title. So if you are using apostrophe ‘s or % sign or special signs in the Script title only pl change that, nothing else. Delete the apostrophe and it should be fine.</p>
                                    </div>
                                  </div>
                                </div>
                                <div class="panel panel-default">
                                  <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                       <strong class="qa">Q.</strong> Members’ Dashboard Options. I don’t see all links on my dashboard after I login as a Member./ <strong class="qa">Q.</strong> All I see after login are Renew and Members’ Contribution Options.
                                      </a>
                                    </h4>
                                  </div>
                                  <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                     <p><strong class="qa">A.</strong>This is mostly because your membership must be due for renewal. If not so, (you may check your validity through date on membership card) then this is because of some technical issue which affects 1 in 30 cases. After a recent upgrade of the system to sync online and offline data, we have been experiencing this bug. If you are facing this problem kindly call/write to us at filmwritersassociation@gmail.com and intimate us. We will look into your problem and reset the validity. We are in the process of fixing this bug and hopefully it should be sorted out soon.</p>
                                    </div>
                                  </div>
                                </div>
                                <div class="panel panel-default">
                                  <div class="panel-heading" role="tab" id="headingFour">
                                    <h4 class="panel-title">
                                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                       <strong class="qa">Q.</strong>We are using other websites which are swifter and smoother than FWA Website. Why can’t FWA Website experience be the same?
                                      </a>
                                    </h4>
                                  </div>
                                  <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                    <div class="panel-body">
                                       <p><strong class="qa">A.</strong>FWA is a Trade Union, we have mounted and maintaining the website on Shared Resources. Due to these shared resources we do encounter some technical glitches. Nevertheless, our website is 99% accurate. There are not more than a couple of genuine complaints in a month. The rest of them are all addressed here in the FAQ. To increase the accuracy from 99% to 100% requires kind of infrastructure only banks and institutions can afford. It’s only rare probability that you will ever come in this 1% problem, in which case we are always there to help you and solve the problem by resetting/cleaning or personal guidance.</p>
                                    </div>
                                  </div>
                                </div>
                                <div class="panel panel-default">
                                  <div class="panel-heading" role="tab" id="headingFive">
                                    <h4 class="panel-title">
                                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                       <strong class="qa">Q.</strong> How much time FWA takes normally to solve a complaint?
                                      </a>
                                    </h4>
                                  </div>
                                  <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                    <div class="panel-body">
                                       <p><strong class="qa">A.</strong> FWA needs around 1-2 working days to solve a problem. If there are holidays in between you may have to wait that much longer. Because FWA office remains closed on bank holidays, we can’t address the problem during those days.</p>
                                    </div>
                                  </div>
                                </div>

                                <div class="panel panel-default">
                                  <div class="panel-heading" role="tab" id="headingSix">
                                    <h4 class="panel-title">
                                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                       <strong class="qa">Q.</strong> I don’t see all my registered scripts/stories when I login to my account? / <strong class="qa">Q.</strong> I see only some of my registered scripts /articles after I login to my account, whereas I have registered many more of them, Why?
                                      </a>
                                    </h4>
                                  </div>
                                  <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                    <div class="panel-body">
                                       <p><strong class="qa">A.</strong>Please note only those documents are seen in your members account which were registered online after July 2014. Documents registered online before this month don't show in the account and were taken down from website after repeated reminders and a year of notice on the website. Documents registered manually don't show in your online account. We are trying to upgrade the system and hopefully that should be in place sometimes next year. </p>
                                    </div>
                                  </div>
                                </div>

                                 <div class="panel panel-default">
                                  <div class="panel-heading" role="tab" id="headingSeven">
                                    <h4 class="panel-title">
                                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                       <strong class="qa">Q.</strong>I have forgotten to download the document after registration. Now how will I get the registered copy?
                                      </a>
                                    </h4>
                                  </div>
                                  <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                    <div class="panel-body">
                                       <p><strong class="qa">A.</strong>Please be careful next time. The Download Link doesn’t knock twice. But our system has a back-up in place and an email is sent to your registered email id. This email will contain the registered script with digital signature and certificate. Please store it carefully in multiple storages. Please note it may take a few minutes of a few hours for this Email to come to you so kindly bear with us. </p>
                                    </div>
                                  </div>
                                </div>

                                 <div class="panel panel-default">
                                  <div class="panel-heading" role="tab" id="headingEight">
                                    <h4 class="panel-title">
                                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                       <strong class="qa">Q.</strong>If your file got registered but you have not yet received the email, please wait sometimes the email takes longer to come. 1 hour is too short time to press panic button. Best is to download the file when the link comes immediately after the registration process is complete.


                                      </a>
                                    </h4>
                                  </div>
                                  <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                    <div class="panel-body">
                                       <p><strong class="qa">A.</strong>Please be careful next time. The Download Link doesn’t knock twice. But our system has a back-up in place and an email is sent to your registered email id. This email will contain the registered script with digital signature and certificate. Please store it carefully in multiple storages. Please note it may take a few minutes of a few hours for this Email to come to you so kindly bear with us. </p>
                                    </div>
                                  </div>
                                </div>

                                <div class="panel panel-default">
                                   <div class="panel-heading" role="tab" id="headingNine">
                                     <h4 class="panel-title">
                                       <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                       <strong class="qa">Q.</strong>I have forgotten to download the document after registration. Now how will I get the registered copy?
                                       </a>
                                     </h4>
                                   </div>
                                   <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                                     <div class="panel-body">
                                         <p><strong class="qa">A.</strong>Please be careful next time. The Download Link doesn’t knock twice. But our system has a back-up in place and an email is sent to your registered email id. This email will contain the registered script with digital signature and certificate. Please store it carefully in multiple storages. Please note it may take a few minutes of a few hours for this Email to come to you so kindly bear with us. </p>
                                     </div>
                                   </div>
                                 </div>

                                 <div class="panel panel-default">
                                   <div class="panel-heading" role="tab" id="headingTen">
                                     <h4 class="panel-title">
                                       <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                       <strong class="qa">Q.</strong>My registration went through successfully. But I haven’t yet received the Email with my Registered script.
                                       </a>
                                     </h4>
                                   </div>
                                   <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                                     <div class="panel-body">
                                         <p><strong class="qa">A.</strong>If your file got registered but you have not yet received the email, please wait sometimes the email takes longer to come. 1 hour is too short time to press panic button. Best is to download the file when the link comes immediately after the registration process is complete. </p>
                                     </div>
                                   </div>
                                   </div>

                                   <div class="panel panel-default">
                                   <div class="panel-heading" role="tab" id="headingEle">
                                     <h4 class="panel-title">
                                       <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEle" aria-expanded="false" aria-controls="collapseEle">
                                       <strong class="qa">Q.</strong>My file got registered by I didn’t see or miss it and cancelled the transaction by Writing to the FWA. Now what?
                                       </a>
                                     </h4>
                                   </div>
                                   <div id="collapseEle" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEle">
                                     <div class="panel-body">
                                         <p><strong class="qa">A.</strong>Before calling or writing to the FWA, please double check if your file was registered or not because refunding will cancel a successful registration. But in case you still come across a situation where you have demanded cancellation of a successful transaction by mistake, then quickly check with FWA, ideally by calling them to confirm if they have cancelled your transaction. If they haven’t then you can stop it. But if they have cancelled it already, you will need to re-register the script. Kindly note once the registration gets cancelled from our system, the hardcopy with digital stamp has no validity, because the entire system is digital, your document will be considered registered only if the digital signature on the soft copy validates with our system.</p>
                                     </div>
                                   </div>
                                   </div>

                                   <div class="panel panel-default">
                                   <div class="panel-heading" role="tab" id="headingTwl">
                                     <h4 class="panel-title">
                                       <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwl" aria-expanded="false" aria-controls="collapseTwl">
                                       <strong class="qa">Q.</strong>By mistake, I have sent a script to Members’ Contribution which I intended to register, now what?
                                       </a>
                                     </h4>
                                   </div>
                                   <div id="collapseTwl" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwl">
                                     <div class="panel-body">
                                         <p><strong class="qa">A.</strong>Don’t worry. We have mentioned on the Contribution Page about this and also write back to the contributor to reconfirm when we find someone has sent a professionally useful material as contribution. Though we take all care, you may send us an email as reply to the acknowledgement you receive for the contribution.Please note, if you intended to register it, you will need to do that by following the registration process only.</p>
                                     </div>
                                   </div>
                                   </div>

                                   <div class="panel panel-default">
                                   <div class="panel-heading" role="tab" id="headingThrt">
                                     <h4 class="panel-title">
                                       <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThrt" aria-expanded="false" aria-controls="collapseThrt">
                                       <strong class="qa">Q.</strong>I have been sending contributions but they don’t publish it, even when it is extremely good and original..out of the world kind.
                                       </a>
                                     </h4>
                                   </div>
                                   <div id="collapseThrt" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThrt">
                                     <div class="panel-body">
                                         <p><strong class="qa">A.</strong>Kindly follow the instructions as mentioned. Sometimes great contributions can’t be published because the senders don’t follow proper procedure. We can’t type out your entire pdf file to upload it on the site. To publish we need specific format. We have provided the editor or you can attach word files. Use only Standard fonts Mangal for Hindi/Marathi. Many other fonts we can’t even read.</p>
                                     </div>
                                   </div>
                                   </div>

                                   <div class="panel panel-default">
                                   <div class="panel-heading" role="tab" id="headingFrtn">
                                     <h4 class="panel-title">
                                       <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFrtn" aria-expanded="false" aria-controls="collapseFrtn">
                                       <strong class="qa">Q.</strong>I have very good script. How can FWA help me?
                                       </a>
                                     </h4>
                                   </div>
                                   <div id="collapseFrtn" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFrtn">
                                     <div class="panel-body">
                                         <p><strong class="qa">A.</strong>Kindly note FWA is a TRADE UNION and not a FILM PRODUCTION HOUSE. We can’t help our members find work. Sending contributions for the Website will give you exposure but don’t expect FWA or the Website Committee find work for you. One has to do his own struggle. FWA does help its members by conducting prestigious workshops and Pitching sessions. They are good opportunities to show your talent. Kindly follow the notifications on our website.</p>
                                     </div>
                                   </div>
                                   </div>

                                   <div class="panel panel-default">
                                   <div class="panel-heading" role="tab" id="headingFift">
                                     <h4 class="panel-title">
                                       <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFift" aria-expanded="false" aria-controls="collapseFift">
                                       <strong class="qa">Q.</strong>We have openings for Writers/ We are looking for Scripts. Can you help us find Writers / Scripts?
                                       </a>
                                     </h4>
                                   </div>
                                   <div id="collapseFift" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFift">
                                     <div class="panel-body">
                                         <p><strong class="qa">A.</strong>Thanks a lot for sharing your opportunity with our members. You may opt to advertise with us on our website. For details pl write to <a href ="mailto:filmwritersassociation@gmail.com">filmwritersassociation@gmail.com</a> <br>Alternatively, if you are a member of any Producers’ Association, we can share your lead on our FB page.</p>
                                     </div>
                                   </div>
                                   </div>


                              </div>
                           </div> -->
                        <div class="tabs">
                           <input type="radio" name="tabs" id="tabone" checked="checked">
                           <label for="tabone">SWA</label>
                           <div class="tab">
                              <div class="cp-acticle-box">
                                 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="headingOne">
                                          <h4 class="panel-title">
                                             <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                             <strong class="qa">Q.</strong>&nbsp;I HAVE A VERY GOOD SCRIPT. HOW CAN SWA HELP ME?
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                          <div class="panel-body">
                                             <p><strong class="qa">A.</strong>SWA is a trade union and not a film production company or employment agency. We can’t help our members find work. However, we do help our members with opportunities of networking and exposure with workshops, pitching sessions, conferences etc.</p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="headingTwo">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                             <strong class="qa">Q.</strong>&nbsp;I'M A PRODUCER LOOKING FOR WRITERS OR SCRIPTS. CAN SWA HELP ME?
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                          <div class="panel-body">
                                             <p><strong class="qa">A.</strong>SWA is a trade union and not an employment agency. We neither differentiate among our members on the basis of skills/talent nor do we read/judge their scripts. Thus, we can’t help you find a writer or scripts.However, you may opt to advertise with us on our website provided you’re a member of one of the Producers' Association. Write to <a href="mailto:web.admin@swa-india.org">web.admin@swa-india.org</a></p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="headingThree">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                             <strong class="qa">Q.</strong>&nbsp;I’M A SWA MEMBER AND I WANT CONTACT DETAILS OF ANOTHER SWA MEMBER. CAN SWA SHARE THE SAME?
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                          <div class="panel-body">
                                             <p><strong class="qa">A.</strong>SWA does not share personal details of members with other members or non-members or agencies, without the strict written permission of the member whose details have been requested. (However, in matters of dispute, with the express permission of the General Secretary, a member's details can be shared with a legitimate agency.)</p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="headingFour">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                             <strong class="qa">Q.</strong>&nbsp;IS IT MANDATORY FOR ME TO BE A MEMBER OF THE SWA IF I WANT TO BE A PROFESSIONAL SCREENWRITER?
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                          <div class="panel-body">
                                             <p><strong class="qa">A.</strong>No. However, most of the producers will ask you to register your script/work with SWA, as it’s the correct and professional way to function. Furthermore, it helps if you run into any problems with fellow writers or Producers/Directors in matter of Copyright, credits, remuneration etc.</p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="headingFive">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                             <strong class="qa">Q.</strong>&nbsp;WOULD SWA HELP ME IN CASE OF COPYRIGHT INFRINGEMENT/ THEFT OF MY WORK?
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                          <div class="panel-body">
                                             <p><strong class="qa">A.</strong> Yes. Our Dispute Settlement Committee will help you in such a situation; provided you’re able give adequate proof of Copyright infringement/ theft and if the said work was registered with us.</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <input type="radio" name="tabs" id="tabtwo">
                           <label for="tabtwo">Register Your Work - General</label>
                           <div class="tab">
                              <div class="cp-acticle-box">
                                 <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="headingSeven">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                             <strong class="qa">Q.</strong>&nbsp;HOW DO I REGISTER MY SCRIPT/WORK WITH SWA?
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                          <div class="panel-body">
                                             <p><strong class="qa">A.</strong>Once you become a member with SWA, you get the option of registering your work at our office or via our website.</p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="headingEight">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                             <strong class="qa">Q.</strong>&nbsp;HOW ELSE CAN I REGISTER MY WORK UNTIL I BECOME A SWA MEMBER?
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                          <div class="panel-body">
                                             <p><strong class="qa">A.</strong>You may contact the Registrar of Copyrights (Copyright Office), Delhi.You can email your script/work to yourself. The date on email will serve as a proof of your copyright on that particular date.There's also an option of mailing your script/work to yourself by registered post. The sealed envelope, date of the registry and the receipt will collectively serve as proof of your copyright on that particular date. </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="headingNine">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                             <strong class="qa">Q.</strong>IS IT SAFE TO SHARE MY WORK PROFESSIONALLY AFTER REGISTERING IT WITH SWA?
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                                          <div class="panel-body">
                                             <p><strong class="qa">A.</strong>Yes. SWA’s Script Registration process is recognized and abided by the film industry as well as the entire body of film associations. However, it’s advisable to share only the registered softcopies (or the Xerox of the registered copies) as a deterrent to any Copyright infringement. </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="headingTen">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                             <strong class="qa">Q.</strong>IF I REWORK MY SCRIPT/SCREENPLAY, DO I HAVE TO REGISTER IT AGAIN?
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                                          <div class="panel-body">
                                             <p><strong class="qa">A.</strong>Yes, it is advisable to register ALL the drafts of your script/work, at every stage of rewrite before you share it professionally.</p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="headingEle">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseEle" aria-expanded="false" aria-controls="collapseEle">
                                             <strong class="qa">Q.</strong>CAN I USE SCRIPT REGISTRATION FACILITY TO REGISTER MY HINDI, OR OTHER SCRIPTS?
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseEle" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEle">
                                          <div class="panel-body">
                                             <p><strong class="qa">A.</strong>Yes. You can register work in all Indian scripts like Roman, Devnagari, Urdu, Tamil, Gurumukhi etc. at our office as well on the website. </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="headingTwl">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwl" aria-expanded="false" aria-controls="collapseTwl">
                                             <strong class="qa">Q.</strong>CAN I REGISTER MY WORK BY POST?
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseTwl" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwl">
                                          <div class="panel-body">
                                             <p><strong class="qa">A.</strong>No. You can either do it in person at the SWA Office or register online on the website. However, your representative can also register the script/work on your behalf at SWA office provided he/she carries your valid SWA Membership Card.</p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="headingThrt">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseThrt" aria-expanded="false" aria-controls="collapseThrt">
                                             <strong class="qa">Q.</strong>WHAT ALL CAN I GET REGISTERED WITH SWA?
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseThrt" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThrt">
                                          <div class="panel-body">
                                             <p><strong class="qa">A.</strong>SWA provides registration for the following categories –
                                                Synopsis, Story, Script, Screenplay, Dialogue, Song and Mukhda.
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="headingFrtn">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFrtn" aria-expanded="false" aria-controls="collapseFrtn">
                                             <strong class="qa">Q.</strong>WHAT ARE THE GUIDELINES FOR SCRIPT REGISTRATION AT SWA OFFICE)?
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseFrtn" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFrtn">
                                          <div class="panel-body">
                                             <p><strong class="qa">A.</strong>
                                             <ul class="mem">
                                                <li class="mem-li">Number all the pages.</li>
                                                <li class="mem-li">At the top of the first page leave space for a paragraph for the SWA registration stamp and signature.</li>
                                                <li class="mem-li">The title, category of work and the name of the writer should be clearly written at the top of the first page, right before the main content. </li>
                                                <li class="mem-li">You can register multiple Songs/Mukhdas in one document but do number and title them specifically and clearly. </li>
                                             </ul>
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="headingFift">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseFift" aria-expanded="false" aria-controls="collapseFift">
                                             <strong class="qa">Q.</strong>CAN I GET A HAND-WRITTEN SCRIPT REGISTERED OR DO I HAVE TO GET IT TYPED?
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseFift" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFift">
                                          <div class="panel-body">
                                             <p><strong class="qa">A.</strong>You can get a handwritten manuscript registered at the SWA office, if it follows the prescribed guidelines. However, getting your material typed and generating a PDF is a prerequisite if you're registering online at our website.</p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="hd6">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cl6" aria-expanded="false" aria-controls="cl6">
                                             <strong class="qa">Q.</strong>WHAT IS THE FEES FOR SCRIPT REGISTRATION (AT SWA OFFICE & ONLINE)?
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="cl6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hd6">
                                          <div class="panel-body">
                                             <p><strong class="qa">A.</strong></p>
                                             <div class="col-md-12">
                                                <table class="table table-hover">
                                                   <tbody>
                                                      <tr>
                                                         <th class="text-center" colspan="2">SCRIPT REGISTRATION FEES AT SWA OFFICE</th>
                                                      </tr>
                                                      <tr>
                                                         <th scope="row">The first page of Synopsis, Story, Script, Screenplay, Dialogue</th>
                                                         <td>INR 20/-</td>
                                                      </tr>
                                                      <tr>
                                                         <th scope="row">After first page</th>
                                                         <td>INR 2/- per page</td>
                                                      </tr>
                                                      <tr>
                                                         <th scope="row">Per Song</th>
                                                         <td>INR 15/-</td>
                                                      </tr>
                                                      <tr>
                                                         <th scope="row">Per Mukhda</th>
                                                         <td>INR 10/-</td>
                                                      </tr>
                                                      <tr>
                                                         <th scope="row">Welfare Fund</th>
                                                         <td>INR 5/- per transaction</td>
                                                      </tr>
                                                      <tr>
                                                         <th class="text-center" colspan="2">ONLINE SCRIPT REGISTRATION* FEES</th>
                                                      </tr>
                                                      <tr>
                                                         <th scope="row">The first page of Synopsis, Story, Script, Screenplay, Dialogue, Song & Mukhda</th>
                                                         <td>INR 40/-</td>
                                                      </tr>
                                                      <tr>
                                                         <th scope="row">After first page</th>
                                                         <td>INR 2/- per page</td>
                                                      </tr>
                                                      <tr>
                                                         <th scope="row">Welfare Fund</th>
                                                         <td>INR 5/- per transaction</td>
                                                      </tr>
                                                      <!-- <tr>
                                                         <th scope="row">Per Song</th>
                                                         <td>INR 10/-</td>
                                                      </tr>
                                                      <tr>
                                                         <th scope="row">Per Mukhda</th>
                                                         <td>INR 5/-</td>
                                                      </tr>
                                                      <tr>
                                                         <th scope="row">Per transaction (Internet handling fees)</th>
                                                         <td>INR 50/-</td>
                                                      </tr> -->
                                                   </tbody>
                                                </table>
                                                <ul>
                                                  <li>
                                                    *Only ONE Article can be registered at a time. Registration of multiple Articles/Songs/Mukhda in a single document will lead to cancellation of the registration.
                                                  </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="hd7">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#cl7" aria-expanded="false" aria-controls="cl7">
                                             <strong class="qa">Q.</strong>I HAVE LOST THE REGISTERED COPY OF SCRIPT/WORK. CAN SWA PROVIDE IT TO ME?
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="cl7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hd7">
                                          <div class="panel-body">
                                             <p><strong class="qa">A.</strong>No. Whether one registers at office or online, the Association DOES NOT store or archive any member’s script/work, for secrecy and security purposes.</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <input type="radio" name="tabs" id="tabthree">
                           <label for="tabthree">Register Your Work - Online Script Registration</label>
                           <div class="tab">
                              <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="ol1">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#o1" aria-expanded="false" aria-controls="o1">
                                          <strong class="qa">Q.</strong>&nbsp; HOW DOES SWA’S ONLINE SCRIPT REGISTRATION WORK?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="o1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ol1" aria-expanded="false" >
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> The Online Script Registration system asks the user to upload a PDF and counts the number of pages. Once the payment is confirmed, it generates a registered PDF with a SWA registration number and unique digital ID, stamped in a QR code. The QR Code retains the registration details. The user is shown a temporary link through which he or she can download the registered soft copy. Additionally, one can also get an email at the registered email ID carrying the registered soft copy as an attachment.  </p>
                                          <p>The following are the TECHNICAL INSTRUCTIONS for Online Script Registration:</p>
                                          <p>
                                            <ol>
                                              <li><p>1. Any attempt to tamper with the document will invalidate the digital signature and cancel the registration.</p></li>
                                              <li><p>2. For secrecy and security purposes, SWA website DOES NOT save your files/work. Therefore, only you are responsible for the safekeeping of your Registered Files.</p></li>
                                              <li><p>3. You can register a maximum of FOUR Articles per transaction by uploading FOUR PDFS. However, registration of multiple Article/Songs/Mukhda in a single PDF will lead to cancellation of the registration.</p></li>
                                              <li><p>4. You can also add a maximum of TWO Co-authors per transaction. However, due to technical limitations, while adding Co-author/s you won't be able to add multiple PDFs.</p></li>
                                              <li><p>5. DO NOT proceed with payment if the number of pages is shown incorrect. If there is discrepancy in the number of actual pages and the payment calculation, it will lead to cancellation of the registration.</p></li>
                                              <li><p>6. Avoid apostrophes and special characters in the script title.</p></li>
                                              <li>
                                                <p>7. UPLOADING THE RIGHT PDF FILE:</p>
                                                <ul>
                                                  <li><p>. Acceptable Font size is 12-14 (standard single spacing).</p></li>
                                                  <li><p>. DO NOT upload PDFs having scanned images. Use only PDFs of text documents.</p></li>
                                                  <li><p>. The PDF file must be A4 size with portrait orientation (landscape not accepted).</p></li>
                                                  <li><p>. There must be a margin of 1 inch on top and bottom of every page of your work.</p></li>
                                                  <li><p>. DO NOT protect your file using any password. Password protected files will not be processed by system.</p></li>
                                                  <li><p>DO NOT include any Header or Footer in your file. They will be inserted by the system as proof of registration.</p></li>
                                                </ul>
                                              </li>
                                            </ol>
                                          </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="ol2">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#o2" aria-expanded="false" aria-controls="o2">
                                          <strong class="qa">Q.</strong>&nbsp;I HAVE FORGOTTEN TO DOWNLOAD THE DOCUMENT AFTER REGISTRATION. HOW WILL I GET THE REGISTERED COPY?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="o2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ol2">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> An email is sent to your registered email ID after every registration, carrying the registered PDF as an attachment.
However, it may take a few minutes for this email to come to you. If you don’t receive the email even after a few hours, contact Technical Support.  </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="ol3">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#o3" aria-expanded="false" aria-controls="o3">
                                          <strong class="qa">Q.</strong> I GET ERRORS WHILE REGISTRATION. WHY? OR
                                          MY ACCOUNT WAS DEBITED BUT REGISTRATION HASN’T TAKEN PLACE. WHY?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="o3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ol3">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> May be, your PDF file had a compression ratio that is not compatible with our online system. Try again with a different PDF.
If the money was deducted but the script was not registered then please contact Technical Support giving you transaction number. We will cancel your transaction and money will be credited to your account. Online free PDF convertors and free screenwriting softwares like CeltX can cause this problem. Use MS Word (or Mac Word), which have standard compression ratios, to convert your files to PDF.</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="ol4">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#o4" aria-expanded="false" aria-controls="o4">
                                          <strong class="qa">Q.</strong> I REQUESTED SWA TO CANCEL MY TRANSACTION, AS I COULDN’T DOWNLOAD THE REGISTERED SCRIPT. I LATER FOUND THE REGISTERED PDF FILE IN MY INBOX. NOW WHAT?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="o4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ol4">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> You shall find only the scripts/work that you registered online after July 2014.
Documents registered before this month were taken down from the website after repeated reminders and a year of notice on the website. Also, documents registered manually at our office won't show online.</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="ol5">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#o5" aria-expanded="false" aria-controls="o5">
                                          <strong class="qa">Q.</strong>IN MY DASHBOARD, REGISTER YOUR WORK TAB IS INACTIVE. WHY?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="o5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ol5">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong>Check your Membership Validity on the membership card. It might need a renewal.  </p>
                                       </div>
                                    </div>
                                 </div>

                              </div>
                           </div>

                           <!--tab four start here-->
                            <input type="radio" name="tabs" id="tabfour">
                           <label for="tabfour">ASK OUR LAWYER</label>
                           <div class="tab">
                              <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="aol1">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ao1" aria-expanded="false" aria-controls="ao1">
                                          <strong class="qa">Q.</strong>&nbsp; HOW CAN ONE GET LEGAL CONSULTATION FROM SWA’S LEGAL OFFICER?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ao1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="aol1" aria-expanded="false" >
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> SWA members can email to our Legal Officer and take consultation.
They can also our Legal Officer by taking a prior appointment.
(Appointment timings: 3.00 -6.00 PM, Monday to Friday)
Write to our Legal Officer, here.  </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="aol2">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ao2" aria-expanded="false" aria-controls="ao2">
                                          <strong class="qa">Q.</strong>&nbsp;I’M NOT A SWA MEMBER. CAN I SEEK CONSULTATION FROM SWA’S LEGAL OFFICER?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ao2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="aol2">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> No. Our Legal Officer provides consultation exclusively SWA members.   </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="aol3">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ao3" aria-expanded="false" aria-controls="ao3">
                                          <strong class="qa">Q.</strong> DO I PAY TO LEGAL OFFICER FOR THE LEGAL CONSULTATION?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ao3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="aol3">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> No. This service is free of charge.</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="aol4">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ao4" aria-expanded="false" aria-controls="ao4">
                                          <strong class="qa">Q.</strong> CAN I CHOOSE THE TIME AND PLACE FOR LEGAL CONSULTATION?
OR, CAN I GET CONSULTATION ON PHONE?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ao4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="aol4">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> No. One can meet our Legal Officer only at SWA office after taking a prior appointment. (Appointment timings: 3.00 - 6.00 PM, Monday to Friday)</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="aol5">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ao5" aria-expanded="false" aria-controls="ao5">
                                          <strong class="qa">Q.</strong> I’M A SWA MEMBER. CAN THE LEGAL OFFICER DRAFT A CONTRACT FOR ME?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ao5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="aol5">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> While the SWA’s Legal Officer can certainly guide and advise its members on an existing contract, the services of drafting a contract for the writers does not fall in the dominion of the services of Legal Officer. However, we do have a database of competent advocates who can offer these services to you either for free or for a subsidized cost.   </p>
                                       </div>
                                    </div>
                                 </div>

                              </div>
                           </div>
                           <!--tab four end here-->

                           <!--tab five start here-->

                            <input type="radio" name="tabs" id="tabfive">
                           <label for="tabfive">COPYRIGHTS & ROYALTY</label>
                           <div class="tab">
                              <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="col1">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#co1" aria-expanded="false" aria-controls="co1">
                                          <strong class="qa">Q.</strong>&nbsp; WHAT ARE THE BASIC RIGHTS OF A SCREENWRITER?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="co1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="col1" aria-expanded="false" >
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> A fair contract, respectable remuneration as per Industry standards, freedom of speech (to be exercised in an accountable manner), due recognition and appropriate royalties are few of the basic rights which a writer possesses by default.
Read about the intellectual property rights of writers, sanctioned by the Amended Copyright Act, here. </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="col2">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#co2" aria-expanded="false" aria-controls="co2">
                                          <strong class="qa">Q.</strong>&nbsp;I AM ABOUT TO PITCH MY WORK TO A PRODUCER. WHAT INSTRUCTIONS DO I KEEP IN MIND?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="co2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="col2">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong>
                                          <ul class="mem">
                                            <li class="mem-li">Foremost, do not pitch/disclose your work to anyone, including a friend/family member/relative before having it registered, either with SWA or the Copyright Office.</li>
                                            <li class="mem-li">Avoid pitching your work through personal meetings and/or on the phone. It is always advisable to have a paper trail in case of copyright theft or breach of confidentiality, and hence, email is a preferable mode of pitching your script to the producer and/or any other individual. </li>
                                            <li class="mem-li">As far as possible, always insist on pitching the script to the official email ID of the person associated with a production house. Do not share your script without insisting on a Non-Disclosure Agreement (“NDA”) even if it is with a trusted individual. A standard template of the NDA can be found here.</li>
                                            <li class="mem-li">If you are unable to enter into a Non-Disclosure Agreement with the person you pitch your script to, at least ensure that you include a disclaimer of confidentiality in the email containing your script as an attachment. You may refer to the following sample disclaimer, modified as per your requirement:
                                            <p style="margin-top:10px">“DISCLAIMER:
This email contains confidential and privileged information, being shared with the recipient in trust. The information is being shared with the recipient only for the purpose of pitching [the script/concept] to the recipient and in case the recipient does not want to proceed to work with the author on the same, he may not save and/or retain the information in any form and delete and/or discard the same upon reaching such decision. Further, the contents of this email are not to be shared with any third party without the explicit permission of the author of the [script/concept], and any such sharing of information otherwise than with the consent of the author shall amount to a breach of confidentiality and trust and the author reserves the right to pursue legal action against the recipient in the occurrence of such breach of confidentiality.”</p>
                                            </li>
                                            <li class="mem-li">If you pitch your work to the producer through a personal meeting and/or narrate your script to the producer in person, always maintain a record of such meeting by sending an email to the producer either before or immediately after such meeting, detailing the discussion you had or are scheduled to have at the meeting. </li>
                                          </ul>
                                         </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="col3">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#co3" aria-expanded="false" aria-controls="co3">
                                          <strong class="qa">Q.</strong> THEY SAY – “YOU DON'T GET A COPYRIGHT ON AN IDEA OR CONCEPT” WHAT DOES THAT MEAN?
HOW CAN MY ORIGINAL THOUGHT GET A LEGAL ACKNOWLEDGEMENT?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="co3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="col3">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> An idea or concept is a seed, germ of a thought; which has not been worked upon to its capacity. An idea or concept, by sheer co-incidence can strike to more than one person at different time intervals. The best practice is to develop your ideas and concepts into treatments or stories and get them registered before narrating to anyone.</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="col4">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#co4" aria-expanded="false" aria-controls="co4">
                                          <strong class="qa">Q.</strong> WHAT IS SCREEN CREDIT? HOW AND AT WHAT STAGE CAN I DETERMINE WHAT SCREEN CREDIT I SHOULD GET?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="co4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="col4">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> Screen Credits, are the titles that appear on the screen when the film/show is shown/aired. Your contract must state what credit/s you'll get and in what order they will appear. You can also negotiate on marketing related credits (like on posters, hoardings and other publicity etc.) by incorporating clauses about the same in your contract.  </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="col5">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#co5" aria-expanded="false" aria-controls="co5">
                                          <strong class="qa">Q.</strong> WHERE CAN I PROCURE INFORMATION AND LATEST UPDATES REGARDING IPR AND COPYRIGHT LAWS AND POLICIES?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="co5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="col5">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> The information is available at <a href="http://www.iprindia.com/copyright.htm">http://www.iprindia.com/copyright.htm</a>    </p>
                                       </div>
                                    </div>
                                 </div>

                              </div>
                           </div>

                           <!--tab five end here-->

                           <!--tab six start here-->

                            <input type="radio" name="tabs" id="tabsix">
                           <label for="tabsix">CONTRACTS, RELEASE FORMS & NDAs </label>
                           <div class="tab">
                              <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="rol1">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ro1" aria-expanded="false" aria-controls="ro1">
                                          <strong class="qa">Q.</strong>&nbsp; WHAT ARE THE BASIC CLAUSES TO LOOK OUT FOR WHILE SIGNING AN AGREEMENT/CONTRACT WITH A PRODUCER?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ro1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="rol1" aria-expanded="false" >
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong>
                                             <ul class="mem">
                                            <li class="mem-li">Do not sign the contract before thoroughly reading and understanding each and every covenant of the agreement. If you are unable to understand any of the clauses in your agreement, consult a legal expert to have it explained. SWA now offers this guidance to its members through its Legal Officer, free of charge.</li>
                                            <li class="mem-li">
                                              As much as possible, insist on a ‘copyright assignment agreement’ agreement rather than a ‘work-for-hire’ or ‘commissioned work’ agreement.
                                            </li>
                                            <li class="mem-li">
                                             Make sure that the amount of consideration is broken down in various stages/tranches instead of a one-time payment of the bulk amount. This would ensure that if the contract is terminated abruptly, you are at least paid a compensation due to you till the stage of such termination.
                                            </li>
                                            <li class="mem-li">
                                             Section 19(4) of the Copyright Act, 1957 gives you the right to have the rights in your work revert back to you in case of non-use by the producer of such rights within a year. However, such right is waivable through the contract. See if your contract requires you to waive your right under Section 19(4) of the Copyright Act. If it does, ensure that there is at least a clause in your agreement that the producer would have to return your rights to you or that such rights will automatically return to you if not used by the producer for a specific period. This would ensure that if the producer acquires the right in your script and later does not end up making a movie on it, he cannot keep the rights in your script in perpetuity.
                                            </li>
                                             <li class="mem-li">Termination – make sure that your termination clause is not one-sided and unfair. Ensure that your termination clause address the question of who shall retain the rights of your script upon termination of the contract by either of the parties: In case the producer wishes to retain the rights over the script upon termination, ensure that there is a clause to the effect that the rights over the script shall not vest with the producer till the consideration due to you till the stage of termination is paid to you.</li>
                                             <li class="mem-li">
                                              Credit – Ensure that the credit clause survives termination of your agreement. Even if the agreement is terminated by either of the parties, you shall still be credited for your work.
                                             </li>
                                              <li class="mem-li">
                                              Royalty – Make sure that your contract addresses the percentage of royalty due to you for your copyrighted work. Your royalty is separate from the consideration and any clause which states that royalty is included in the consideration is not valid. Your royalty is and should be separate from your consideration.
                                             </li>
                                             <li class="mem-li">Make sure that your contract specifically mentions and lists down the particular rights which are being assigned to the producer rather than merely stating ‘all rights.’</li>
                                             <li class="mem-li">Union/Guild Jurisdiction – Ensure that the jurisdiction of SWA or any other writer’s union/guild is not excluded from the contract. If there is a dispute at a later date and the writer has waived the right to raise any grievance with or raise the jurisdiction of his or her union/guild, such union including SWA shall not be able to intervene to resolve the dispute on your behalf.</li>

                                            <li class="mem-li">These are only the basic clauses that one needs to look out for in the contract. However, other than ensuring the presence of these above clauses, do study your agreement thoroughly for other clauses that might pose a potential danger to you.
                                            </li>

                                            </ul>
                                           </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="rol2">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ro2" aria-expanded="false" aria-controls="ro2">
                                          <strong class="qa">Q.</strong>&nbsp;CAN THERE BE TWO CONTRACTS? ONE THAT I PREPARE, AND ANOTHER THAT MY PRODUCER PROVIDES?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ro2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="rol2">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong>
                                          No. Both the parties sign one single mutually agreed contract.
                                         </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="rol3">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ro3" aria-expanded="false" aria-controls="ro3">
                                          <strong class="qa">Q.</strong> ARE CONTRACTS FOR FILMS AND TV ONE AND THE SAME?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ro3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="rol3">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> No. There are different contracts for Films, Television, Digital Media and Songs. </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="rol4">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ro4" aria-expanded="false" aria-controls="ro4">
                                          <strong class="qa">Q.</strong> IS THERE A MODEL OR SAMPLE CONTRACT THAT I CAN REFER TO FOR DRAFTING AN AGREEMENT BETWEEN THE PRODUCER AND ME?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ro4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="rol4">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> Yes. Refer to our Minimum Basic Contracts (Film, TV, Lyrics, Digital) here.
Although strongly proposed by SWA, the MBCs are still at a conceptual stage and not yet approved by Producer Bodies. Also, they may have certain minor provisions missing. Use them as models for drafting your contract, making modifications as per your requirements.   </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="rol5">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ro5" aria-expanded="false" aria-controls="ro5">
                                          <strong class="qa">Q.</strong> WHAT ARE MY RIGHTS AND DUES IF A PRODUCER TERMINATES AN AGREEMENT AT SOME STAGE OF DEVELOPMENT?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ro5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="rol5">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> Your contract will determine what will happen when such a situation arises. A contract normally will have specific termination clauses. 
Therefore, do ensure beforehand that the contract you sign is in conformity with the basic terms and conditions as stated above.    </p>
                                       </div>
                                    </div>
                                 </div>


                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="rol6">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ro6" aria-expanded="false" aria-controls="ro6">
                                          <strong class="qa">Q.</strong> THE PRODUCER HAS APPROVED MY SCRIPT AND IS ASKING FOR MODIFICATIONS IN IT. BUT WE DO NOT HAVE A CONTRACT YET. WHAT DO I DO?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ro6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="rol6">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> Request the producer to enter into an agreement with you for assignment of copyright in your script. Do ensure that the agreement is in conformity with the basic terms and conditions as stated above.<br>If the producer is unable to enter into a full-fledged agreement at the moment, request the Producer to at least enter into a Memorandum of Understanding (MoU) with you for a copyright assignment/license in your script before you work on any modifications suggested by the producer. An MoU is a preliminary document crystallizing the intention of the parties to enter into a Long Form Agreement at a later date. Please note that an MoU is not sufficient by itself and is only an understanding between the parties of the basic terms and conditions of their deal. Do not sign the MoU or any other legal document to that effect without reading and thoroughly understanding it and preferably having it examined by a lawyer. </p>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="rol7">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ro7" aria-expanded="false" aria-controls="ro7">
                                          <strong class="qa">Q.</strong> I HAVE SIGNED A CONTRACT WITH A PRODUCER BUT NOW I WISH TO GET OUT OF IT. CAN I DO THAT?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ro7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="rol7">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> An agreement forms a legal relationship between the parties where one party promises to deliver the goods/services (which, in your case, is the script/screenplay) and the other party promises to pay a valuable consideration for the goods/services in return. When such a legal promise is entered into, the breach of such promise (by either parties) would naturally attract a penalty in the form of damages. The party breaching the promise has to compensate the party who suffered a loss (monetary or otherwise). Avoiding a contract or non-performance of a contract amounts to a breach and would entitle the producer for compensation from you for the losses suffered due to your breach. <br>The amount for compensation of such losses would vary from case to case and would solely depend upon the contract you have entered into. </p>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="rol8">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ro8" aria-expanded="false" aria-controls="ro8">
                                          <strong class="qa">Q.</strong>FOR HOW LONG IS A CONTRACT BINDING ON ME? IF THE PRODUCER HASN’T STARTED PRODUCTION ON MY MOVIE, AM I ENTITLED TO TERMINATE IT?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ro8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="rol8">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> The term of validity of each contract differs and is completely dependent upon the wordings of your own contract. The termination and term of copyright also depend solely upon what your contract sets out.  </p>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="rol9">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ro9" aria-expanded="false" aria-controls="ro9">
                                          <strong class="qa">Q.</strong>THE PRODUCER HAS GIVEN ME A CONTRACT TO SIGN, WHICH SEEMS UNFAIR, BUT HAS ASSURED ME THAT THIS IS JUST TEMPORARY AND THAT WE WILL SIGN A BETTER CONTRACT IN THE FUTURE. SHOULD I SIGN THIS CONTRACT?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ro9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="rol9">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> NO. The writer has the most bargaining power to set out the terms of his contract fairly before such contract is signed. Once the contract is signed, no such oral promises of signing a better contract in the future are valid. A written agreement signed by both the parties supersedes any other oral agreement that the parties may have had. Do not sign any unfair contract based on the promises of a better contract in the future and/or assurance of the producer or any other parties that these unfair terms shall not be enforced on you. Any changes that you want present in your contract have to be made before you sign such a contract. Do not sign a contract unless you are completely satisfied of its fairness towards you.</p>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="rol10">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ro10" aria-expanded="false" aria-controls="ro10">
                                          <strong class="qa">Q.</strong>HOW MUCH GST IS APPLICABLE ON MY CONTRACT AND WHO SHALL PAY THE GST?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ro10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="rol10">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> SWA is speaking to experts on this issue and shall come up with a consolidating answer shortly.</p>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="rol11">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ro11" aria-expanded="false" aria-controls="ro11">
                                          <strong class="qa">Q.</strong>WHAT IS THE STAMP DUTY PAYABLE ON A COPYRIGHT ASSIGNMENT AGREEMENT AND NON-DISCLOSURE (CONFIDENTIALITY) AGREEMENT? WHO PAYS THE STAMP DUTY?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ro11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="rol11">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> Assuming that the agreement was executed in Bombay and governed by the Bombay Stamp Act:<br>
                                          Stamp duty value for ​a copyright assignment agreement:<br>
                                          (a) if the amount agreed (i.e., your consideration) does not exceed rupees ten lakhs - Two rupees and fifty paise for every rupees 1,000 or part thereof on the amount agreed in the contract subject to minimum of rupees 100.
                                          <br>
                                          (b) in any other case - Five rupees for every rupees 1,000 or part thereof on the amount agreed in the contract.
                                          <br>
                                          For Non-Disclosure (Confidentiality) Agreement: Since the same is not expressly stated under the Stamp Duty Act, it is my opinion that it will fall under the ‘miscellaneous’ category, for which the stamp duty is Rs. 100.

The question of who pays the stamp duty would be usually addressed by your contract itself. However, the payment of stamp duty is flexible and can be paid by either parties or divided between both the parties, as per the parties’ mutual decision.
                                          </p>
                                       </div>
                                    </div>

                                 </div>


                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="rol12">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ro12" aria-expanded="false" aria-controls="ro12">
                                          <strong class="qa">Q.</strong>I’M A NEW WRITER. HOW MUCH REMUNERATION SHOULD I ASK FOR?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ro12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="rol12">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> The rule of thumb for screenwriting fee is 2-5% of the total production budget. However, it largely depends upon the negotiations between the Producer and the writer. SWA is in the process of negotiating standard minimum wages with the Producer bodies. <br>
                                      See the Minimum Rate Cards proposed by SWA, here.
                                          </p>
                                       </div>
                                    </div>

                                 </div>

                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="rol13">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ro13" aria-expanded="false" aria-controls="ro13">
                                          <strong class="qa">Q.</strong>HOW SAFE IS IT TO SIGN A ‘RELEASE FORM’ GIVEN TO ME BY A PRODUCER OR A PRODUCTION HOUSE?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="ro13" class="panel-collapse collapse" role="tabpanel" aria-labelledby="rol13">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> Usually, before you pitch your script/screenplay to the Producer, you are asked to sign a ‘release form’ or a ‘submission release’ which states that you shall not sue production company or be entitled to any compensation on the occasion of any similarities between the material submitted by you and any material which may be developed or is under development by the production house.  <br>
                                      It is a standard industry practice for producers to make writers sign such release forms, wherein the writers waive their right to sue the producer or claim compensation in case of any similarity between their submission and any project that the producer may undertake. While this is done primarily to protect the legitimate interests of the producer, because it is quite possible that several writers may co-incidentally pitch the same story idea to the producer, or the production house may itself through its in-house team of writers be in the process of developing a similar idea, the legal wordings of such release forms are often quite harsh and skewed against the writers. <br>
                                      Please be assured however, that no release form/submission release can provide a producer a licence to commit copyright theft. If the writer can prove that his/her submission was actually stolen by the producer, then irrespective of the release, the writer can seek a legal remedy against the producer. <br>However, having said that, never sign away anything without first having understood it and weighing the implications of signing the document. What rights you are essentially signing away entirely depend on the wordings of your particular release and SWA would always advise you to have your release looked over and explained to you by a lawyer before you sign it.
                                          </p>
                                       </div>
                                    </div>

                                 </div>

                              </div>
                           </div>


                           <!--tab six end here-->

                           <!--tab seven start here-->

                            <input type="radio" name="tabs" id="tabseven">
                           <label for="tabseven">ASK OUR DSC</label>
                           <div class="tab">
                              <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="dol1">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#do1" aria-expanded="false" aria-controls="do1">
                                          <strong class="qa">Q.</strong>&nbsp; WHEN CAN I APPROACH THE DSC?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="do1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="dol1" aria-expanded="false" >
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong>
                                            You can approach the DSC to resolve matters concerning your Copyright infringement, remuneration and credits. Learn more about DSC, here.
                                           </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="dol2">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#do2" aria-expanded="false" aria-controls="do2">
                                          <strong class="qa">Q.</strong>&nbsp;CAN NON-MEMBERS TURN TO DSC FOR A DISPUTE CASE?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="do2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="dol2">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong>
                                          No. SWA is a trade union, and its DSC provides support only to its members.
                                         </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="dol3">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#do3" aria-expanded="false" aria-controls="do3">
                                          <strong class="qa">Q.</strong> IT’S TURNING BAD BETWEEN A PRODUCER/WRITER AND MYSELF. HOW CAN I ASK THE DSC FOR HELP?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="do3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="dol3">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> Try for a settlement, one last time, by formal email.
You can also consider taking legal consultation from our Legal Officer.
To file a complaint, submit the duly filled and signed DSC Complaint Form with the required documents.
Learn more about filing a complaint with our DSC, here.  </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="dol4">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#do4" aria-expanded="false" aria-controls="do4">
                                          <strong class="qa">Q.</strong> IN A DISPUTE, SHOULD I GO TO DSC FIRST OR SEND A LEGAL NOTICE DIRECTLY? 
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="do4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="dol4">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> You should come to the DSC and they shall take your case forward. Taking any legal action without following the proper procedure would bar DSC to intervene at a later stage.   </p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="dol5">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#do5" aria-expanded="false" aria-controls="do5">
                                          <strong class="qa">Q.</strong> I DID NOT REGISTER MY DISPUTED SCRIPT AND/OR SIGNED A CONTRACT. BUT I HAVE ENOUGH PROOF THAT THERE WAS A COPYRIGHT INFRINGEMENT. WILL DSC ACCEPT MY CASE?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="do5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="dol5">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> Yes. The DSC can accept a case if there is enough evidence of Copyright infringement in the form of emails, text/Whats App messages etc.
In absence of a contract, the DSC follows the Minimum Rate Card proposed by SWA.  </p>
                                       </div>
                                    </div>
                                 </div>


                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="dol6">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#do6" aria-expanded="false" aria-controls="do6">
                                          <strong class="qa">Q.</strong>DO I HAVE TO PAY TO DSC FOR THEIR SERVICE?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="do6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="dol6">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> Only if there is some kind of compensation generated towards the writer, as and when the case is resolved, 5 percent of it goes to SWA. </p>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="dol7">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#do7" aria-expanded="false" aria-controls="do7">
                                          <strong class="qa">Q.</strong> CAN I APPROACH DSC FOR THE COPYRIGHT INFRINGEMENT OF MY REGISTERED STAGE-PLAY/BOOK?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="do7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="dol7">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> No. The Dispute Settlement Sub-Committee of SWA only takes up disputes related to films, TV and digital media.  </p>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="dol8">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#do8" aria-expanded="false" aria-controls="do8">
                                          <strong class="qa">Q.</strong>WILL THE DSC TAKE MY CASE WHILE THE MATTER IS UNDER TRAIL IN COURT?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="do8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="dol8">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> No. This is not allowed as per DSC’s procedure prescribed in the DS Bye-Laws.   </p>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="dol9">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#do9" aria-expanded="false" aria-controls="do9">
                                          <strong class="qa">Q.</strong>CAN THE DSC TAKE ACTION TO STOP THE RELEASE OF A FILM?
OR
CAN THE DSC ISSUE A STATEMENT / LETTER TO HELP ME FILE A F.I.R. OR SEND A LEGAL NOTICE?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="do9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="dol9">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> No. This is not allowed as per DSC’s protocol and the procedure prescribed in the DS Bye-Laws. </p>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="dol10">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#do10" aria-expanded="false" aria-controls="do10">
                                          <strong class="qa">Q.</strong>WHAT IF I FEEL DISSATISFIED WITH DSC VERDICT IN MY CASE?
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="do10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="dol10">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong>You can appeal against the verdict of the DSC, to Executive Committee of SWA.</p>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="dol11">
                                       <h4 class="panel-title">
                                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#do11" aria-expanded="false" aria-controls="do11">
                                          <strong class="qa">Q.</strong>YOUR QUESTION NOT LISTED ABOVE?
Ask us by sending an email at web.admin@swaindia.org
                                          </a>
                                       </h4>
                                    </div>
                                    <div id="do11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="dol11">
                                       <div class="panel-body">
                                          <p><strong class="qa">A.</strong> Download all the FAQs as a PDF here.
                                          </p>
                                       </div>
                                    </div>

                                 </div>
                              </div>
                           </div>
                           <!--tab seven end here-->
                        </div>
                     </div>
                     <!--  <div class="col-md-3">
                        </div> -->
                  </div>
               </div>
            </div>
         </div>
         <?php include_once('footer.php'); ?>
      </div>
      <script src="js/jquery-1.11.3.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/jquery.bxslider.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
      <script src="js/jquery.counterup.min.js"></script>
      <script src="js/custom.js"></script>
   </body>
</html>
