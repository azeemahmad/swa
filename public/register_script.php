<?php session_start();
if(isset($_SESSION['is_login']) &&  $_SESSION['is_login'] == 'true' )
{
  // header('Location: dashboard.php');         //comment this line
  if($_SESSION['exp_flag'] == 1){
      header('Location: dashboard.php');
  }
  unset($_SESSION['done']);
  ?>
  <!doctype html>
  <html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href=""/>
    <title>Instruction</title>
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <link href="css/color.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/> -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="inner-page">
    <div id="wrapper" class="inside-menu">
      <?php include_once('header.php'); ?>
      <div id="cp-content-wrap" class="page404 cp-login-page">
        <div class="container">
          <div class="row">
            <div class="col-md-8 col-md-offset-2 main-reg-div">

                <div class="cp-reg-box sub-reg-div">

                    <?php
                    if(!empty($_SESSION['work_register']['payment_error'])) {
                            ?>
                            <div class="alert alert-danger" style="font-size: 16px; text-transform: capitalize;"><?php echo $_SESSION['work_register']['payment_error']; ?></div>
                            <?php
                            unset($_SESSION['work_register']['payment_error']);
                    }
                    ?>

                <h4>Online Script Registration</h4>
                <h4>Author</h4>
                <p class="text-center">
                  <form id="regform1" class="appnitro" onsubmit="return validateForm()" enctype="multipart/form-data" method="post" action="scriptregproc.php">
                    <div class="form-group">
                      <label for="writername">Writer's Name</label>
                      <input type="text" class="form-control" id="writername" name="name" value="<?php echo $_SESSION['name']; ?>" readonly>
                    </div>
                    <?php
                    if(isset($_SESSION['coa']))
                    {
                      $cao_size = sizeof($_SESSION['coa']);
                    }
                    else {
                      $cao_size = 0;
                    } ?>

                    <?php
                    if(isset($_SESSION['coa']))
                    {
                      $coa = $_SESSION['coa'];
                      foreach ($coa as $key => $author) { ?>
                        <div class="form-group file-grp">
                          <span class="script-no remove_auth" data-coa = "<?php echo $author['membership']; ?>">Remove</span>
                          <label for="writername">Co Author - <?php echo ($key+1); ?></label>
                          <input type="text" class="form-control" id="writername" name="name" value="<?php echo $author['name']; ?>" readonly>
                        </div>
                      <?php   }
                    }
                    ?>
                    <h4>File</h4>
                    <div class="file-grp element" id="new">
                      <span class="script-no remove-btn">Remove</span>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Type of Creation</label>
                        <select class="form-control element creation_req" name="rtsstype[]" id="creation">
                          <option value="" selected="selected">Select Type</option>
                          <option value="Synopsis">Synopsis</option>
                          <option value="Story">Story</option>
                          <option value="Script">Script</option>
                          <option value="Screenplay">Screenplay</option>
                          <option value="Dialogue">Dialogue</option>
                          <option value="Song">Song</option>
                          <option value="Mukhda">Mukhda</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="pdfupload">Title</label><br>
                        <input type="text" name="rtstitle[]" id="rtstitle" class="form-control element title_req">
                      </div>
                      <div class="form-group">
                        <label for="pdfupload">Upload PDF</label>
                        <input type="file" name="pdffile[]" id="pdfupload" class="form-control element pdf_req">
                      </div>

                    </div>
                    <div id="ncopy"></div>
                    <br>

                    <div class="row">
                      <div class="col-md-12 text-justify">
                        <h4><span class="label label-default">You can add 2 more PDF files. <!-- OR add 2 more Co-Authors for this transaction. --> </span></h4>
                      </div>
                      <br><br>
                      <div class="col-md-3">
                        <?php if($cao_size == 0 ) { ?>
                          <button type="button" class="add-btn btn btn-default" id="copy">Add More PDF</button>
                        <?php } else { ?>
                          <button type="button" class="add-btn btn btn-default" disabled id="copy">Add More PDF</button>
                        <?php } ?>

                      </div>
                      <div class="col-md-3">
                        <?php /* if( $cao_size <= 1 ) { ?>
                          <button type="button" class="coauth-btn btn btn-default" id="auth">Add Co Author</button>
                        <?php }  else { ?>
                          <button type="button" class="coauth-btn btn btn-default" disabled id="auth">Add Co Author</button>
                        <?php } */ ?>
                      </div>
                    </div>
                    <h4 class="mar-tp-25">DECLARATION BY THE MEMBER</h4>
                    <p><input type="checkbox" id="tc_check" name="tc_check">&nbsp;<strong> I Accept That:</strong><br>
                      <ul>
                        <li>I solemnly undertake that this work is my original creation and that in case of a dispute if proved otherwise, my registration will stand canceled.</li>
                        <li>I've read all the <a href="javascript:void(0)" data-toggle="modal" data-target="#tech_instruction">Technical Instruction</a></li>
                      </ul>

                    </p>
                    <button type="submit" class="btn btn-default proced-btn">Submit</button>
                    <div id="file_msg"></div>
                  </form>
                </p>
                <p>
                    <h4 class="mar-tp-25">Note</h4>
                    <span>
                        Due to technical reasons, Co-author facility for Online Script Registrations is currently unavailable.
                    </span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include_once('footer.php'); ?>
    </div>
    <!-- Modal -->
    <div id="authModel" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content project-details-popup">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="modal-header">
            <img class="header-img" src="images/coauth.jpg">
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                <h4>Co-Author Script Registration</h4>
                <p>Register your work created together!!</p>
                <div id="coa_msg"></div>
                <form id="coa_frm" class="appnitro" action="javascript:void(0)">
                  <div class="form-group">
                    <label for="writername">Co Writer's Membership Number</label>
                    <input type="text" class="form-control" id="coa_membership" name="coa_membership">
                  </div>
                  <button id="coa_otp" type="submit" class="btn btn-default proced-btn">Proceed</button>
                </form>
                <form id="coa_ss_frm" class="appnitro hidden" action="javascript:void(0)">
                  <div class="form-group">
                    <label for="writername">Co Writer's Membership Number</label>
                    <input type="text" class="form-control" id="coa_mem" name="coa_mem" disabled readonly>
                  </div>
                  <div class="form-group">
                    <label for="writername">Secure Code</label>
                    <input type="text" class="form-control" id="coa_securecode" name="coa_securecode">
                  </div>
                  <button id="coa_reg" type="submit" class="btn btn-default proced-btn">Proceed</button>
                </form>
              </div>
            </div>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <div id="tech_instruction" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content project-details-popup">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 main-reg-div">
                <div class="cp-reg-box sub-reg-div">
                  <h5>Instruction</h5>
                  <ol>
                    <li>Any attempt to tamper with the document will invalidate the digital signature and cancel the registration.</li>
                    <li>For secrecy and security purposes, SWA website DOES NOT save your files/work. Therefore, only you are responsible for the safekeeping of your Registered Files.</li>
                    <li>You can register a maximum of FOUR Articles per transaction by uploading FOUR PDFS. However, registration of multiple Article/Songs/Mukhda in a single PDF will lead to cancellation of the registration.</li>
                    <li>You can also add a maximum of TWO Co-authors per transaction. However, due to technical limitations, while adding Co-author/s you won't be able to add multiple PDFs. </li>
                    <li>DO NOT proceed with payment if the number of pages is shown incorrect. If there is discrepancy in the number of actual pages and the payment calculation, it will lead to cancellation of the registration.</li>
                    <li>Avoid apostrophes and special characters in the script title.</li>
                    <li>UPLOADING THE RIGHT PDF FILE:
                        <ul>
                          <li>Acceptable Font size is 12-14 (standard single spacing).</li>
                          <li>DO NOT upload PDFs having scanned images. Use only PDFs of text documents.</li>
                          <li>The PDF file must be A4 size with portrait orientation (landscape not accepted).</li>
                          <li>There must be a margin of 1 inch on top and bottom of every page of your work.</li>
                          <li>DO NOT protect your file using any password. Password protected files will not be processed by system.</li>
                          <li>DO NOT include any Header or Footer in your file. They will be inserted by the system as proof of registration.</li>
                        </ul>
                    </li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.js"></script>
    <!-- <script src="js/jquery.prettyPhoto.js"></script> -->
    <script src="js/custom.js"></script>
    <script type="text/javascript">
    function validateForm()
    {

      var flag = 0;
      $('.creation_req').each(function() {
        if($(this).val() == '' || $(this).val() == null)
        {
          $(this).parent().addClass('has-error');
          flag++;
        }
        else {
          $(this).parent().removeClass('has-error');
        }
      });

      $('.title_req').each(function() {
        if($(this).val() == '' || $(this).val() == null)
        {
          $(this).parent().addClass('has-error');
          flag++;
        }
        else {
          $(this).parent().removeClass('has-error');
        }
      });

      var file_name = '';
      var fileflag = 1;
      $('.pdf_req').each(function() {
        if($(this).val() == '' || $(this).val() == null)
        {
          $(this).parent().addClass('has-error');
          flag++;
        }
        else {

          file_name = $(this).val();
          // var file_extension = file_name.split('.').pop();
          var file_extension = file_name.replace(/^.*\./, '');

          if(file_extension.toLowerCase() != 'pdf')
          {
              fileflag = 0;
              $(this).parent().addClass('has-error');
          }
          else {
            $(this).parent().removeClass('has-error');
          }
        }
      });
      // var writername = document.getElementById('writername').value;

      if(document.getElementById("tc_check").checked == false)
      {
        document.getElementById('file_msg').innerHTML = '<span class="label label-danger">Accept Declaration</span>';
        return false;
      }
      else if(flag > 0)
      {
        document.getElementById('file_msg').innerHTML = '<span class="label label-danger">All fields are required</span>';
        return false;
      }
      else if(fileflag == 0)
      {
        document.getElementById('file_msg').innerHTML = '<span class="label label-danger">Invalid file only PDF file is allowed</span>';
        return false;
      }
      else
      {

        return true;

        // console.log(creation);return false;
        // var allowed_extensions = new Array("pdf","PDF");
        // var file_extension = fileName.split('.').pop();
        // for(var i = 0; i <= allowed_extensions.length; i++)
        // {
        //   if(allowed_extensions[i]!=file_extension)
        //   {
        //     fileflag = 0; // invalid file extension
        //   }
        // }
        //
        // if(fileflag==0)
        // {
        //   document.getElementById('file_msg').innerHTML = '<span class="label label-danger">Invalid file only PDF file is allowed</span>';
        //   return false;
        // }
        // else
        // {
        //   return true;
        // }
      }
    }
    </script>
    <script type="text/javascript">
    $(document).ready(function(){

      var div_count = 0;
      div_count = parseInt(div_count);


      $('#copy').unbind('click');
      $('#copy').click(function(){
        var total_count = $('.file-grp').length;
        if(total_count < 3)
        {
          var template = $('#new').clone();
          div_count++;
          var attendee = template.clone().find('.element').each(function(){
            var newId = this.id + div_count;
            this.id = newId;
            this.value = '';
            // var newName = this.name + div_count;
            // this.name = newName;
            //alert(newId );
          }).end().attr('id', 'new' + div_count).appendTo('#ncopy');
          $('#auth').prop('disabled',true);
        }
      });

      $("body").on("click", ".remove-btn", function(event){
        var total_count = $('.file-grp').length;
        //console.log(total_count);
        if(total_count > 1)
        {
          $(this).parent().remove();
          total_count--;
        }

        if(total_count == 1)
        {
          $('#auth').prop('disabled',false);
        }


      });

      $("body").on("click", "#auth", function(event){
        $('#authModel').modal('show');
      });
      //classclick();
    });
    </script>
  </body>
  </html>
<?php }
else{
  header('Location: login.php');
} ?>
