<?php session_start();
include_once('../includes/config.php');
include_once('../includes/config_sync.php');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('Asia/Calcutta');

if(!empty($_POST['ajax']))
{
	$ajax = $_POST['ajax'];
	switch ($ajax) {

		case '1': /** login */
		$username = $_POST['username'];
		$username =	reg_no_chk($username);
		$password = $_POST['password'];
		$sql = "select fu.*,mt.* ,mem.*
		from `fwa_users` fu
		left join `mem_type` mt on mt.type_id = fu.type_id
		left join `fwa_members` mem on mem.reg_no = fu.reg_no
		where fu.reg_no = '".$username."' and fu.pass = '".$password."' and fu.pass != ''";



		if($result = mysqli_query($db,$sql))
		{
			$row = mysqli_fetch_assoc($result);
			if(mysqli_num_rows($result) > 0)
			{
				if($row['type_id'] == 2 || $row['type_id'] ==3 || $row['type_id'] ==6 || $row['type_id'] ==10)
				{
					echo 'You can not log in as a SWA member your records shows '.$row['type'].' as Memebership type.<br>Please contact SWA for more info';
				}
				else
				{
					$exp_date = date($row['membership_expiry_date']);

					if($row['type_id'] != 1 && $row['type_id'] != 4 && $row['type_id'] != 7 )// if member types are not Life / Senior - R / Senior - R
					{
						$today_date = strtotime("today midnight"); // unix time stamp
						// $today_date = strtotime(date("Y-m-d")); // unix time stamp
						// var_dump($today_date);die;
 						$exp_date = strtotime($row['membership_expiry_date']);
						$_SESSION['exp_date'] = date("d-m-Y", $exp_date);
						// var_dump(strtotime("+7 day",$today_date));die;
						if($exp_date >= $today_date)
						{
							if($exp_date <= strtotime("+7 day",$today_date))
							{
								$_SESSION['exp_flag'] = 2;	//will expire in 7 days
							}
							else {
									$_SESSION['exp_flag'] = 0;	//not expired
							}
						}
						else {
							$_SESSION['exp_flag'] = 1; //expired
						}
					}
					else {
						$_SESSION['exp_date'] = 'N/A'; // no expiry date
						$_SESSION['exp_flag'] = 0;//not expired
					}

					$_SESSION['is_login'] = 'true';
					$_SESSION['username'] = $username;
					$_SESSION['name'] = $row['name'];
					$_SESSION['type_id'] = $row['type_id'];
					if($row['email'] != null && $row['email'] != '')
					{
						$_SESSION['User_Email'] = $row['email'];
					}
					else
					{
						$_SESSION['User_Email'] =  'web.admin@swaindia.org';
					}

					echo 'Success';
				}

			}
			else
			{
				echo 'Invalid username or password';
			}
		}
		else
		{
			echo 'Something went wrong';
		}
		break;

		case '2':   /** Register step1 */
		 $reg_no = reg_no_chk($_POST['reg_no']);
		//$reg_no = $_POST['reg_no'];
		//var_dump($_POST['reg_no']);
		// $mobile = $_POST['mobile'];
		$sql_mem = "select fu.*,mt.* ,mem.*
		from `fwa_users` fu
		left join `mem_type` mt on mt.type_id = fu.type_id
		left join `fwa_members` mem on mem.reg_no = fu.reg_no
		where fu.reg_no = '".$reg_no."'";

		$result_mem = mysqli_query($db,$sql_mem);
		// var_dump(mysqli_num_rows($result_mem));die;
		if(mysqli_num_rows($result_mem) > 0)
		{

			$row = mysqli_fetch_assoc($result_mem);
			if($row['pass'] == NULL)
			{
				$securecode = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 4);
				$mobile = $row['mobileno'];
				//$mobile = '9769828965';    //comment

				if($row['name'] != '' && $row['name'] != null)
				{
					$name = $row['name'];
				}
				else
				{
					$name = 'writer';
				}

				if($mobile!='' && $mobile != null)
				{
					// $smsapi = "http://www.myvaluefirst.com/smpp/sendsms?username=firsteconomy&password=firsteco&to=".$mobile."&udh=0&from=SWASCO&text=".urlencode("Hey ".$name." your secure code is ".$securecode." Team SWA.");
					// $sms_response = sms($smsapi);


					$usr_msg = "Hey ".$name." your secure code is ".$securecode." Team SWA.";
					  $mobileNumber = $mobile;
						$user = 'TRANSAC1';
						$key = 'cafbae387eXX';
						$message = urlencode ($usr_msg);
						$senderid = 'INFOSM';
						$accusage = 1;

					 	$url="pt.k9media.in/submitsms.jsp?user=".$user."&key=".$key."&mobile=".$mobileNumber."&senderid=".$senderid."&accusage=".$accusage."&message=".$message."";

						$ch = curl_init($url);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$curl_response = curl_exec($ch);

						curl_close($ch);
						$sms_response = 'Sent';
					$mobile_flag = '1';
				}
				else
				{
					$sms_response = '0';
					$mobile_flag = '0';
				}

				$email = $row['email'];
				//$email = 'nirmeet@firsteconomy.com';

				if($email!='' && $email != null)
				{
					$reciever = $email;
					$subject = "SWA Secure Code for web sign up";
					$body = "Dear ".$name.", <br> Your secure code for sign up is: ".$securecode."<br><br> Team SWA.";
					$sender_email = "assist@firsteconomy.com";
					$sender_password = "f3e6f3e6";

					$email_response = send_mail($reciever,$subject,$body,$sender_email,$sender_password);
					$mail_flag = '1';
				}
				else
				{
					$email_response = '0';
					$mail_flag = '0';
				}


				//if(1)
				if($mobile_flag == '1' || $mail_flag == '1')
				{
					$carrier='';
					if($mobile_flag == '1')
					{
						$carrier = 'Mobile no: '. substr($mobile, 0,1).'******'.substr($mobile, -3).' - ';
					}

					if($mail_flag == '1')
					{
						$carrier = $carrier . 'Email: '.$email;
					}

					// echo $sms_response.'   |    '.$email_response;
					if($sms_response == 'Sent' || $email_response == '1')
					{
						$sql_update = "UPDATE `fwa_users` set `password_flag` = 1 , `securecode` = '".$securecode."' where `reg_no` = '".$reg_no."'";
						if(mysqli_query($db,$sql_update))
						{
							$msg = 'You will recieve your secure code via '.$carrier;
							echo json_encode(array('success' => '1' , 'message' => $msg, 'reg_no' => $reg_no , 'mobile' => $mobile ));
							//echo json_encode(array('success' => '1' , 'message' => "You will recieve an SMS containing secure code on your mobile number: ******".substr($mobile, -4)));
						}
						else
						{
							echo json_encode(array('success' => '0' , 'message' => "Something went wrong A"));
						}
					}
					else
					{
						echo json_encode(array('success' => '0' , 'message' => "We'are unable to send OTP, please make sure that your contact details are uptodate: ".$carrier));
					}
				}
				else
				{
					echo json_encode(array('success' => '0' , 'message' => "Dear Writer neither your mobile number nor your email id is registered with SWA. Please contact SWA office in order to web sign up"));
				}

			}
			else
			{
				echo json_encode(array('success' => '0' , 'message' => "You've already signed up, please Sign In"));
			}
		}
		else
		{
			echo json_encode(array('success' => '0' , 'message' => 'User not found' ));
		}
		break;

		case '6':
		$reg_no = reg_no_chk($_POST['reg_no']);
		//$mobile = $_POST['mobile'];
		$securecode = $_POST['securecode'];
		$sql = "SELECT * FROM `fwa_users` WHERE `reg_no` = '".$reg_no."'";

		$sql = "select fu.*,mem.* from `fwa_users` fu
		left join `fwa_members` mem on mem.reg_no = fu.reg_no
		where fu.reg_no = '".$reg_no."'";

		$result_mem = $result = mysqli_query($db,$sql);
		// var_dump(mysqli_num_rows($result_mem));die;
		if(mysqli_num_rows($result_mem) > 0)
		{
			$row = mysqli_fetch_assoc($result_mem);
			// var_dump($row);die;

			if($row['securecode'] == $securecode)
			{
				$_SESSION['is_login'] = 'true';
				$_SESSION['username'] = $reg_no;
				$_SESSION['name'] = $row['name'];
				$_SESSION['set_password'] = 'true';
				if($row['email'] != null && $row['email'] != '')
				{
					$_SESSION['User_Email'] = $row['email'];
				}
				else
				{
					$_SESSION['User_Email'] =  'web.admin@swaindia.org';
				}
				echo json_encode(array('success' => '1' , 'message' => '' ));
			}
			else
			{
				echo json_encode(array('success' => '0' , 'message' => 'Invalid Secure Code' ));
			}
		}
		else
		{
			echo json_encode(array('success' => '0' , 'message' => 'User not found' ));
		}
		break;

		case '7':
		$password = $_POST['password'];
		$reg_no = $_SESSION['username'];
		$sql = "UPDATE `fwa_users` SET `pass` = '".$password."' , `securecode` = '' , `password_flag` = 0  WHERE `reg_no` = '".$reg_no."'";
		if(mysqli_query($db,$sql))
		{
			echo json_encode(array('success' => '1' , 'message' => 'Success' ));
		}
		else
		{
			echo json_encode(array('success' => '0' , 'message' => 'Something went wrong' ));
		}
		break;

		case '3':

		$old_pass = $_POST['old_pass'];
		$new_pass = $_POST['new_pass'];
		$confirm_pass = $_POST['confirm_pass'];

		if($confirm_pass == $new_pass)
		{
		 	$reg_no = $_SESSION['username'];
			$sql = "SELECT * FROM fwa_users WHERE pass='".$old_pass."' and `reg_no` = '".$reg_no."'";

			// var_dump($sql);die;

			if($result = mysqli_query($db,$sql))
			{
				$row = mysqli_fetch_assoc($result);
				if(mysqli_num_rows($result)== 1)
				{
					$sql1 = "UPDATE fwa_users set pass='".$new_pass."' WHERE reg_no = '".$row['reg_no']."' and pass='".$old_pass."'";
					$result = mysqli_query($db,$sql1);
					if($result)
					{
						echo '1';
					}else{
						echo 'Something went wrong';
					}
				}
				else
				{
					echo 'Invalid username or password';
				}
			}
			else
			{
				echo 'Invalid username or password';
			}

		}
		else
		{
			echo 'Your Password is not matching';
		}

		break;

		case '4':
		include_once('functions.php');
		$email = $_POST['email'];
		$reg_no = $_POST['username'];
		$reg_no = reg_no_chk($reg_no);
		// $check_user = "select * from `fwa_users` where `reg_no` = '".$reg_no."' and `email` = '".$email."'";
		$check_user = "select fu.*,mem.* from `fwa_users` fu
		left join `fwa_members` mem on mem.reg_no = fu.reg_no
		where fu.reg_no = '".$reg_no."' and fu.email = '".$email."'";
		// var_dump($check_user);die;
		$result_chk_usr = mysqli_query($db,$check_user);
		if($result_chk_usr)
		{
			$count = mysqli_num_rows($result_chk_usr);
			$row = mysqli_fetch_assoc($result_chk_usr);
			// var_dump($row);die;
			$recName = $row['name'];
			if($count > 0)
			{
				$enc_reg_no = enc_num($reg_no);
				$body = "Dear ".$recName.",<br><br>
				We've received your request to reset your password. Please click on below link to proceed.<br><br>
				<a href='http://www.swaindia.org/resetpassword_change.php?q=".$enc_reg_no."'>Click Here To Reset</a><br><br>
				Team SWA India";
				$subject = "SWA India Password Reset";
				/** email id and password from where mail will be send */
				$username = 'assist@firsteconomy.com';
				$password = 'f3e6f3e6';

				$senderName = 'SWA India';
				$senderEmail = 'web@swaindia.org';
				$recName = '';
				$recEmail = $email;
				// $recEmail = 'nirmeet@firsteconomy.com';
				$result = sendMail($username,$password,$senderName,$senderEmail,$recName,$recEmail,$body,$subject);
				if($result)
				{
					echo '1';
				}
				else
				{
					echo '0';
				}
			}
			else
			{
				echo '2';		//user not found
			}
		}
		else
		{
			echo '0';    //something went wrong
		}

		break;

		case '5':
		include_once('functions.php');
		$password = $_POST['password'];
		$q = $_POST['q'];
		$reg_no = dec_num($q);
		$sql1 = "select * from `fwa_users` where reg_no = '".$reg_no."'";
		$result1 = mysqli_query($db,$sql1);
		if($result1)
		{
			if(mysqli_num_rows($result1) > 0)
			{
				$sql2 = "update `fwa_users` set pass='".$password."' WHERE reg_no = '".$reg_no."'";
				$result2 = mysqli_query($db,$sql2);
				if($result2)
				{
					echo '1';
				}
				else
				{
					echo '0';
				}
			}
			else
			{
				echo '2';
			}
		}
		else
		{
			echo '0';
		}

		break;

		case 8:
		// co auth step 1
		$coa_mem = reg_no_chk($_POST['coa_mem']);
		if(isset($_SESSION['coa']))
		{
			$coa_arr = $_SESSION['coa'];
		}
		else {
			$coa_arr = array();
		}

   if( !in_array( $coa_mem, array_column($coa_arr, 'membership')) )
	 {
		$sql = "select fu.*,mem.* from `fwa_users` fu
		left join `fwa_members` mem on mem.reg_no = fu.reg_no
		where fu.reg_no = '".$coa_mem."'";
		//var_dump($sql);
		$result = mysqli_query($db,$sql);
		if(mysqli_num_rows($result) > 0)
		{
			$row = mysqli_fetch_assoc($result);
			if($row['mobileno'] != '' && $row['mobileno'] != null)
			{
				$coa_mobile = $row['mobileno'];
				$securecode_coauthor = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 4);
				$_SESSION['securecode_coauthor'] = $securecode_coauthor;

				$name = $_SESSION['name'];
				// $smsapi = "http://www.myvaluefirst.com/smpp/sendsms?username=firsteconomy&password=firsteco&to=".$coa_mobile."&udh=0&from=SWASCO&text=".urlencode("Hi ".$row['name']." share your secure code ".$securecode_coauthor." to confirm as the co author with ".$name.".");

				//$sms_response = sms($smsapi);
				$usr_msg = "Hi ".$row['name']." share your secure code ".$securecode_coauthor." to confirm as the co author with ".$name.".";
				  $mobileNumber = $coa_mobile;
					$user = 'TRANSAC1';
					$key = 'cafbae387eXX';
					$message = urlencode ($usr_msg);
					$senderid = 'INFOSM';
					$accusage = 1;

				 	$url="pt.k9media.in/submitsms.jsp?user=".$user."&key=".$key."&mobile=".$mobileNumber."&senderid=".$senderid."&accusage=".$accusage."&message=".$message."";

					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$curl_response = curl_exec($ch);

					curl_close($ch);
          //var_dump($curl_response);

				//if(1)
				// if($sms_response == 'Sent.')
				// {
					$trim_mob = substr($coa_mobile, 0,1).'******'.substr($coa_mobile, -3);
					echo json_encode(array('success' => '1' , 'message' => 'A Secure Code is sent to co author mobile number '.$trim_mob.', please enter it here.' , 'coa_mem' => $coa_mem));
					 //echo json_encode(array('success' => '0' , 'message' => 'Something went wrong.' ));
				// }
				// else {
				// 	echo json_encode(array('success' => '0' , 'message' => 'Something went wrong.' ));
				// }
			}
			else
			{
				echo json_encode(array('success' => '0' , 'message' => 'Co Author mobile number is not registered with SWA, please contact SWA.' ));
			}
		}
		else {
			echo json_encode(array('success' => '0' , 'message' => 'User not found' ));
		}
	}
	else
	{
		echo json_encode(array('success' => '0' , 'message' => 'Co Author already added' ));
	}
		break;

		case 9:
		// co auth step 1
				$coa_mem = $_POST['coa_mem'];
				$coa_sc = $_POST['coa_sc'];
				if($coa_sc == $_SESSION['securecode_coauthor'])
				{
					$sql = "select fu.*,mem.* from `fwa_users` fu
									left join `fwa_members` mem on mem.reg_no = fu.reg_no
									where fu.reg_no = ".$coa_mem;
					$result = mysqli_query($db,$sql);
					$row = mysqli_fetch_assoc($result);
					$coa_arr = array(
															'name' => $row['name'],
															'membership' => $coa_mem,
															'mobileno' => $row['mobileno'],
															'email' => $row['email']
													);
					if(!isset($_SESSION['coa']))
					{
						$_SESSION['coa'] = array();
					}

					if(sizeof($_SESSION['coa']) <= 1 )
					{
						array_push($_SESSION['coa'],$coa_arr);
					}

					// $_SESSION['coa']['membership'] = $coa_mem;
					// $_SESSION['coa']['name'] = $row['name'];
					echo json_encode(array('success' => '1' , 'message' => 'Co Author added' ));
				 }
				 else {
				 	echo json_encode(array('success' => '0' , 'message' => 'Secure code incorrect' ));
				 }

		break;

		case 10:
			$coa_mem = $_POST['coa_mem'];
			$coa_arr = $_SESSION['coa'];
			$membership_arr = array_column($coa_arr, 'membership');
			$coa_index = array_search($coa_mem,$membership_arr);
			unset($coa_arr[$coa_index]);
			$_SESSION['coa'] = array_values($coa_arr);
			echo json_encode(array('success' => '1' ));
		break;

		case 11:
					include_once('functions.php');
					if(!empty($_POST['name']))
					{
						$name = $_POST['name'];
					}
					else
					{
						$name = '';
					}

					if(!empty($_POST['email']))
					{
						$email = $_POST['email'];
					}
					else
					{
						$email = '';
					}

					if(!empty($_POST['ismember']))
					{
						$ismember = $_POST['ismember'];
					}
					else
					{
						$ismember = '';
					}

					if(!empty($_POST['reg_no']))
					{
						$reg_no = $_POST['reg_no'];
					}
					else
					{
						$reg_no = '';
					}

					if(!empty($_POST['subject']))
					{
						$subject = $_POST['subject'];
					}
					else
					{
						$subject = '';
					}
					if(!empty($_POST['comments']))
					{
						$comments = $_POST['comments'];
					}
					else
					{
						$comments = '';
					}

					switch ($subject) {
						case 'Query':
							$rec_email='contact@swaindia.org';
							$subject = 'Query';
						break;

						case 'Feedback':
							$rec_email='contact@swaindia.org';
							$subject = 'Feedback';
						break;

						case 'Other':
							$rec_email='contact@swaindia.org';
							$subject = 'Other';
						break;

						case 'Legal_Consultation':
							$rec_email='legal.officer@swaindia.org';
							$subject = 'Legal Officer - Consultation';
						break;

						case 'Legal_Officer':
							$rec_email='legal.officer@swaindia.org';
							$subject = 'Legal Officer - Appointment';
						break;

						case 'Dsc':
							$rec_email='office@swaindia.org';
							$subject = 'To The DSC';
						break;

						case 'Tech_Sign':
							$rec_email='web.admin@swaindia.org';
							$subject = 'Technical Support - Not Able to Sign In/Sign Up';
						break;

						case 'Tech_Reg':
							$rec_email='web.admin@swaindia.org';
							$subject = 'Technical Support - Online Script Registration';
						break;

						default:
							$rec_email='contact@swaindia.org';
							$subject = 'Other';
						break;
					}

					$body = "An enquiry was placed for ".$subject." with following details:<br><br>
									Name: ".$name."<br>
									Email: ".$email."<br>
									Membership No.: ".$reg_no."<br>
									Comments: ".$comments."<br>
									Subject: ".$subject."<br><br>";
					$subject = "Enquiry Placed on SWA Website for ".$subject;
					/** email id and password from where mail will be send */
					$username = 'assist@firsteconomy.com';
					$password = 'f3e6f3e6';

					$senderName = $name;
					$senderEmail = $email;
					$recName = "Team SWA";
					//$recEmail = 'nirmeet@firsteconomy.com';   //for testing
					$result = sendMail($username,$password,$senderName,$senderEmail,$recName,$rec_email,$body,$subject);

					if($result)
					{
						$sql = "INSERT INTO `contact_form` (`name`, `email`, `reg_id`, `ismember`, `comments`, `subject`, `date_created`)
										VALUES ('".$name."', '".$email."', '".$reg_no."', '".$ismember."', '".$comments."', '".$subject."', now())";
						if(mysqli_query($db,$sql))
						{
							echo '1';
						}
						else {
							echo '0';
						}
					}
					else
					{
						echo '0';
					}
		break;

		case 15:
					include_once('functions.php');
					// $reg_no = $_SESSION['username'];
					if(!empty($_SESSION['username']))
					{
						$reg_no = $_SESSION['username'];
					}
					else
					{
						echo 0;
						exit;
					}
					if(!empty($_POST['name']))
					{
						$name = $_POST['name'];
					}
					else
					{
						$name = '';
					}
					if(!empty($_POST['phone']))
					{
						$phone = $_POST['phone'];
					}
					else
					{
						$phone = '';
					}
					if(!empty($_POST['mobileno']))
					{
						$mobileno = $_POST['mobileno'];
					}
					else
					{
						$mobileno = '';
					}
					if(!empty($_POST['dob']))
					{
						$dob = $_POST['dob'];
					}
					else
					{
						$dob = '';
					}
					if(!empty($_POST['email']))
					{
						$email = $_POST['email'];
					}
					else
					{
						$email = '';
					}
					if(!empty($_POST['add1']))
					{
						$add1 = $_POST['add1'];
					}
					else
					{
						$add1 = '';
					}
					if(!empty($_POST['add2']))
					{
						$add2 = $_POST['add2'];
					}
					else
					{
						$add2 = '';
					}

					if(!empty($_POST['city']))
					{
						$city = $_POST['city'];
					}
					else
					{
						$city = '';
					}

					if(!empty($_POST['pin']))
					{
						$pin = $_POST['pin'];
					}
					else
					{
						$pin = '';
					}
					if(!empty($_POST['state']))
					{
						$state = $_POST['state'];
					}
					else
					{
						$state = '';
					}

					if(!empty($_POST['sadd1']))
					{
						$sadd1 = $_POST['sadd1'];
					}
					else
					{
						$sadd1 = '';
					}
					if(!empty($_POST['sadd2']))
					{
						$sadd2 = $_POST['sadd2'];
					}
					else
					{
						$sadd2 = '';
					}

					if(!empty($_POST['scity']))
					{
						$scity = $_POST['scity'];
					}
					else
					{
						$scity = '';
					}

					if(!empty($_POST['spin']))
					{
						$spin = $_POST['spin'];
					}
					else
					{
						$spin = '';
					}
					if(!empty($_POST['sstate']))
					{
						$sstate = $_POST['sstate'];
					}
					else
					{
						$sstate = '';
					}

					if(!empty($_POST['address_use']))
					{
						$address_in_use = $_POST['address_use'];
					}
					else
					{
						$address_in_use = 1;
					}

					// mysqli_autocommit($db,FALSE);
				 	$sql_members = "UPDATE `fwa_members` SET `add1` = '".$add1."' , `add2` = '".$add2."' ,`city` = '".$city."' , `pin` = '".$pin."' , `state` = '".$state."' , `sadd1` = '".$sadd1."' , `sadd2` = '".$sadd2."' ,`scity` = '".$scity."' , `spin` = '".$spin."' , `sstate` = '".$sstate."' , `address_in_use` = ".$address_in_use."  WHERE `reg_no` = '".$reg_no."'";


					// var_dump($sql_members);die;

					$res_1 = mysqli_query($db, $sql_members);

					$sql_sync = "INSERT INTO `members_data` (`reg_no`,`add1`,`add2`,`city`,`pin`,`state`,`sadd1`,`sadd2`,`scity` , `spin`, `sstate` , `address_in_use`) VALUES       ('".$reg_no."','".$add1."','".$add2."', '".$city."','".$pin."','".$state."' , '".$sadd1."' , '".$sadd2."' , '".$scity."' , '".$spin."' , '".$sstate."' , ".$address_in_use.") ON DUPLICATE KEY  UPDATE `add1` = '".$add1."' , `add2` = '".$add2."' ,`city` = '".$city."' , `pin` = '".$pin."' , `state` = '".$state."' , `sadd1` = '".$sadd1."' , `sadd2` = '".$sadd2."' ,`scity` = '".$scity."' , `spin` = '".$spin."' , `sstate` = '".$sstate."' , `address_in_use` = ".$address_in_use;

					if($res_1)
					{
						$res_2 = mysqli_query($ds, $sql_sync);
						if($res_2)
						{
							echo "1";
						}
						else {
							echo "A";
						}
					}
					else {
						echo "B";
					// 	// Rollback transaction
					// 	mysqli_rollback($db);
					}
					// Commit transaction
					// mysqli_commit($db);
					break;

					case 16:
								include_once('functions.php');
								// $reg_no = $_SESSION['username'];
								if(!empty($_SESSION['username']))
								{
									$reg_no = $_SESSION['username'];
								}
								else
								{
									echo 0;
									exit;
								}
								if(!empty($_POST['new_number']))
								{
									$new_mobile = $_POST['new_number'];
								}
								else
								{
									echo 0;
									exit;
								}

								$otp_mobile = substr(str_shuffle("0123456789"), 0, 4);
								$_SESSION['otp_mobile'] = $otp_mobile;
								$usr_msg = "Hey your OTP code for updating mobile number is ".$otp_mobile." Team SWA.";
								$mobileNumber = $new_mobile;
								// $mobileNumber = '9769828965';
								$user = 'TRANSAC1';
								$key = 'cafbae387eXX';
								$message = urlencode ($usr_msg);
								$senderid = 'INFOSM';
								$accusage = 1;

								$url="pt.k9media.in/submitsms.jsp?user=".$user."&key=".$key."&mobile=".$mobileNumber."&senderid=".$senderid."&accusage=".$accusage."&message=".$message."";

								// var_dump($url);die;

								$ch = curl_init($url);
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								// $curl_response = curl_exec($ch);       /* uncomment */
								$curl_response = 1;
								curl_close($ch);
								if($curl_response)
								{
									$_SESSION['new_number'] = $new_mobile;
									echo 1;
									echo $otp_mobile;
								}
								else {
									echo 0;
								}
					break;

					case 17:
								include_once('functions.php');
								// $reg_no = $_SESSION['username'];
								if(!empty($_SESSION['username']))
								{
									$reg_no = $_SESSION['username'];
								}
								else
								{
									echo 0;
									exit;
								}
								if(!empty($_POST['otp_mobile_usr']))
								{
									$otp_mobile_usr = $_POST['otp_mobile_usr'];
								}
								else
								{
									echo 0;
									exit;
								}

								if(!empty($_SESSION['new_number']))
								{
									$new_number = $_SESSION['new_number'];
								}
								else
								{
									echo 0;
									exit;
								}
								if(!empty($_SESSION['otp_mobile']))
								{
									$otp_mobile = $_SESSION['otp_mobile'];
								}
								else
								{
									echo 0;
									exit;
								}
								// var_dump($otp_mobile);
								// echo '|||';
								// var_dump($otp_mobile_usr);
								// die;
								if($otp_mobile == $otp_mobile_usr)
								{
									$sql_members = "UPDATE `fwa_members` SET `mobileno` = '".$new_number."' WHERE `reg_no` = '".$reg_no."'";

									// var_dump($sql_members);die;

									$res_1 = mysqli_query($db, $sql_members);

									// $sql_sync = "INSERT INTO `members_mobile` (`reg_no`,`mobile`) VALUES('".$reg_no."','".$new_number."')";
									$sql_sync = "INSERT INTO `members_mobile` (`reg_no`,`mobile`) VALUES ('".$reg_no."','".$new_number."') ON DUPLICATE KEY  UPDATE `reg_no` = '".$reg_no."' , `mobile` = '".$new_number."'";

									if($res_1)
									{
										$res_2 = mysqli_query($ds, $sql_sync);
										if($res_2)
										{
											unset($_SESSION['otp_mobile']);
											unset($_SESSION['new_number']);
											echo "1";
										}
										else {
											echo "A";
										}
									}
									else {
										echo "B";
									// 	// Rollback transaction
									// 	mysqli_rollback($db);
									}
								}
								else {
									echo 3; //otp incorrect
								}

								break;
								case 18:
											include_once('functions.php');
											// $reg_no = $_SESSION['username'];
											if(!empty($_SESSION['username']))
											{
												$reg_no = $_SESSION['username'];
											}
											else
											{
												echo 0;
												exit;
											}
											if(!empty($_POST['new_mail']))
											{
												$new_mail = $_POST['new_mail'];
											}
											else
											{
												echo 0;
												exit;
											}

											$otp_mail = substr(str_shuffle("0123456789"), 0, 4);
											$_SESSION['otp_mail'] = $otp_mail;

											$body = "Hey your OTP code for updating email id is ".$otp_mail."<br> Team SWA.";
											$subject = "SWA OTP for updating Email Id";
											/** email id and password from where mail will be send */
											$username = 'assist@firsteconomy.com';
											$password = 'f3e6f3e6';

											$senderName = 'SWA India';
											$senderEmail = 'contact@swaindia.org';
											$recName = '';
											$rec_email = $new_mail;
											// $recEmail = 'nirmeet@firsteconomy.com';   //for testing
											$result = sendMail($username,$password,$senderName,$senderEmail,$recName,$rec_email,$body,$subject);


											if($result)
											{
												$_SESSION['new_email'] = $new_mail;
												echo 1;
												echo $otp_mail;
											}
											else {
												echo 0;
											}
								break;
								case 19:
											include_once('functions.php');
											// $reg_no = $_SESSION['username'];
											if(!empty($_SESSION['username']))
											{
												$reg_no = $_SESSION['username'];
											}
											else
											{
												echo 'Q';
												exit;
											}
											if(!empty($_POST['otp_mail_usr']))
											{
												$otp_mail_usr = $_POST['otp_mail_usr'];
											}
											else
											{
												echo 'W';
												exit;
											}

											if(!empty($_SESSION['new_email']))
											{
												$new_mail = $_SESSION['new_email'];
											}
											else
											{
												echo 'E';
												exit;
											}
											if(!empty($_SESSION['otp_mail']))
											{
												$otp_mail = $_SESSION['otp_mail'];
											}
											else
											{
												echo 'R';
												exit;
											}
											// var_dump($otp_mobile);
											// echo '|||';
											// var_dump($otp_mobile_usr);
											// die;
											if($otp_mail == $otp_mail_usr)
											{
												$sql_members = "UPDATE `fwa_users` SET `email` = '".$new_mail."' WHERE `reg_no` = '".$reg_no."'";

												// var_dump($sql_members);die;

												$res_1 = mysqli_query($db, $sql_members);

												// $sql_sync = "INSERT INTO `members_email` (`reg_no`,`email`) VALUES('".$reg_no."','".$new_mail."')";
												$sql_sync = "INSERT INTO `members_email` (`reg_no`,`email`) VALUES ('".$reg_no."','".$new_mail."') ON DUPLICATE KEY  UPDATE `reg_no` = '".$reg_no."' , `email` = '".$new_mail."'";

												if($res_1)
												{
													$res_2 = mysqli_query($ds, $sql_sync);
													if($res_2)
													{
														unset($_SESSION['otp_mail']);
														unset($_SESSION['new_mail']);
														echo "1";
													}
													else {
														echo "A";
													}
												}
												else {
													echo "B";
												// 	// Rollback transaction
												// 	mysqli_rollback($db);
												}
											}
											else {
												echo 3; //otp incorrect
											}

											break;



		default:
		echo 'invalid ajax';
		break;

	}
}
else
{
	echo 'invalid access';
}


function sms($smsapi) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, $smsapi);
	curl_setopt($ch, CURLOPT_POST, 1);
	// curl_setopt($ch, CURLOPT_MUTE, TRUE);
	$data = curl_exec($ch);
	// if(false == $data){
	// 	echo curl_error($ch);
	// }
	curl_close($ch);

	return $data;
}

function send_mail($reciever,$subject,$body,$sender_email,$sender_password)
{
	include_once('../PHPMailerAutoload.php');
	$mail = new PHPMailer(true);
	$mail->IsSMTP(); // telling the class to use SMTP
	$mail->IsHTML(true); //
	$mail->SMTPAuth = true; // enable SMTP authentication
	$mail->SMTPSecure = "ssl"; // sets the prefix to the servier
	$mail->Host = "smtp.gmail.com"; // sets GMAIL as the SMTP server
	$mail->Port = 465; // set the SMTP port for the GMAIL server

	$mail->Username = $sender_email; // GMAIL username
	$mail->Password = $sender_password; // GMAIL password
	$mail->AddAddress($reciever);
	// $mail->AddBcc('nirmeet@firsteconomy.com');
	$mail->Subject = $subject;
	$mail->SetFrom('web@swaindia.org','SWA India');
	$mail->Body = $body;
	try{
		$mail->Send();

		return '1';            //New register
	} catch(Exception $e){
		return '0';			// something went wrong
	}
}

function reg_no_chk($reg_no)
{
	$length = strlen($reg_no);

	switch ($length) {
		case 1:
		$reg_no = '00000'.$reg_no;
		break;

		case 2:
		$reg_no = '0000'.$reg_no;
		break;

		case 3:
		$reg_no = '000'.$reg_no;
		break;

		case 4:
		$reg_no = '00'.$reg_no;
		break;

		case 5:
		$reg_no = '0'.$reg_no;
		break;

	}

	return $reg_no;
}

?>
