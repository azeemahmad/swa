<?php session_start();
include_once('../includes/config.php');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('Asia/Calcutta');

if(!empty($_POST['ajax']))
{
    $ajax = $_POST['ajax'];
    switch ($ajax) {

        case '1': /**  ISC login as member*/
        $username = $_POST['username'];
        $username =	reg_no_chk($username);
        $password = $_POST['password'];
        $sql = "select fu.*,mt.* ,mem.*
        from `fwa_users` fu
        left join `mem_type` mt on mt.type_id = fu.type_id
        left join `fwa_members` mem on mem.reg_no = fu.reg_no
        where fu.reg_no = '".$username."' and fu.pass = '".$password."' and fu.pass != ''";



        if($result = mysqli_query($db,$sql))
        {
            $row = mysqli_fetch_assoc($result);
            if(mysqli_num_rows($result) > 0)
            {
                if($row['type_id'] == 2 || $row['type_id'] ==3 || $row['type_id'] ==6 || $row['type_id'] ==10)
                {
                    echo 'You can not log in as a SWA member your records shows '.$row['type'].' as Memebership type.<br>Please contact SWA for more info';
                }
                else
                {
                    $exp_date = date($row['membership_expiry_date']);

                    if($row['type_id'] != 1 && $row['type_id'] != 4 && $row['type_id'] != 7 )// if member types are not Life / Senior - R / Senior - R
                    {
                        $today_date = strtotime("today midnight"); // unix time stamp
                        // $today_date = strtotime(date("Y-m-d")); // unix time stamp
                        // var_dump($today_date);die;
                        $exp_date = strtotime($row['membership_expiry_date']);
                        $_SESSION['exp_date'] = date("d-m-Y", $exp_date);
                        // var_dump(strtotime("+7 day",$today_date));die;
                        if($exp_date >= $today_date)
                        {
                            $_SESSION['exp_flag'] = 0;	//not expired
                            $_SESSION['isc_login'] = 'true';
                            $_SESSION['username'] = $username;
                            $_SESSION['name'] = $row['name'];
                            $_SESSION['type_id'] = $row['type_id'];
                            $_SESSION['isc_type'] = 1; //1 for member
                            if($row['email'] != null && $row['email'] != '')
                            {
                                $_SESSION['User_Email'] = $row['email'];
                            }
                            else
                            {
                                $_SESSION['User_Email'] =  'web.admin@swaindia.org';
                            }
                        }
                        else
                        {
                            $_SESSION['exp_flag'] = 1; //expired
                            echo 'Membership expired';
                            exit;
                        }
                    }
                    else
                    {
                        $_SESSION['exp_date'] = 'N/A'; // no expiry date
                        $_SESSION['exp_flag'] = 0;//not expired

                        $_SESSION['isc_login'] = 'true';
                        $_SESSION['username'] = $username;
                        $_SESSION['name'] = $row['name'];

                        $_SESSION['type_id'] = $row['type_id'];
                        $_SESSION['isc_type'] = 1; //1 for member

                    }

                    echo 'Success';
                }

            }
            else
            {
                echo 'Invalid username or password';
            }
        }
        else
        {
            echo 'Something went wrong';
        }
        break;

        case '2': /**  ISC login as gurst*/
            $_SESSION['isc_login'] = 'true';
            $_SESSION['username'] = 'Guest';
            $_SESSION['name'] = 'Guest';
            $_SESSION['exp_flag'] = 0;	//not expired
            $_SESSION['type_id'] = 0;
            $_SESSION['isc_type'] = 2; //2 for guest
            echo 'Success';
        break;

        case '3': /**  ISC login as gurst*/
            $_SESSION['isc_login'] = 'true';
            $_SESSION['username'] = 'Student';
            $_SESSION['name'] = 'Student';
            $_SESSION['exp_flag'] = 0;	//not expired
            $_SESSION['type_id'] = 0;
            $_SESSION['isc_type'] = 3; //3 for student
            echo 'Success';
        break;

    }
}


function reg_no_chk($reg_no)
{
	$length = strlen($reg_no);

	switch ($length) {
		case 1:
		$reg_no = '00000'.$reg_no;
		break;

		case 2:
		$reg_no = '0000'.$reg_no;
		break;

		case 3:
		$reg_no = '000'.$reg_no;
		break;

		case 4:
		$reg_no = '00'.$reg_no;
		break;

		case 5:
		$reg_no = '0'.$reg_no;
		break;

	}

	return $reg_no;
}

?>
