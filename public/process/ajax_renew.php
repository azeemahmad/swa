<?php session_start();
include_once('../includes/config.php');
include_once('../includes/config_sync.php');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('Asia/Calcutta');
if($_GET){
  die('invalid access');
}
else
{
  try {

    if(!empty($_POST['duration']))
    {
      $duration =  $_POST['duration'];
    }
    else
    {
      echo "0";
      exit(0);
    }

    if(!empty($_POST['new_card_status']))
    {
      $new_card_status =  $_POST['new_card_status'];
    }
    else {
      $new_card_status = 0;
    }

    if(!empty($_POST['card_post_status']))
    {
      $card_post_status = $_POST['card_post_status'];
    }
    else
    {
      $card_post_status = 0;
    }


    if($new_card_status)
    {
      $new_card_charge = 50;

      if($card_post_status)
      {
        $card_post_charge = 50;
      }
      else {
        $card_post_charge = 0;
      }
    }
    else {
      $new_card_charge = 0;
      $card_post_charge = 0;
    }

    if($_SESSION['monthly_pay'] == 0) {

      $mem_renew_charge = $_SESSION['mem_renew_charge'];        // renew fesss per year
      $current_exp_date = $_SESSION['current_exp_date'];        // expiry date (before renew)
      $mem_renew_penelty = $_SESSION['mem_renew_penelty'];       // late fees

      $total_mem_renew_charge = $mem_renew_charge * $duration;
      $_SESSION['total_mem_renew_charge'] = $total_mem_renew_charge;


      $new_exp_date = date('d-m-Y', strtotime($current_exp_date.' +'.$duration.' years')); //adding +duration years along with the $current_exp_date

      $grand_total = $total_mem_renew_charge + $mem_renew_penelty + $new_card_charge + $card_post_charge;

      $_SESSION['new_exp_date'] = $new_exp_date;
      $_SESSION['grand_total'] = $grand_total;

      echo json_encode(
         array('total_mem_renew_charge' => $total_mem_renew_charge , 'new_card_charge' => $new_card_charge , 'card_post_charge' => $card_post_charge, 'grand_total' => $grand_total,'new_exp_date' => $new_exp_date , 'error_flag'=>0 , 'monthly_pay' =>0)
      );

    }
    else {
        $total_mem_renew_charge = $_SESSION['final_fees'];
        $mem_renew_penelty = $_SESSION['mem_renew_penelty'];       // late fees
        $grand_total = $total_mem_renew_charge + $mem_renew_penelty + $new_card_charge + $card_post_charge;
        $_SESSION['grand_total'] = $grand_total;

        echo json_encode(
           array('new_card_charge' => $new_card_charge , 'card_post_charge' => $card_post_charge, 'grand_total' => $grand_total, 'error_flag'=>0 , 'monthly_pay' =>1)
        );

    }

  }catch (\Exception $e) {
    echo json_encode(
       array('error_flag'=>1)
    );
  }
}
