<?php
if (!isset($_SESSION))
	session_start();
	include('Cryptoif.php');

	// production keys
	//$working_key='E93C7C8FA026C576DCD2941297B6E255';		//Working Key should be provided here.

	//test keys
	$working_key='BFCC3D4DF0DEA863AFB73F9DAF8B5C18';//Shared by CCAVENUES
	$encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server

	$rcvdString=decrypt($encResponse,$working_key);		//Crypto Decryption used as per the specified working key.
	//var_dump($rcvdString);die;
	$order_status="";
	$decryptValues=explode('&', $rcvdString);
	$dataSize=sizeof($decryptValues);
	// echo "<center>";
	// echo '<pre>';
	// var_dump($decryptValues);
	// echo '</pre>';

	for($i = 0; $i < $dataSize; $i++)
	{
		$information=explode('=',$decryptValues[$i]);
		if($i==3)	$order_status=$information[1];
		if($i==0)	$order_id=$information[1];
		if($i==1)	$trk_id=$information[1];
		if($i==2)	$bnk_ref=$information[1];
		if($i==4)	$f_msg=$information[1];
		if($i==5)	$p_mode=$information[1];
		if($i==6)	$c_name=$information[1];
		if($i==10)	$amt=$information[1];
		if($i==11)	$b_name=$information[1];
		if($i==18)	$b_email=$information[1];
		if($i==26)	$stype=$information[1];
		if($i==27)	$regno=$information[1];
		if($i==28)	$title=$information[1];
		if($i==29)	$ses_id=$information[1];
		if($i==30)	$order_id=$information[1];
		if($i==25)	$dcl=$information[1];
	}
//var_dump($order_status);
	if($order_status==="Success")
	{
		$tr_tbl = "s_tran";

        $orderid_arr =  explode('#', $order_id);
		$file_count = sizeof($orderid_arr);

		include("includes/config.php");
		// var_dump($_SESSION);die;
		$sql = "INSERT INTO s_tran(`reg_no`, `date`, `orderid`, `trackingid`, `bankrefno`, `orderstatus`, `failurmsg`, `payment_mode`, `cardname`, `amount`, `billing_name`, `bill_email`,`multiple_upload_count`) VALUES ('".$regno."', '".date("Y-m-d")."', '".$order_id."', '".$trk_id."', '".$bnk_ref."', '".$order_status."', '".$f_msg."', '".$p_mode."', '".$c_name."', '".$amt."', '".$b_name."', '".$b_email."', '".$file_count."')";
		$result2 = mysqli_query($db,$sql);
		$_SESSION['prn'] = $regno;
		$_SESSION['name'] =$b_name ;
		$_SESSION['User_Email'] = $b_email ;
		$_SESSION['orderid']= preg_replace('/[^A-Za-z0-9\-]/', '',$orderid_arr);
		$_SESSION['sess_id']=  preg_replace('/[^A-Za-z0-9\-]/', '',$ses_id);
		$_SESSION['stype']= $stype ;
		$_SESSION['title']= $title ;
		$_SESSION['trk_id']=$trk_id;
		$_SESSION['dcl']= preg_replace('/[^A-Za-z0-9\-]/', '',$dcl);

        header("location:scriptregcomp2.php");

	}
	else if($order_status==="Aborted")
	{
		$tr_tbl = "f_tran";

        include("includes/config.php");
		$sql= "INSERT INTO f_tran(`reg_no`, `date`, `orderid`, `trackingid`, `bankrefno`, `orderstatus`, `failurmsg`, `payment_mode`, `cardname`, `amount`, `billing_name`, `bill_email`) VALUES ('".$regno."', '".date("Y-m-d")."', '".$order_id."', '".$trk_id."', '".$bnk_ref."', '".$order_status."', '".$f_msg."', '".$p_mode."', '".$c_name."', '".$amt."', '".$b_name."', '".$b_email."')";

		$result2 = mysqli_query($db,$sql);

        $_SESSION['work_register']['payment_error'] = "Payment aborted. Please try again.";
        header("location:register_script_p.php");


	}
	else if($order_status==="Failure")
	{
		$tr_tbl = "f_tran";

        include("includes/config.php");
		$sql= "INSERT INTO f_tran(`reg_no`, `date`, `orderid`, `trackingid`, `bankrefno`, `orderstatus`, `failurmsg`, `payment_mode`, `cardname`, `amount`, `billing_name`, `bill_email`) VALUES ('".$regno."', '".date("Y-m-d")."', '".$order_id."', '".$trk_id."', '".$bnk_ref."', '".$order_status."', '".$f_msg."', '".$p_mode."', '".$c_name."', '".$amt."', '".$b_name."', '".$b_email."')";

		$result2 = mysqli_query($db,$sql);

        $_SESSION['work_register']['payment_error'] = "Payment failed. Please try again after sometime.";
        header("location:register_script_p.php");


	}
	else
	{
		//echo "<br>Security Error. Illegal access detected";

        include("includes/config.php");
		$sql= "INSERT INTO f_tran(`reg_no`, `date`, `orderid`, `trackingid`, `bankrefno`, `orderstatus`, `failurmsg`, `payment_mode`, `cardname`, `amount`, `billing_name`, `bill_email`) VALUES ('".$regno."', '".date("Y-m-d")."', '".$order_id."', '".$trk_id."', '".$bnk_ref."', '".$order_status."', '".$f_msg."', '".$p_mode."', '".$c_name."', '".$amt."', '".$b_name."', '".$b_email."')";

		$result2 = mysqli_query($db,$sql);

        $_SESSION['work_register']['payment_error'] = "Something went wrong. Please try again after sometime.";
        header("location:register_script_p.php");


	}

	/*if($order_status==="Success")
	{
		$orderid_arr =  explode('#', $order_id);
		$file_count = sizeof($orderid_arr);

		include("includes/config.php");
		// var_dump($_SESSION);die;
		$sql = "INSERT INTO s_tran(`reg_no`, `date`, `orderid`, `trackingid`, `bankrefno`, `orderstatus`, `failurmsg`, `payment_mode`, `cardname`, `amount`, `billing_name`, `bill_email`,`multiple_upload_count`) VALUES ('".$regno."', '".date("Y-m-d")."', '".$order_id."', '".$trk_id."', '".$bnk_ref."', '".$order_status."', '".$f_msg."', '".$p_mode."', '".$c_name."', '".$amt."', '".$b_name."', '".$b_email."', '".$file_count."')";
		$result2 = mysqli_query($db,$sql);
		$_SESSION['prn'] = $regno;
		$_SESSION['name'] =$b_name ;
		$_SESSION['User_Email'] = $b_email ;
		$_SESSION['orderid']= preg_replace('/[^A-Za-z0-9\-]/', '',$orderid_arr);
		$_SESSION['sess_id']=  preg_replace('/[^A-Za-z0-9\-]/', '',$ses_id);
		$_SESSION['stype']= $stype ;
		$_SESSION['title']= $title ;
		$_SESSION['trk_id']=$trk_id;
		$_SESSION['dcl']= preg_replace('/[^A-Za-z0-9\-]/', '',$dcl);

		//echo $_SESSION['prn']." = ".$regno."<br>";
		//echo $_SESSION['name']." = ".$b_name."<br>" ;
	 	// echo  $_SESSION['User_Email']." = ".$b_email."<br>" ;
		//echo $_SESSION['orderid']." = ".trim($order_id,"")."<br>";
		//echo $_SESSION['sess_id']." = ".$ses_id."<br>" ;
		//echo $_SESSION['stype']." = ". $stype."<br>" ;
		//echo $_SESSION['title']." = ".$title."<br>" ;

		header("location:scriptregcomp2.php");

	}
	else
	{
		include("includes/config.php");
		$sql= "INSERT INTO f_tran(`reg_no`, `date`, `orderid`, `trackingid`, `bankrefno`, `orderstatus`, `failurmsg`, `payment_mode`, `cardname`, `amount`, `billing_name`, `bill_email`) VALUES ('".$regno."', '".date("Y-m-d")."', '".$order_id."', '".$trk_id."', '".$bnk_ref."', '".$order_status."', '".$f_msg."', '".$p_mode."', '".$c_name."', '".$amt."', '".$b_name."', '".$b_email."')";

		$result2 = mysqli_query($db,$sql);
		echo "<center> Sorry We have not received your payment.....<br> Registration failed.....<br><a href='register_script.php'> Try again</a>";
	}*/

	?>
