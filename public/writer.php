<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href=""/>
  <title>SWA Membership</title>
  <link href="css/custom.css" rel="stylesheet" type="text/css">
  <link href="css/color.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
  <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>

      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body class="inner-page">
      <div id="wrapper">
       <?php include_once('header.php'); ?>
       <div id="cp-content-wrap">
        <div class="banner_inner">
          <img src="images/bg.jpg">
        </div>
        <div class="cp_our-story-section">
         <div class="container">
          <div class="row">
           <div class="col-md-12">
            <div class="cp-acticle-box abt-div">
              <h1 class="title bold text-center">
                THE WRITERS CHARTER
              </h1>
              <p class="text-justify">The writer is a primary creator of works of imagination, entertainment and enlightenment; a significant initiator of cultural, social and economic processes. These are of basic importance in all societies.</p>
             
              <div class="cp-acticle-box" id="membership-form">
               <p class="text-justify">To fulfil effectively these social responsibilities, the writer must have the following rights:</p>
                <ol class="listol">
                  <li class="sub-mem-li"><p class="text-justify">The right to be acknowledged, legally, morally and contractually, as the author of one's work.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">The right to complete freedom of expression and communication, and freedom from any form of censorship. </p></li>
                  <li class="sub-mem-li"><p class="text-justify">The right to maintain the integrity of a work and to protect it from any distortion or misuse. </p></li>
                  <li class="sub-mem-li"><p class="text-justify">The right to fair payment for all uses of a work. </p></li>
                  <li class="sub-mem-li"><p class="text-justify">The right to have the work published or produced solely on the basis of its merit, without regard to any form of invidious discrimination which shall include but not be limited to age, color, gender, marital status, national origin, physical or emotional instability, race, religion, sexual orientation, social or political affiliation or belief of the writer.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">The right to form unions or guilds entitled to bargain and act collectively for their members.</p>
                </ol> 
              </div>

              <div class="cp-acticle-box" id="membership-form">
               <p class="text-justify">Believing that these rights are fundamental to the dignity, integrity and well-being of writers, it is the policy of the International Affiliation of Writers Guilds of which Screenwriters Association, Mumbai, is a full member, to work for their attainment everywhere, and as part of this effort to take all necessary steps to achieve the following goals:</p>
                <ol class="listol">
                  <li class="sub-mem-li"><p class="text-justify">To work for copyright laws that are based on the concept that the creator of any work is its first owner, with inalienable legal and moral rights in that work that protect its use and integrity. </p></li>
                  <li class="sub-mem-li"><p class="text-justify">To ensure continuing participation of the writer in the preparation and production of a work for film, radio, television or the stage. This includes the casting, the selection of a director, and the right to be present when the work is being produced or presented.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">To fight all attempts by others to claim authorship or ownership in a work, including moves by producers, directors and corporate entities to be accorded ‘possessory’ credits.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">To encourage and maintain the distinct cultural identities of each country.</p></li>
                  <li class="sub-mem-li"><p class="text-justify">To oppose all attempts by governments, corporations, special interest groups, and others, to impose on creators censorship in any form. </p></li>
                  <li class="sub-mem-li"><p class="text-justify">To seek means to facilitate the free movement of writers in and between all nations. </p>
                  <li class="sub-mem-li"><p class="text-justify">To establish the conditions that will free the writer's imagination and fulfill his or her capacity for creative expression.</p>
                </ol> 
              </div>
        </div>   
      </div>
    </div>
  </div>
</div>
<?php include_once('footer.php'); ?>
</div>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/custom.js"></script>
<!-- <script type="text/javascript" src="js/jquery.mixitup.min.js"></script> -->
<!-- <script type="text/javascript">
  $(function () {

    var filterList = {

      init: function () {

        // MixItUp plugin
        // http://mixitup.io
        $('#portfoliolist').mixItUp({
          selectors: {
            target: '.portfolio',
            filter: '.filter' 
          },
          load: {
            filter: '.exe'  
          }     
        });               

      }

    };
    
    // Run the show!
    filterList.init();
    
    
  }); 
  
</script> -->
</body>
</html>