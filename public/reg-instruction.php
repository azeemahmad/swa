<?php session_start(); ?>
<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href=""/>
      <title>Film Writers Association | Instruction</title>
      <link href="css/custom.css" rel="stylesheet" type="text/css">
      <link href="css/color.css" rel="stylesheet" type="text/css">
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="css/responsive.css" rel="stylesheet" type="text/css">
      <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
      <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
      <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body class="inner-page">
      <div id="wrapper" class="inside-menu">
         <?php include_once('header.php'); ?>
         <div id="cp-content-wrap" class="page404 cp-login-page">
            <div class="container">
               <div class="row">
                  <div class="col-md-8 col-md-offset-2 main-reg-div">
                     <div class="cp-reg-box sub-reg-div">
                        <p><emp>C</emp>ongratulations Writer on your fresh Creation. We are pleased that you have decided to register your creation online with us. Please read and follow the instructions carefully. In case you have any problems with Registrations kindly go through the FAQs – most issues are addressed there. You may alternatively check the FAQs before registering to avoid any errors at a later stage..</p><br>
                        <h5>Registration Process</h5>
                        <ol>
                           <li>Select the Category of Creation to be registered viz. story, screenplay,song etc. and input the Title. Please note that one and only one Article can be registered at a time. Registrations of multiple Articles esp. songs, mukhadas in single transaction will lead to cancellation of the entire registration.</li>
                           <li>Kindly note in order to let members write and register in their choice of script Roman, Devnagari, Urdu, Tamil, Punjabi etc. we have made the design of the new system Universal that it will accept PDF as input file and deliver stamped and digitally signed PDF as output file. You can straightway save or print this output file and get registered copy just the same way as you do in the office. So here you browse & select the PDF of the story/script/ screenplay/song that you want to register.</li>
                           <li>
                              <h5>Rules for the PDF to be accepted by the system</h5>
                              <ul>
                                 <li>All the PDF must be A4 size with portrait orientation (landscape not allowed).</li>
                                 <li>There must be a margin of 1 inch on top and bottom of every page of your work.</li>
                                 <li>Please <strong>do not</strong> protect your file using any password. Password protected file will not be processed by system.</li>
                                 <li>Please <strong>do not</strong> include any Header or Footer on your fi le. They will be inserted by the system as proof of registration.</li>
                                 <li>Our system accepts only Standard PDF compression . Most inbuilt PDF convertors that come with official licensed MS Word, Mac Word, etc. yield output files with standard compressions. However, files converted using some Online Freeware, have different compressions and might lead to errors during Registrations.</li>
                              </ul>
                           </li>
                           <li>The page also presents a declaration form. Please go through the form and check on the appropriate box that applies to the work you intend to register. Your registration will be subject to this declaration and incomplete without it.</li>
                           <li>After all this is done, please recheck everything once. If everything is fine, click the SUBMIT button.</li>
                           <li>The system will compute the registration charges and will show you the payment screen. If the number of pages is shown incorrect, means there is something wrong. Don’t proceed with the Registration. Please read <a href="faqs.php">FAQs</a> - follow the tips given there, try some other pdf convertor. If problem still persists kindly write to us.</li>
                           <li>If the payment is successful Registration System will apply header and footers to your uploaded file, add a certificate of registration with digital signature to your file and present you with a Download Link immediately after the registration is done. Please download your Registered Article by clicking on the link and save it safely on your hard drive. You may rename the file later, to help you identify it. After sometime the system will also send an Email to you with the Registered Article as attachment. Pl preserve this email.</li>
                           <li>You need to confirm the completion of registration process by clicking the link in mail sent to you by FWA. If you fail to click the link within 4 days the System will automatically consider the registration process as completed.</li>
                           <li> KINDLY NOTE that unlike the Previous System – the New System WILL NOT SAVE your work on the Website’s server. This is IN YOUR INTEREST to provide higher secrecy and security to your registered work that you are the only one who has knowledge and information of your work and no one else. Therefore you are responsible for safe keeping of your Registered Files. Please keep the softcopy saved safely in your computer’s hard-disk and also on a portable device. Soft copies will be useful for verification of digital signatures and quick comparison of the scripts in case of disputes. You may also keep a hard-copy for your record if you prefer that. It is advisable to share only the registered softcopies with other writers/film-makers because they will have relevant proofs of registration viz. certificate, stamp QR code and the digital signature.</li>  
                        </ol>
                        <br>
                        <p>Please note that any attempt to tamper with the document will invalidate the digital signature and cancel the registration. The file should be kept as it is and if there is considerable change in the Creation in future it is advisable to get it registered afresh</p>
                        <br>
                        <p class="text-center">
                        <strong>I have already read and understood everything thoroughly,</strong> <br><br><br>
                        <a href="login.php" class="proced-btn">Proceed to Login</a>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include_once('footer.php'); ?>
      </div>
      <script src="js/jquery-1.11.3.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/jquery.bxslider.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/custom.js"></script>
   </body>
</html>