<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/x-icon" href="images/favicon.png"/>
	<title>Home </title>
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/color.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="css/responsive.css" rel="stylesheet" type="text/css">
	<link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css" rel="stylesheet" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/vertical.news.slider.css?v=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.css" />
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
     <style type="text/css">
     	.bx-wrapper .bx-controls-direction a{
     		z-index: 999!important;
     	}
      header{
        background:#000;
      }
      #cp-banner-style-1{
        margin-top:140px;
        width:60%;
      }
      .cp-content-wrap{
        margin-top:-14px;
        padding: 0px !important;
      }
     </style>
  </head>
  <body class="homepage">
  	<div id="wrapper">
      <?php include_once('header.php'); ?>
  <div id="cp-banner-style-1">
  	<ul id="cp-banner-1">
    <li onclick="window.location.href = 'article.php'">
        <img src="images/banner-img-2.jpg" alt="img">
        <div class="caption">
          <div class="container">
            <div class="holder">
              <ul>
                <li>Reading</li>
              </ul>
              <h1>Film Writers Association: The Diamond Years
              </h1>
            </div>
          </div>
        </div>
      </li>
    <li onclick="window.location.href = 'article_dyn.php?q=TXc'">
        <img src="images/whosescriptisit.jpg" alt="img">
        <div class="caption">
          <div class="container">
            <div class="holder">
              <ul>
                <li>EDITOR'S COLUMN</li>
              </ul>
              <h1>Whose Script Is it, Anyway?</h1>
            </div>
          </div>
        </div>
      </li>
      
  		<!-- <li>
  			<img src="images/banner-img-1.jpg" alt="img">
  			<div class="caption">
  				<div class="container">
  					<div class="holder">
  						<ul>
  							<li>Old is Gold</li>
  						</ul>
  						<h1>Sholay - 3D (2014) Theatrical Trailer</h1>
  					</div>
  				</div>
  			</div>
  		</li> -->
  		<!-- <li>
  			<img src="images/banner-img-2.jpg" alt="img">
  			<div class="caption">
  				<div class="container">
  					<div class="holder">
  						<ul>
  							<li>Reading</li>
  						</ul>
  						<h1>Film Writers Association: The Diamond Years
  						</h1>
  					</div>
  				</div>
  			</div>
  		</li> -->
  		<!-- <li>
  			<img src="images/banner-img-3.jpg" alt="img">
  			<div class="caption">
  				<div class="container">
  					<div class="holder">
  						<ul>
  							<li>Old is Gold</li>
  						</ul>
  						<h1>
  						  Bollywood Classics
  						</h1>
  					</div>
  				</div>
  			</div>
  		</li> -->
      <li onclick="window.location.href = 'article_dyn.php?q=TkE'">
        <img src="images/newbanner.jpg" alt="img">
        <div class="caption">
          <div class="container">
            <div class="holder">
              <ul>
                <li>FILM DESK</li>
              </ul>
              <h1>
               KARTHICK NAREN: The Wonder Boy of Tamil Neo-Noir
              </h1>
            </div>
          </div>
        </div>
      </li>
  	</ul>
  </div>
  <div id="cp-content-wrap" class="cp-content-wrap">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-9 left_block col-xs-12">
  				<div class="content-inner">
  					<div class="page-section">
  						<div class="">
  							<h1 class="title bold text-center" data-bgtext="LATEST VIDEOS">
  								LATEST VIDEOS
  							</h1>
  						</div>
  					</div>
  				</div>
  				<div class="news-holder cf">
  					<ul class="news-headlines">
  						<li class="selected">WRITERS CONTRACTS: Protection or Exploitation (Part-1) </li>
  						<li>WRITERS CONTRACTS: Protection or Exploitation (Part-2)</li>
  						<li>WRITERS CONTRACTS: Protection or Exploitation (Part-3)</li>
  						
  					</ul>
  					<div class="news-preview">
  						<div class="news-content top-content">
                <article class="video">
                  <figure>
                    <a class="fancybox fancybox.iframe" href="https://youtu.be/uN0IHGbDp6M"><img class="videoThumb" src="images/vid1.png"></a>
                  </figure>
                </article>
              </div>
              <div class="news-content top-content">
                <article class="video">
                  <figure>
                    <a class="fancybox fancybox.iframe" href="https://youtu.be/4hGNWgBJxVA"><img class="videoThumb" src="images/vid2.png"></a>
                  </figure>
                </article>
              </div>
              <div class="news-content top-content">
                <article class="video">
                  <figure>
                    <a class="fancybox fancybox.iframe" href="https://youtu.be/Gpw8nUzqqdk"><img class="videoThumb" src="images/vid3.png"></a>
                  </figure>
                </article>
              </div>
              
              
  					</div>
  				</div>
  				<div class="cp-posts-style-1">
  					<div class="content-inner">
  						<div class="page-section">
  							<div class="">
  								<h1 class="title bold text-center">
  									
  								</h1>
  							</div>
  						</div>
  					</div>
  					<ul class="cp-posts-list" id="swa_article">
  						<li class="cp-post">
  							<div class="cp-thumb">
  								<img src="images/hp-1.jpg" alt="neo">
  								<div class="cp-post-hover" ><a data-toggle="tooltip" title="Share" data-placement="auto" href="article_dyn.php?q=TWc9PQ"><i class="fa fa-link"></i></a></div>
  							</div>
  							<div class="cp-post-base">
  								<div class="cp-post-content">
  									<h2 onclick="window.location.href = 'article_dyn.php?q=TWc9PQ'">Toilet - Ek Premkatha</h2>
  									<ul class="cp-post-meta">
  										<li><a href="article_dyn.php?q=TWc9PQ">From Film Desk</a></li>
  									</ul>
  									<p>Script Analysis
  									</p>
  									<a href="#" class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</a> 
  								</div>
  							</div>
  						</li>
  						<li class="cp-post">
  							<div class="cp-thumb">
  								<img src="images/hp-2.jpg" alt="neo">
  								<div class="cp-post-hover"> <a href="javascript:void(0)" data-toggle="tooltip" title="Share" data-placement="auto"><i class="fa fa-link"></i></a> </div>
  							</div>
  							<div class="cp-post-base">
  								<div class="cp-post-content">
  									<h2 onclick="window.location.href = 'article.php'">SWA HISTORY</h2>
  									<ul class="cp-post-meta">
  										<li><a href="article.php">FILM WRITERS ASSOCIATION: THE DIAMOND YEARS</a></li>
  									</ul>
  									<p>BASED ON FACTS CURTSEY OPENDER CHANANA.</p>
  									<a href="#" class="leave-comment"><i class="fa fa-comment-o"></i> Leave a Comment</a> 
  								</div>
  							</div>
  						</li>
  					</ul>
  				</div>
  				<div class="cp-posts-style-1">   
  					<div class="content-inner">
  						<div class="page-section">
  							<div class="">
  								<h1 class="title bold text-center" data-bgtext="NEWS & NOTICES">
  									UPDATES
  								</h1>
  							</div>
  						</div>
  					</div>      
  					<div class="owl-carousel" id="news">
  						<div class="item">
  							<figure class="snip0021">
                  <img src="images/news1.jpg" alt="sample22"/>
                  <figcaption>
                    <div><h4><span>Lucknow Girls</span> win First Prize</h4></div>
                    <div>
                      <p>International Short Film Screenwriting Competition</p>
                    </div>
                    <a href="#"></a>
                  </figcaption>     
                </figure>

  						</div>
  						<div class="item">
  							<figure class="snip0021 hover">
                  <img src="images/news2.jpg" alt="sample20"/>
                  <figcaption>
                    <div><h4>“A new chapter has started!” - <span>Javed Akhtar leisure</span></h4></div>
                    <div>
                      <p>Shri Javed Akhtar is the new Chairman of revamped IPRS</p>
                    </div>
                    <a href="#"></a>
                  </figcaption>     
                </figure>
  						</div>
  						<div class="item">
  							<figure class="snip0021">
                  <img src="images/news3.jpg" alt="sample21"/>
                  <figcaption>
                    <div><h4>Screen writing Workshop by <span>Anjum Rajabali</span></h4></div>
                    <div>
                      <p>Whistling Woods International (WWI), Film workshop</p>
                    </div>
                    <a href="#"></a>
                  </figcaption>     
                </figure>
  						</div>
  					</div>
  				</div> 
  			</div>
  			<div class="col-md-3 col-xs-12 sidemob"> <!-- removed class pd-top-70 -->
  				<div class="sidebar">
  					<div class="widget featured-posts">
  						<h3>SWA EVENTS</h3>
  						<div class="widget-content reviewwidget">
  							<div class="carousel-wrap">
  								<div class="owl-carousel" id="upcome">
                    <div class="item">
                      <a href="https://www.youtube.com/watch?v=xMISd7xBsCM&t=278s" class="desc_ImageOverlay" target="_blank" style="background-image: url('images/rev/swaeventsnew1.jpg');">
                        <div class="desc">
                          <h4>Jaideep Sahni at SWA</h4>
                          <p>
                            Read More
                          </p>
                        </div>
                      </a>
                    </div>

  									<div class="item">
  										<a href="https://www.facebook.com/population.first/videos/1434028089978781/" class="desc_ImageOverlay" target="_blank" style="background-image: url('images/rev/swaeventsnew.jpg');">
  											<div class="desc">
  												<h4>Lights Camera Patriarchy</h4>
  												<p>
  													Read More
  												</p>
  											</div>
  										</a>
  									</div>
  								</div>
  							</div>
  						</div>
  					</div>
  					<div class="widget archives-widget">
  						<h3>DESI SCRIPTWRITER</h3>
  						<div class="widget-content listwidget" data-toggle="modal" data-target="#sc_modal">
  							<img src="images/sw_th.jpg" style="max-width: 100%;border: 1px solid #c7c7c7;">
  						</div>
  					</div>
  					<div class="widget tags-widget">
  						<h3>CATEGORY</h3>
  						<div class="widget-content widmcontentmob">
  							<ul>
  								<li> <a href="">Film Desk</a> <a href="">Old is Gold</a> <a href="">Television Desk</a> <a href=""> Question & Answers</a> <a href="">Regional Desk</a> <a href="">Discussion Board</a></li>
  							</ul>
  						</div>
  					</div>
  					<!-- <div class="widget latest-stories-widget">
  						<h3>SWA REVIEWS</h3>
              <div class="widget-content reviewwidget">
                <div class="carousel-wrap">
                  <div class="owl-carousel" id="reviews">
                    <div class="item">
                      <a href="#" class="desc_ImageOverlay" style="background-image: url('images/rev/rev1.jpg');">
                        <div class="desc">
                          <h4>बहन होगी तेरी - "दर्शकों को पटाने में असफल!"</h4>
                          <p>
                            Click for full review
                          </p>
                        </div>
                      </a>
                    </div>
                    <div class="item">
                      <a href="#" class="desc_ImageOverlay" style="background-image: url('images/rev/rev2.jpg');">
                        <div class="desc">
                          <h4>राब्ता - "दर्शकों से नहीं बना पाई राब्ता"</h4>
                          <p>
                            Click for full review
                          </p>
                        </div>
                      </a>
                    </div>
                    <div class="item">
                      <a href="#" class="desc_ImageOverlay" style="background-image: url('images/rev/rev3.jpg');">
                        <div class="desc">
                          <h4>Noor - "Leaves audience be-noor"</h4>
                          <p>
                            Click for full review
                          </p>
                        </div>
                      </a>
                    </div>
                    <div class="item">
                      <a href="#" class="desc_ImageOverlay" style="background-image: url('images/rev/rev5.jpg');">
                        <div class="desc">
                          <h4>Wonder Woman - "Wonderful by Comparison; Ordinary on its Own"</h4>
                          <p>
                            Click for full review
                          </p>
                        </div>
                      </a>
                    </div>
                    <div class="item">
                      <a href="#" class="desc_ImageOverlay" style="background-image: url('images/rev/rev4.jpg');">
                        <div class="desc">
                          <h4>Saab Bahadur - "Suspense-thriller? Really?"</h4>
                          <p>
                            Click for full review
                          </p>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
                </div>
  					</div> -->
  				</div>
  			</div>
  		</div>
  	</div>
  </div>
  <?php include_once('footer.php'); ?>

</div>
<!-- Modal -->
<div id="sc_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content project-details-popup">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      <div class="modal-header">
        <img class="header-img" src="images/sw.jpg">
      </div>
    </div>
  </div>
</div>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/vertical.news.slider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js"></script>
<script type="text/javascript">
	$('#reviews').owlCarousel({
		loop: true,
		margin: 10,
		nav: true,
		navText: [
		"<i class='fa fa-angle-left' aria-hidden='true'></i>",
		"<i class='fa fa-angle-right' aria-hidden='true'></i>"
		],
		autoplay: true,
		autoplayHoverPause: true,
		items: 1
	});

	$('#upcome').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: [
    "<i class='fa fa-angle-left' aria-hidden='true'></i>",
    "<i class='fa fa-angle-right' aria-hidden='true'></i>"
    ],
    autoplay: true,
    autoplayHoverPause: true,
    items: 1
  });

	$('#news').owlCarousel({
		loop: true,
		margin: 50,
		nav: true,
		navText: [
		"<i class='fa fa-angle-left' aria-hidden='true'></i>",
		"<i class='fa fa-angle-right' aria-hidden='true'></i>"
		],
		autoplay: true,
		autoplayHoverPause: true,
		items: 3,
     responsiveClass: true,
      responsive: {
       0: {
         items: 1,
         margin: 50,

       },
       600: {
         items: 1,
         margin: 50,

       },
       1000: {
         items: 3
       }
     }
	});
  $('a').tooltip('show')

  // Fancybox
$(document).ready(function() {
  $('.fancybox').fancybox({
    padding   : 0,
    maxWidth  : '100%',
    maxHeight : '100%',
    width   : 560,
    height    : 315,
    autoSize  : true,
    closeClick  : true,
    openEffect  : 'elastic',
    closeEffect : 'elastic'
  });
  $('.bx-controls').addClass('container');
  $("figure").mouseleave(
    function () {
      $(this).removeClass("hover");
    }
  );

});
</script>
</body>
</html>