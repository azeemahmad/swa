<?php session_start();
//echo "<pre>";print_r($_SESSION);die;
if(isset($_SESSION['is_login']) &&  $_SESSION['is_login'] == 'true')
{
  if($_SESSION['exp_flag'] == 1)
  {
    header('Location: dashboard.php');
  }

  if($_SESSION['type_id'] == 1 || $_SESSION['type_id'] == 4 && $_SESSION['type_id'] == 7 )// if member types are not Life / Senior - R / Senior - R
  {
    $exp = 'N/A';
  }
  else {
    $exp = $_SESSION['exp_date'];
  }
  include_once('includes/config.php');
  ?>

  <!doctype html>
  <html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href=""/>
    <title>Film Writers Association | My Creations</title>
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <link href="css/color.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="inner-page">
    <div id="wrapper" class="inside-menu">
      <?php include_once('header.php'); ?>
      <div id="cp-content-wrap" class="page404 cp-login-page">
        <div class="container">
          <div class="row">
            <div class="col-md-12 main-reg-div">
              <div class="cp-reg-box sub-reg-div">
                <!-- <h5 class="sub-hd">WELCOME  <?php //echo $_SESSION['name'];?></h5> -->
                <?php
                $sql = "select fu.*,mem.* from `fwa_users` fu
                left join `fwa_members` mem on mem.reg_no = fu.reg_no
                where fu.reg_no = '".$_SESSION['username']."'";
                $result =  mysqli_query($db,$sql);
                $row = mysqli_fetch_assoc($result);
                ?>
                <h4>Your Details <span class="pull-right"><span style="cursor:pointer" onclick="javascript:void(0)" class="label label-success">Edit (Coming Soon)</span></span></h4>
                <!-- <h4>Your Details <span class="pull-right"><span style="cursor:pointer" onclick="location.href='details_edit.php'" class="label label-success">Edit (Coming Soon)</span></span></h4> -->
                <table class='table table-hover'>
                    <tr>
                      <th>Membership No.</th>
                      <td><?php echo $row['reg_no']; ?></td>
                    </tr>
                    <tr>
                      <th>SWA Membership Expiry Date</th>
                      <td><?php echo $exp; ?></td>
                    </tr>
                    <tr>
                      <th>Email</th>
                      <td><?php echo $row['email']; ?></td>
                    </tr>
                    <tr>
                      <th>Name</th>
                      <td><?php echo $row['name']; ?></td>
                    </tr>
                    <tr>
                      <th>Address 1</th>
                      <td><?php echo $row['add1']; ?></td>
                    </tr>
                    <tr>
                      <th>Address 2</th>
                      <td><?php echo $row['add2']; ?></td>
                    </tr>
                    <tr>
                      <th>City</th>
                      <td><?php echo $row['city']; ?></td>
                    </tr>
                    <tr>
                      <th>Pin</th>
                      <td><?php echo $row['pin']; ?></td>
                    </tr>
                    <tr>
                      <th>State</th>
                      <td><?php echo $row['state']; ?></td>
                    </tr>
                    <tr>
                      <th>Mobile no</th>
                      <td><?php echo $row['mobileno']; ?></td>
                    </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php include_once('footer.php'); ?>
  </div>
  <script src="js/jquery-1.11.3.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.bxslider.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/custom.js"></script>
</body>
</html>
<?php }
else{
  header('Location: login.php');
} ?>
