<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href=""/>
  <title>Film Writers Association | About Us</title>
  <link href="css/custom.css" rel="stylesheet" type="text/css">
  <link href="css/color.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
  <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>

      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body class="inner-page">
      <div id="wrapper">
       <?php include_once('header.php'); ?>
       <div id="cp-content-wrap">
       <!-- Banner -->
       <div class="banner_inner">
         <img src="images/bg.jpg">
       </div>
       <!-- End of Banner -->
        <div class="cp_our-story-section">
         <div class="container">
          <div class="row">
           <div class="col-md-12">
              <div class="cp-acticle-box abt-div">
                <h4 class="titlesm bold text-center">
                  CONSTITUTION OF THE SCREENWRITERS ASSOCIATION MUMBAI
                </h4>
                <h5>
                  As revised and amended by the General Body on July 17, 2016.
                </h5>
    <p><strong>1. NAME:&nbsp;&nbsp; </strong></p>
    <p>The Screenwriters Association (hereinafter referred to as “The Association”) is a Trade Union of screenwriters of the film and television industry registered under Trade Unions Act, 1926.</p>
    <p><strong>2.OFFICE:&nbsp;&nbsp;&nbsp; </strong></p>
    <p>The registered office of the Association shall be within the limits of greater Mumbai. Currently: 201/204, Richa, Plot no B - 29, Off New Link Road, Opp. City Mall, Andheri (West) Mumbai - 400 053.</p>
    <p><strong>3.AIMS &amp; OBJECTS:&nbsp;&nbsp;&nbsp; </strong></p>
    <p>The Screenwriters’ Association (SWA) shall be an autonomous organisation having the following aims and objectives: </p>
    <p>3.a) To foster a feeling of fraternity, sorority and unity amongst its members.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
    <p>3.b) To regulate the relationship of its members with producers, directors, studios, networks, channels and other assignees through collective bargaining, via Minimum Basic Contracts for film and TV writers and for lyricists, as well as for those working in the new media, which ensure minimum fees and the protection of all the legitimate rights of the writers and lyricists, including those covered under the existing copyright laws of India.</p>
    <p>3.c) To secure and safeguard the interests, rights and privileges of its members in all matters relating to their professional engagement and working conditions.</p>
    <p>3.c.i) However, the Association is not responsible for securing employment or contracts/assignments for its members.</p>
    <p>3.d) To promote and encourage high standards of professional conduct and integrity amongst its members. To also provide appropriate learning opportunities to members to upgrade their scriptwriting and lyric-writing skills.</p>
    <p>&nbsp;</p>
    <p>3.e) To mediate in disputes between members and producers, directors, studios, TV networks, web networks, employers, etc. arising out of any breach of the terms of their contracts or agreements, with a view to settle them. To also address and mediate in disputes between members, which may arise out of any breach of contractual obligations, including that of copyright infringement.</p>
    <p>&nbsp;</p>
    <p>3.f) To secure compensation for its members in cases of accidents under the Workmen's Compensation Act, and other related laws through all remedies available that are legal and equitable.</p>
    <p>3.g) To provide legal assistance to its members in respect of matters arising out of/or incidental to their profession, including legal consultancy and other such support services.</p>
    <p>3.h)Cases to be taken up with the criterion that they pose a threat to screenwriters’ rights, in general.
        <br> 3.i)&nbsp; All such matters described in Clause 3.g)&amp; 3.h) will be scrutinised and decided by the Executive Committee.</p>
    <p>3.j) To try and secure representation of its members on delegations, commissions, committees, etc., set-up by the Central or State Governments or the film industry or other bodies where issues concerning screenwriters or screenwriting are to be discussed.</p>
    <p>3.k) To collaborate with any individual or organisation, whether private or governmental, within India or internationally, to promote the aims and objects of the Screenwriters Association including securing and protecting the rights of its members, as well as initiatives which will help SWA members as well as other screenwriters to further their craft and help them build their writing careers.</p>
    <p><strong>4.DEFINITION OF MEMBERSHIP :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>
    <p>4.a) Membership of SWA is meant for screenwriters, which term includes film writers, TV writers, lyric writers for films, TV or audio formats, and screenwriters writing for digital or other platforms or any new media, who reside within the boundary of the Union of India.</p>
    <p>4.b) “Fellow membership” can be offered to Non-Resident Indians as well as foreign nationals who reside outside India, as per the terms and conditions fixed by the Executive Committee, from time to time.</p>
    <p><strong>5. MEMBERSHIP:&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>
    <p>&nbsp; 5.a.i) Any person who has attained the age of 18 years, and is engaged in the work of writing as per Clause 4 above can become a member of the Association irrespective of caste, gender, colour, creed, race, religion, and language. Such a person shall be proposed by one Life or Regular member and seconded by another Life or Regular member. The application for membership must be made on the prescribed form of the Association.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br> 5.a.ii) In exceptional cases, subject to the approval of the Executive Committee, membership of the Association may also be given to a person who has not attained the age of 18 years.</p>
    <p>5.b) There are FOUR Classes of Membership:</p>
    <p>5.b.i) <u>Regular Member</u>: The following people are eligible to become Regular members: Any person who has had his/her work certified by CBFC as a feature film (or on the Internet), with the credit of either Story, Screenplay, or Dialogue. For TV writers, either a TV serial or a programme on TV with credit of either Created, Story, Screenplay, or Dialogue by. Lyric writers whose song has appeared in a film or a TV serial, or has been published in an album.</p>
    <p>5.b.ii) <u>Associate Member</u>: Any writer who has been professionally and legally contracted by a producer to write Story, Screenplay or Dialogue or Lyrics for a feature film or TV or on the Internet, with the first cheque having been paid to him/her and deposited, or diploma or certificate holders from any recognised institute having studied a screenwriting course with the duration of a minimum of one year will be admitted as Associate members. The duration of this membership is three years. If in those three years, the said member doesn't upgrade to Regular category, his/her Associate membership will lapse. In which case, s/he will have to apply again afresh for membership.</p>
    <p>5.b.iii) <u>Fellow Member</u>: Any person who has a passion for screenwriting is eligible for Fellow membership. A Fellow member will be allowed to register his/her work at SWA. However, s/he will not be able to attend SWA AGMs or vote or stand for SWA elections. S/he can attend SWA events like seminars, conferences, workshops, etc., that are open to other members. The duration of Fellow membership will be three years, within which period s/he will either get himself/herself upgraded to Associate or Regular category, or else the Fellow membership will expire after three years. If s/he wishes to become a Fellow member again, s/he will have to reapply afresh for it by fulfilling all the application protocols.</p>
    <p>5.b.iv) <u>Life Member</u>: Any Regular member or any other person eligible to become a Regular member can enrol himself/herself as Life member after paying an amount fixed by the Association from time to time and shall not be required to pay the monthly subscription thereafter.</p>
    <p>5.c) If a Fellow member becomes eligible for Associate Membership, or if an Associate Member becomes eligible for Regular membership, then it is mandatory for him/her to upgrade to that category. If, even after receiving a notice from SWA to do so, the person does not apply for that appropriate category, her/his membership is liable to be cancelled.</p>
    <p><strong>6. ADMISSION FEE &amp; MONTHLY  SUBSCRIPTION:&nbsp;&nbsp;&nbsp; </strong> </p>
    <p>The admission and the annual renewal (subscription) fees will be revised from time to time by the Executive Committee to rates considered reasonable and appropriate by it.</p>
    <p>The current monthly subscription payable by members shall be as below:
        <br> LIFE MEMBER: RS. 21,000/- (TOTAL FEE, FOR LIFE)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br> REGULAR MEMBER: RS.10/- (MONTHLY)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br> ASSOCIATE MEMBER: RS. 6/- (MONTHLY)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br> FELLOW MEMBER: RS.6/- (MONTHLY)</p>
    <p><strong>7. THE RIGHTS &amp; RESPONSIBILITIES OF MEMBERS:&nbsp; </strong></p>
    <p><strong>7.a) The Rights of Regular, Life and Associate members: </strong></p>
    <p>7.a.i) Right to attend Annual General Meetings, participate in the discussions therein, and to vote on its resolutions. </p>
    <p>7.a.ii) Right to apply for pension, medical aid, educational support and legal aid.</p>
    <p>7.a.iii) Right to have their work registered at the Association.</p>
    <p>7.a.iv) Right to apply for mediation from the Disputes Settlement Committee via the DSC Advisory Cell. </p>
    <p>7.a.v) Right to use the Association library, and the right to use the facilities that the Association offers its members, from time to time.</p>
    <p>7.a.vi) Right to attend SWA events which are open to all members of the Association, as per the terms and conditions declared for each event.</p>
    <p>7.a.vii) Right to vote at SWA elections, and the right to stand for elections to the Executive Committee, contingent on the eligibility mentioned in the relevant section below.&nbsp; </p>
    <p><strong>7.b. The Rights of Fellow Members: </strong></p>
    <p>7.b.i) Right to have their work registered at the Association.</p>
    <p>7.b.ii) Right to apply for mediation from the Disputes Settlement Committee via the DSC Advisory Cell. </p>
    <p>7.b.iii) Right to use the Association library, and the right to use the facilities that the Association offers its members, from time to time.</p>
    <p>7.b.iv) Right to attend SWA events which are open to all members of the Association, as per the terms and conditions declared for each event.</p>
    <p><strong>7.c) Responsibilities and Duties Of Members: </strong></p>
    <p>7.c.i) To not indulge in any action or behaviour which would compromise the integrity, dignity or reputation of the Association. </p>
    <p>7.c.ii) To ensure that their personal information in the Association records is updated from time to time. </p>
    <p>7.c.iii) To pay their renewal fees regularly. </p>
    <p>7.c.iv) To behave in a dignified, calm and civilised manner at Association events, AGMs, meetings, and in the Association office. </p>
    <p>7.c.v) To not knowingly infringe upon the copyright or other rights of Association members, other writers, or of anyone else. </p>
    <p>7.c.vi) To upgrade their membership to the appropriate category as soon as they become eligible for the same. </p>
    <p>Any violation of these responsibilities is liable to lead to a cancellation of membership after following the disciplinary action protocols listed in the relevant section below. </p>
    <p><strong>8. ADDITIONAL RULES ABOUT MEMBERSHIP</strong></p>
    <p>8.a.Termination/ Suspension of Membership: If a member fails to renew his/her membership for six months after it is due, then a penalty of Rs. 500/- will be levied if the membership is renewed between six and 18 months. On failure to renew after that, a penalty of Rs. 1000/- will be levied for renewals between 18 and 24 months after the renewal date. However, if for 24 months after the renewal date, the pending fees and the penalty are not paid, his/her membership will be terminated. After that, if the person chooses to seek membership again, s/he will have to re-apply by fulfilling all the requirements for new members all over again.</p>
    <p>8.b) Renewal fees will be paid in the month of January every year.</p>
    <p>8.c) The Association shall maintain a membership register with relevant details of all members. However, personal details of members, including phone numbers, e-mail ids and addresses will not be shown or shared with other members or with any other persons or agencies, without the strict written permission of the member whose details have been requested. (However, in matters of dispute, with the express permission of the General Secretary, a member's details can be shared with a legitimate agency.)<strong></strong></p>
    <p><strong>9. ADMINISTRATION:&nbsp; </strong></p>
    <p><strong>The affairs of the association shall be administered by: </strong></p>
    <p>9.a) <strong>GENERAL BODY</strong>: The General Body, comprising of Regular, Life and Associate members of the Association shall be the overseeing body which issues guidelines to the Executive Committee from time to time. The General Body will have the right to examine, debate and even overturn decisions of the Executive Committee, where it believes that the interests of the Association are being compromised.</p>
    <p>The General Body will function in accordance with the Constitution of SWA.</p>
    <p>9.b) <strong>EXECUTIVE COMMITTEE</strong>: The Executive Committee will have 31 members of which three will be Associate members, and seven will be Office Bearers. Elections to the Executive Committee will be held every two years. The Executive Committee will function as per the guidelines and authority accorded to it in the Constitution of SWA, and will be responsible for implementing the objectives of SWA and for its day-to-day functioning.</p>
    <p><strong>10. OFFICE BEARERS</strong></p>
    <p>The Officer Bearers of the Association shall be as under:
        <br> President&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br> Vice Presidents (Two)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br> General Secretary&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br> Joint Secretaries (Two)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br> Treasurer&nbsp; </p>
    <p>All the aforesaid office bearers shall be elected for 2 years by secret ballot at the General Body election meeting of the Association. On the expiry of their term, they shall be eligible for re-election for another term. However, after holding the post of Office Bearer for two consecutive terms, a member cannot contest for the post of Office Bearer. However, he/she can stand for election for membership to the Executive Committee. And, s/he will be eligible to contest for the post of Office Bearer after a gap of one term.&nbsp;&nbsp;&nbsp;&nbsp; </p>
    <p><strong>11. EXECUTIVE COMMITTEE:</strong></p>
    <p>11.a) The Executive Committee will have the absolute power to remove any erring Office Bearer from his/her post, after following the disciplinary action protocols listed in the relevant section below.</p>
    <p>11.b) The Executive Committee shall continue functioning till the new Executive Committee is duly elected in the General Body Election Meeting and announced by the Election Officer.&nbsp;&nbsp;&nbsp;&nbsp; </p>
    <p>11.c) If an EC member is unable to attend an EC meeting, s/he will inform the General Secretary beforehand and request leave of absence. In the event of any EC member remaining absent for three consecutive meetings (or a total of six meetings in the year), and if the Executive Committee is not convinced by the reasons for his/her absence, the EC will have the authority, in its discretion, to terminate the said member's membership from the Executive Committee. However, such a member can contest the next election, if s/he so wishes.</p>
    <p>11.d) In the unexpected event of an Office Bearer's position falling vacant, the Executive Committee shall fill up the same by co-opting a member of the Executive Committee to the said post. In case there is a vacancy created in the Executive Committee, for any reason whatsoever, then the same shall be filled by the Executive Committee by means of co-option from among the Regular, Life or Associate members of the Association, as may be the case. Also, the Executive Committee shall have the discretion and authority to co-opt members if their services are considered essential or desirable for the functioning of the Association. The Executive Committee will ensure that its strength never falls below 31 for more than a month. However, the total number of members in the Executive Committee, including the co-opted ones, will never exceed 35.</p>
    <p>11.e) The Executive Committee shall meet once a month on a day, place and time fixed by the General Secretary, in consultation with the President. However, emergency meetings can be held whenever necessary.</p>
    <p>11.f.i) Any Executive Committee member (including an Office Bearer) found guilty of any act of misconduct, misdemeanour or any action against the interest of the Association shall be issued a show-cause notice, whereupon his/her explanation or defence will be heard by the Executive Committee or a sub-committee appointed by it. If the said member's testimony is found unsatisfactory, then the member may be fined or suspended from EC membership by the Executive Committee.</p>
    <p>11.f.ii) Any member of the Association found guilty of working in any manner through word or action against the interest of the Association, or found guilty of a crime confirmed by a court of law, or found guilty of infringement of the rights of other members, will be liable for suspension of membership of the Association. The Executive Committee, or a sub-committee appointed by it, will give the said member due opportunity to explain and defend his/her actions. If the same is found unsatisfactory, depending on the nature and gravity of the offense, the member's membership will be suspended by the Executive Committee for either a period of two years or for life. If the former, then the case shall be reviewed after the period of two years by the Executive Committee in office at that time which will then either revoke the suspension or declare a further term for which the suspension will continue, as considered appropriate. Members whose membership is under suspension will not be able to avail of any benefits whatsoever that are otherwise available to members (including registration, medical support, mediation facility from the Dispute Settlement Committee) and shall not be eligible to stand for election or participate in any activity of the Association available only to members. The suspended member will not have to pay annual renewal fees for the period of suspension.</p>
    <p>11.g) At least three days’ notice shall be given for Executive Committee Meetings. </p>
    <p>11.h) <strong>PRESIDENT</strong>: The President shall preside over the meetings of the Executive Committee.</p>
    <p>11.i) The President shall be responsible for preserving order in the Executive Committee meetings as well as in the General Body meetings. Ergo, if any member, in any of these meetings, behaves in an unruly or disruptive manner, the President will have the authority to have the person evicted from the meeting, with the help of security officers, if needed. The President will sign all the minutes of the meeting and shall be eligible to vote on EC and General Body resolutions. However, in case of a tie in voting, in any of these meetings, the President will have the authority of an additional vote (casting vote) to break the tie.</p>
    <p>&nbsp;</p>
    <p>11.j) The President or the General Secretary may call a special meeting any time, if s/he thinks it’s necessary. If a third of the members of the Executive Committee submit a requisition for a special meeting, specifying the purpose, the President or the General Secretary must call a special meeting immediately.&nbsp;&nbsp; </p>
    <p>11.k) <strong>VICE PRESIDENT</strong>: In the absence of the President, the senior-most Vice President shall fulfil the duties of the President.</p>
    <p>11.l) <strong>GENERAL SECRETARY</strong>: The General Secretary shall be the Chief Executive Officer of the Association and will be responsible for the day-to-day functioning of the Association and its office.</p>
    <p>11.m) The General Secretary shall be responsible for recording the minutes of the meetings of the Executive Committee.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
    <p>11.n) The General Secretary shall convene all the Executive Committee Meetings in consultation with the President.</p>
    <p>11.o) The General Secretary along with the Treasurer will keep all accounts and prepare a Balance Sheet annually showing clearly every item of Receipt and Expenditure.&nbsp;&nbsp;&nbsp;&nbsp; </p>
    <p>11.p) The General Secretary shall have the authority to appoint office staff, including senior administrators or executives or managers or specialists or consultants as considered necessary for the Association, in consultation with the President and subject to the approval of the Executive Committee.</p>
    <p>11.q) The General Secretary shall submit all the returns and notices to the Registrar of Trade Unions, which are required to be submitted.</p>
    <p>11.r) <strong>JOINT SECRETARIES</strong>: The Joint Secretaries will assist the General Secretary in the performance of his/her tasks and duties.</p>
    <p>11.s) In the absence of the General Secretary, one of the Joint Secretaries nominated by the General Secretary shall perform all the duties of the General Secretary.
        <br> 11.t) <strong>TREASURER</strong>: The Treasurer shall be responsible for maintaining due accounts of the finances of the Association, and present monthly financial reports to the Executive Committee, and annually to the General Body.</p>
    <p>11.u) The Treasurer shall make payment towards all expenditure sanctioned by the Executive Committee.</p>
    <p>11.v) The Treasurer shall not have the power to draw any amount from the bank without first having the cheque signed by the President or the General Secretary.</p>
    <p><strong>12.FUNDS:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong> </p>
    <p><strong>12.a) Income</strong></p>
    <p>12.a.i) The General Funds of the Association shall consist of the Admission Fee, Life membership Fee, Subscription from the Members, Registration of writing work, Donations, Service charges for the settlement of disputes and income arising from recognised source. The amount of the General fund shall be kept in the Bank/Banks approved by the Executive Committee.</p>
    <p>12.a.ii) The Bank Account shall be operated by the President or the General Secretary and the Treasurer.</p>
    <p>12.a.iii) The General Secretary or The Treasurer shall not keep more than Rs.10,000/- (Ten Thousand Rupees) with her/him for current expenses.</p>
    <p>12.a.iv) The General funds of the Association (subject to the provisions of the Trade Union Act, 1926) shall not be spent on any object other than the following, namely:</p>
    <p><strong>12.b) Expenditure:</strong></p>
    <p>12.b.i) Salaries, allowances and other benefits to employees, consultants, retainers and other service-providers to the Association. Expenses and allowances to EC members and others contributing voluntary effort to the Association.</p>
    <p>12.b.ii) The Payments of expenses for the administration of the Association, including the audit of the accounts of the general funds of the Association.</p>
    <p>12.b.iii) All legal and related expenses in cases in which the Association is a party or has sought to intervene. The EC may use its discretion to offer legal and financial support to members who may be fighting cases that have a relevance to SWA's struggle to secure and protect its members' rights.</p>
    <p>12.b.iv) Expenses arising out of any activity or initiative undertaken by the Association to further its objectives.
        <br> 12.b.v) Medical, Educational or other Welfare allowances to members as decided by the Welfare or Educational or other relevant sub-committees responsible for such recommendations, and as approved by the EC.</p>
    <p>12.b.vi)&nbsp; Expenses related to the website, or any other publishing or communication activity of the Association.</p>
    <p>12.b.vii) In furtherance of the objects above, the General Funds of the association may also be spent on contributions to any cause intended to benefit Association members in general, directly or indirectly, subject to the condition that these expenses shall not at any time (in any financial year) be in excess of the net income, which has up to that time accrued to the General Funds of the Association during that year and of the balance to the credit of those funds at the commencement of the year.</p>
    <p>&nbsp;</p>
    <p><strong>13. AUDIT: </strong></p>
    <p>13.a) The Association shall make due provision for the Annual Audit of the accounts by a competent Auditor appointed by the Executive Committee (in accordance with the rule 18 of the Bombay Trade Union Regulations, 1927). </p>
    <p>13.b) The books of accounts of the Association shall be open to inspection by a member, with prior permission from the General Secretary for a specific purpose, during working hours. However, all EC members of the Association will be at liberty to inspect the books of accounts during office hours.</p>
    <p><strong>14. GENERAL BODY MEETING&nbsp; </strong></p>
    <p>14.a) The Annual General Body Meeting of the Association shall be held every year, latest in the month of August, for the following business:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br> 14.a.i) to confirm the minutes of the last AGM.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br> 14.a.ii) to adopt the report of the work done by the Association during the year.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br> 14.a.iii) to adopt the Audited statement of Accounts.
        <br> 14.a.iv) to appoint the Auditor and fix her/his remuneration.
        <br> 14.a.v) any other point which has been placed on the agenda by the General Secretary.&nbsp;
        <br> 14.a.vi) to transact such other business as may be brought forward with the permission of the chair. </p>
    <p><strong>15. SPECIAL GENERAL BODY MEETING</strong></p>
    <p>15.a) The President or the General Secretary or the Executive Committee (by a majority vote) can call for a special general body meeting for the express purpose of discussing or deciding or resolving any matter. </p>
    <p>15.b) A Special General Body Meeting can also be called if more than 500 Regular or Life members jointly ask the Executive Committee for a Special General Body Meeting via a signed requisition. Note: If the General Secretary or the President fails to convene a meeting within 20 days after this, then the requisitionists themselves are entitled to call for one after giving due notice and the proceedings of such a meeting shall be binding on the Association.</p>
    <p><strong>&nbsp;</strong></p>
    <p><strong>16. NOTICE OF THE MEETING: </strong></p>
    <p>A minimum of 15 days’ notice is necessary to be given to the members for the General Body Meeting or for the Special General Body Meeting.</p>
    <p><strong>17 QUORUM: </strong></p>
    <p>Two hundred and fifty members will be the quorum required to begin the meeting, failing which the meeting shall be adjourned. No quorum will be required once the meeting resumes after the adjournment.</p>
    <p><strong>18. ELECTIONS:&nbsp;&nbsp;&nbsp;&nbsp; </strong> </p>
    <p>18.a) Elections shall be held in the General Body election meeting after the expiry of one term, i.e., two years.</p>
    <p>18.b) Any Regular Member whose subscription is not in arrears for more than six months and any Regular or Life member whose membership is not under suspension is eligible to nominate himself/herself to contest the election to the Executive Committee or for the post of any of the Office Bearers, provided s/he fulfils the eligibility criteria as listed in the relevant section below.</p>
    <p>18.c) Associate members can contest for the membership of the Executive Committee for the seats reserved for Associate members, provided their subscription is not in arrears for more than six months. Associate members will be elected only by Associate members.</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>18.d) <strong>Eligibility</strong></p>
    <p>18.d.i) To contest for membership of the Executive Committee, a Regular/ Life member should have to her/ his credit at least three released feature films, with at least one in the last five years for Office Bearers (eight years for Executive Committee members) or 100 half-hour episodes or 25 one-hour episodes or their equivalent in terms of time, with at least 10 hours of programming in the last five years for Office Bearers (eight years for Executive Committee members) on TV or any new media, or 25 songs, with at least 10 in the last five years for Office Bearers (eight years for Executive Committee members).</p>
    <p>18.d.ii) To stand for election for an Office Bearer's post, a member will have to have served at least one full term as an ordinary member of the Executive Committee.</p>
    <p>18.d.iii) To contest for the seat of Associate Membership on the Executive Committee, a person should have been an Associate Member for at least one year, and her/ his subscription should not be in arrears for more than six months.</p>
    <p>18.d.iv) An SWA member who is already a member of the Executive Committee (or equivalent body) of any producers' association or guild will not be eligible to contest for the post of Executive Committee member of SWA. (If, during his/her term as Executive Committee member or as Office Bearer, s/he were to become a member of the Executive Committee (or equivalent body) of any producers' association or guild, then s/he will have to resign from her/ his position in the Executive Committee of SWA.</p>
    <p>18.e)&nbsp; On receipt of the nomination papers, the same shall be scrutinized by a Scrutiny Committee appointed by the Executive Committee, to determine their validity. The scrutiny committee will consist of not less than five members of the Association who are not contesting the election or those appointed in consultation with the Federation of Western India Cine Employees. Ineligible nominations will be disqualified by the scrutiny committee.</p>
    <p>&nbsp;</p>
    <p>18.f) Personal presence of the contestants in the General Body Election Meeting will be essential, unless the absentee has intimated in writing the reason for his/her absence earlier and the scrutiny committee has permitted the absence.</p>
    <p>&nbsp;</p>
    <p>18.g) Voting will be by secret ballot, either via paper or via electronic voting machines and/or via an online process (e-voting), after due testing and after due safeguards are confirmed by the Executive Committee with the help of experts consulted by them.</p>
    <p>18.h) The Office Bearers, along with other members of Executive Committee shall retire after transacting the normal business, before the commencement of the election.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
    <p>18.i) Prior to the election, the Scrutiny Committee shall appoint an Election Officer from among its own members or from among the non-contestants present. This Election Officer will conduct the election proceedings and declare the results. However, the Election Officer shall not be appointed from the retiring Office Bearers or members of the outgoing Executive Committee.</p>
    <p>18.j) The Election Officer so appointed shall conduct the elections, assisted by the Scrutiny Committee members present and by non-contesting members from the AGM and by the staff of the SWA.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
    <p>&nbsp;</p>
    <p>18.k) The Election officer will declare the results. The number of votes polled by each and every contestant will be announced. As soon as the new Executive Committee is declared by the Election Officer, it will hold an immediate meeting, the minutes of which will be recorded. The next meeting will be held within three working days, the time and place for which will be fixed at this meeting itself. Therefore, it is compulsory that all the contestants be present in the Election AGM until the declaration of the election result, except in extraordinary circumstances or an emergency with prior permission of the Election Officer. Also, if a contestant requests a recount it can only be allowed provided the margin of loss is less than 5% of the total votes polled by the victor, or if there has been any kind of tampering or technical glitch in the machine or the counting process as ascertained by the Election Officer, and confirmed by the Scrutiny Committee.</p>
    <p><strong>19. Vacancies and Re-Election :</strong></p>
    <p>19.1)In case of vacancy of any Officer Bearer caused by the cessation of membership, demise or illness or resignation or shifting of residence or by any directive of the Executive Committee or General Body, then the same shall be filled in by the Executive Committee.</p>
    <p>19. 2)&nbsp; In case of vacancy of the office of Treasurer due to any reason, re-election shall be held immediately within the Executive Committee to elect the new TREASURER from amongst the Executive Committee members. In the gap period till re-election, the President and the General Secretary shall operate the bank accounts and look after routine financial matters.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
    <p>19.3) In case of any valid objection against election of any member in the General Body Election Meeting, a recounting shall be held provided the difference between the loosing &amp; winning candidates vote is not more than five.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
    <p>&nbsp;</p>
    <p><strong>20. SUB-COMMITTEES: </strong></p>
    <p>The Executive Committee shall have the power to form sub-committees for the purpose of fulfilling the aims and objectives of SWA, including amendment of bye-laws. The strength of a sub-committee will not exceed seven, including the convener and/or chairperson. Each sub-committee will periodically report to the Executive Committee.</p>
    <p><strong>21.&nbsp; AMENDMENTS TO THE  CONSTITUTION: </strong></p>
    <p>21.a) Amendments to the Constitution of SWA may be proposed by the Executive Committee. To discuss them, a Special General Meeting may be called by the Executive Committee for this express purpose with a notice to members, accompanied by the proposed amendments, at least 15 days in advance of the meeting.</p>
    <p>21.b) The amendment/s shall not be passed in an adjourned meeting.</p>
    <p>21.c) On receiving the notice of the meeting to amend the Constitution, any Regular or Life member can propose amendments in the Constitution, provided their proposals are received by the SWA office at least seven days before the date of the meeting. Upon receiving them, the Executive Committee will examine the proposals received from members to ensure that none of them violate the law of the land or the essential aims and objects and defining principles on which SWA was formed. </p>
    <p>&nbsp;</p>
    <p>21.d) If there is no quorum to begin with, then that General Body meeting will be terminated without the proposed amendments being discussed. The Executive Committee will call for another General Body meeting with the express agenda of discussing the proposed amendments within the period of one month after this meeting. If in that meeting, there is no quorum, then the meeting will be adjourned for 30 minutes. Upon resumption, the meeting will proceed, regardless of quorum, and the agenda of the proposed amendments will be taken up. Proposed amendments to the Constitution can be passed only by a 2/3rd majority of Regular and Life members present.</p>
    <p><strong>22. Dissolution: </strong></p>
    <p>The Association shall not be dissolved except by the vote of a majority of members on the rolls of the Association, who have the right to vote and are present at a General Body meeting called for the purpose. In the case of the dissolution of the Association, after meeting all the liabilities, the funds of the Association shall be disposed off in accordance with the dissolution meeting. </p>
    <p><strong>SCREENWRITERS  ASSOCIATION BYE-LAWS</strong></p>
    <ol>
        <li>Bye-laws will be guiding force for the Executive Committee to govern the FWA as per the Constitution hence the Executive Committee shall have the power to amend any of the bye-laws at any time, provided, however, that more than 50% of the total number of members of the Executive committee are present at the meeting.</li>
    </ol>
    <p>&nbsp;</p>
    <p>2. FEE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br> a) ADMISSION FEE: For Regular members shall be Rs. 4700/- and Associate Members shall be Rs. 3,700/- and fellow members ,it shall be Rs. Rs.2500/- .&nbsp;&nbsp;&nbsp; </p>
    <p>b) LIFE MEMBERSHIP FEE: Rs. 21,000/-&nbsp; </p>
    <p>c) The above mentioned fees are subject to change by the Executive Committee.</p>
    <p>3<strong>. REFISTRATION OF WRITTEN WORK:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br> a) The Association shall provide the facility of Registering its member's work, such as story, synopsis, concept, screenplay, scenario, dialogue and lyrics for feature films, video films, teleplays, serials and Audio Cassettes etc.&nbsp; </p>
    <p>b) The Registration work shall be done in the office of the Association on five days in a week i.e.. Monday to Friday from 2.00 p.m to 5.30 p.m&nbsp; </p>
    <p>c) The Registration shall not be construed as 'Copyright Registration'.</p>
    <p>d) The Executive Committee is entitled to change the Registration Charges from time to time, as ii deems fit.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
    <p>e) While registering the written work submitted, the Registration Officer shall, under no circumstances read the content and shall not keep a copy of the work registered, in the office. </p>
    <p>f) While registering the work the officer shall subscribe his signature with date and affix the official seal of the Association on every page.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
    <p>g) The Registration record shall contain:1) Name of the&nbsp; writer 2) language of the script 3) cash book receipt number and 4) Total number of pages of the script.</p>
    <p>&nbsp;h) The Registration shall be subject to the solemn undertaking by the writer as under:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>'THIS  WORK IS MY ORIGINAL CREATION AND THAT IN CASE OF A DISPUTE IF PROVED OTHERWISE  THE F.W.A. HAS THE UNQUESTIONABLE RIGHT TO CANCEL IT."</strong></p>
    <ol>
        <li><strong>Registration  charges in the office:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></li>
    </ol>
    <p>SCRIPT&nbsp;&nbsp; Rs.20/- up to five pages&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rs.2/- every additional page&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br> LYRICS&nbsp;&nbsp;&nbsp; Rs.15/- per Lyric&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br> MUKHDA Rs.10/- per Mukhada.&nbsp;&nbsp;&nbsp; </p>
    <p><strong>Registration charges on the  Website:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong>
        <br> SCRIPT&nbsp;&nbsp; Rs.40/- up to five pages&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rs.2/- every additional page&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br> LYRICS&nbsp;&nbsp;&nbsp; Rs.40/- per Lyric &amp; Mukhada.</p>
    <p>j) No Registration of the member's work sent by post.</p>
    <p>k) Registration Allowance as fixed by the Managing Committee, Shall be paid to the Registering Officer.</p>
    <p>&nbsp;</p>
    <p>l) The Registration Officer can be appointed from amongst the Executive Committee Members, Life or Regular Members may also be appointed as Registration Officer subject to the approval of the Executive Committee. All the Executive Committee Members will be eligible to take up the registration work unless they themselves opt out from doing the same.</p>
    <p><strong>4. DISPUTE SETTLEMENT COMMITTEE:</strong></p>
    <p>The Executive Committee shall constitute a Sub-Committee for the purpose of settling disputes between a member and his/her employer or between a member and member in which the Association is directly or indirectly involved.</p>
    <ol>
        <ol>
            <li>The Sub-Committee shall comprise of a Chairman, a Convener and a maximum of five other members. The President and the Hon. General Secretary shall be ex-officio members.</li>
        </ol>
    </ol>
    <p>&nbsp;</p>
    <ol>
        <ol>
            <li>The Sub-Committee shall be constituted by the members of the Executive Committee by consensus of opinion or by election.</li>
        </ol>
    </ol>
    <ol>
        <ol>
            <li>The Dispute Settlement Committee shall generally have two meetings in a month. But the Convener shall have the power to call an emergency meeting in case the circumstances so warrant and the Convener is convinced that the complaint is of an urgent nature.</li>
        </ol>
    </ol>
    <p>&nbsp;</p>
    <ol>
        <ol>
            <li>All the members of the Dispute Settlement Committee shall be entitled to get sitting allowance for attending the meeting, fixed by the Executive Committee.</li>
        </ol>
    </ol>
    <ol>
        <ol>
            <li>The venue of the settlement of disputes shall be the office premises of the Association or such other place or places as the Dispute Settlement Committee may decide from time to time.</li>
        </ol>
    </ol>
    <p>&nbsp;</p>
    <ol>
        <ol>
            <ol>
                <li><strong>SETTLEMENT OF THE DISPUTES (GUIDE LINES)</strong></li>
            </ol>
        </ol>
    </ol>
    <p>Any member aggrieved or having claim in respect of a co-member or his/her employer, etc., as stated above shall lodge a complaint with the Association supported by the Registered copy of the work, copies of the documents relied on by him/her and list of witnesses which he/she may like to produce before the Dispute Settlement Committee for the settlement and/or adjudication of his/her dispute or claim. (EXPLANATION): Member shall mean and include his/her legal heirs and successors in case the claim is for the dues of a deceased member. The claimant has to have nomination or authorization of the deceased or a succession certificate.</p>
    <ol>
        <li>The Association shall, on the receipt of the said complaint issue notice to the other party inter-alia enclosing copy of the complaint to obtain a reply to the same, within ten days of the receipt of the said complaint and subsequently fix a date and inform the parties as to when and where the dispute shall be taken up for settlement.</li>
    </ol>
    <p>&nbsp;</p>
    <ol>
        <li>On the date fixed, the Dispute Settlement Committee shall hear the parties record their statements, scrutinize their documents and record evidences if necessitated, and thereafter, proceed to settle the dispute.</li>
    </ol>
    <ol>
        <li>In case, on the fixed date, if only one of the parties is present, the Committee shall adjourn the said case to another date. And in case, on the second date so fixed, the same party does not attend, the Dispute Settlement Committee shall send a final Registered Acknowledgement Due letter, specifying that ex-parte decision shall be taken by the Dispute Settlement Committee in case the party fails to attend even the third and final meeting, the D.S.C. shall act accordingly. In case the complainant remains absent for</li>
    </ol>
    <p>&nbsp;</p>
    <p>two consecutive meetings, the complaint shall be kept in abeyance until the complainant satisfies the D.S.C. with reasons for having been absent twice.</p>
    <ol>
        <li>The decision of the D.S.C. shall be conveyed in writing to the contesting parties within three days of the decision.</li>
    </ol>
    <p>&nbsp;</p>
    <ol>
        <li>The decision shall be binding on the parties save and except their right to appeal to the Executive Committee.</li>
    </ol>
    <ol>
        <li>Any member complainant, not satisfied with the decision of the D.S.C., has the right to appeal against the decision to the Executive Committee one month from the receipt of the decision. The Executive Committee, before hearing the said appeal, shall take a solemn undertaking, in writing, from the member appellant that he/she shall abide by the decision of the Executive Committee.</li>
        <li>In case the employer goes to the court of law against the decision of the Association, the Association shall defend the case concerned only on behalf of itself and shall assist and give full co-operation to the member concerned but the member shall have to defend the case against him/her at his/her own cost and consequence.</li>
        <li>In case any member or members of the Dispute Settlement Committee happens to be connected with the film in question in any capacity or with it’s Producer against whom the dispute is being processed, the said member/members shall be debarred from participating in the case. In such a case the Executive Committee shall nominate other member/members from out of the Executive Committee in his/her/their places in that particular case.</li>
        <ol>
            <li>In the event of any dispute in respect of the copyright of a particular script or lyric where two similar scripts or lyrics are involved and the Association is called upon to decide as to who is the actual author entitled to the ownership of copyright, the same shall be decided on the basis of the Registration, i.e., the script or lyric registered prior in date or in time by the writer, shall be considered as the bonafide copyright owner.</li>
        </ol>
    </ol>
    <p>&nbsp;</p>
    <ol>
        <ol>
            <li>In addition to the above clause 5/(H), if the shooting has been started or the song has been recorded, but the producer has failed to make any significant progress for a period of one year, then in that case, the producer shall pay the balance amount in full or in installments as per the decision of Dispute Settlement Committee-- else the copyright of the written work shall be transferred back to its writer and the producer would have no claim against it whatsoever. In case of a dispute between the script writer and the producer, the dialogue writer and the lyricist shall not be instructed by F.W.A. not to co-operate with the producer and likewise in alternate cases. However, in complex cases, wherein the nature of assignment overlaps or story scenario and dialogue are by one writer alone, the D.S.C. can recommend the Executive Committee to issue directives to all for non co-operation.</li>
        </ol>
    </ol>
    <p>&nbsp;</p>
    <ol>
        <li>Wherein a member writer has complained with proper proof that his/her work partly or wholly has been taken by producer/employer without making any payment to him/her or without giving any credit to him/her and even without entering into a contract with him/her, the D.S.C., after thorough inquiry from the concerned parties,can ask the producer/employer to pay damages to the writer. In case a writer has lodged a false complaint then such a writer should be asked to tender unconditional apology to the producer/employer and the complaint should be treated as closed.</li>
    </ol>
    <ol>
        <ul>
            <li>Wherein a producer/employer has used the work of the writer, whether script or lyric, or any other intellectual work of the writer, without his/her permission, and it is proved beyond doubt that indeed such a thing has happened, then the D.S.C. has the right to demand c o m p e n s a t i o n / d a m a g e s f r o m s u c h producer/employer after thorough investigation.</li>
        </ul>
        <p>6.<strong>APPEAL</strong></p>
        <ol>
            <li>Upon entertaining an appeal, the Executive Committee of the Association shall appoint an Appellate Board which shall comprise of not less than three members of the Executive Committee, one of whom shall be designated as Chairman. The Convener of the D.S.C., however, shall have to attend the Appellate Board's meeting to explain and defend the decision of the D.S.C. Further to it, the Executive Committee has the power to convert itself into the Appellate Board, if it so decides.</li>
        </ol>
        <p>&nbsp;</p>
        <ol>
            <li>The Appellate Board shall meet once in a month if there is an appeal pending.</li>
            <li>The Executive Committee shall take an irrevocable written commitment from the appellant that Appellate Board's decision shall be binding upon him/her as stated above in clause 5 (g).</li>
            <li>The Appellate Board shall hear the appeal against the decision of the Dispute Settlement Committee within one month. The Executive Committee however,</li>
        </ol>
        <p>shall refuse to entertain any appeal which is not filed within one month of the decision of the Dispute Settlement Committee.</p>
        <ol>
            <li>The Appellate Board shall decide the appeal on the basis of the records produced before the Dispute Settlement Committee. The Appellate Board in its decision may allow the parties to introduce fresh documents and witnesses in the interest of justice and equity.</li>
        </ol>
        <p>&nbsp;</p>
        <ol>
            <li><strong>LOAN:</strong></li>
        </ol>
        <ol>
            <ol>
                <li>Only Regular and Life members who have been members for at least one year shall be entitled to approach the Executive Committee for a loan.</li>
            </ol>
        </ol>
        <p>&nbsp;</p>
        <ol>
            <li>The applicant has to record the duration of the loan and commit in writing the expected date of the repayment of the loan.</li>
        </ol>
        <ol>
            <li>The Executive Committee shall have the power to sanction the loan not exceeding Rupees Ten Thousand.</li>
        </ol>
        <p>&nbsp;</p>
        <p>C (1) The Executive Committee in exceptional cases may even grant a loan exceeding Rupees Ten Thousand.</p>
        <ol>
            <li>No further loan shall be granted to a member who is in arrears of a full or part of the previous loan.</li>
        </ol>
        <p>&nbsp;</p>
        <ol>
            <li>The Executive Committee shall have the power to refuse any such loan without assigning any reason to the applicant.</li>
        </ol>
        <ol>
            <li><strong>FINANCIAL AID:</strong></li>
        </ol>
        <p><strong>&nbsp;</strong></p>
        <ol>
            <ol>
                <li>All members excluding Fellow Members shall be entitled to ask for financial aid, not exceeding Rs.5000/-.</li>
            </ol>
        </ol>
        <p>&nbsp;</p>
        <ol>
            <ol>
                <li>A sum of Rupees Five Thousand shall be offered to the next of kin of a deceased member at the earliest.</li>
            </ol>
        </ol>
        <ol>
            <ol>
                <li>The Hon. Gen. Secretary shall have the right to grant up to Rupees Five Thousand only in an emergency case, but he will have to ratify his sanction in the following Executive Committee meeting.</li>
            </ol>
        </ol>
        <p>&nbsp;</p>
        <ol>
            <ol>
                <li>The Executive Committee shall have the right to refuse any application for emergency relief without having to assign any reason for it to the applicant.</li>
            </ol>
        </ol>
        <ol>
            <li><strong>FILM WRITERS' ASSOCIATION'S&nbsp;  'MEDICAL</strong></li>
        </ol>
        <p><strong>&nbsp;</strong></p>
        <p><strong>RELIEF SCHEME’</strong></p>
        <ol>
            <ol>
                <li>FUNDS: Total annual disbursement amount shall not exceed 20% of the excess of income over the expenses amount, and shall be declared in the every first meeting of the Executive Committee after the Annual General Body meeting while electing the special Sub-Committee for the operation of the scheme.</li>
            </ol>
        </ol>
        <p>&nbsp;</p>
        <ol>
            <ol>
                <li>AUTHORITY: The Medical Relief Scheme will be operated and managed by a Sub-Committee comprising of the following:</li>
            </ol>
        </ol>
        <ol>
            <ol>
                <ol>
                    <li>President of FWA</li>
                </ol>
            </ol>
        </ol>
        <p>&nbsp;</p>
        <ol>
            <ol>
                <ol>
                    <li>One Vice President of FWA</li>
                    <li>General Secretary of FWA</li>
                    <li>Hon. Treasurer of FWA</li>
                    <li>One member representing Regular Members</li>
                    <li>One Member representing Associate Member.</li>
                </ol>
            </ol>
        </ol>
        <ol>
            <ol>
                <li>This Scheme is exclusively meant for rendering medical assistance to the members of the Association who are eligible for such aid as per the rules of this Scheme. No member can avail of this scheme for his/her spouse or dependant.</li>
            </ol>
        </ol>
        <p>&nbsp;</p>
        <ol>
            <li>ELIGIBILITY: Members who satisfies any of the following conditions:</li>
        </ol>
        <ol>
            <ol>
                <li>A member whose membership is five years old.</li>
            </ol>
        </ol>
        <p>&nbsp;</p>
        <ol>
            <ol>
                <li>A member who has three films to his/her credit.</li>
                <li>A member whose subscription is not in arrears.</li>
            </ol>
        </ol>
        <p><strong>NON ELIGIBILITY:</strong></p>
        <ol>
            <li>A member whose loan repayment is due for more than two years.</li>
        </ol>
        <p>&nbsp;</p>
        <ol>
            <li>A member who is employed elsewhere or carrying on any other profession or business as well.</li>
        </ol>
        <ol>
            <ol>
                <li>The Committee should consider all applications in its monthly meeting to be held half an hour before the Executive Committee meeting. The Committee should insist upon reimbursing paid up medical or hospital bills, supported by certificates of the qualified doctor.</li>
            </ol>
        </ol>
        <p>&nbsp;</p>
        <ol>
            <ol>
                <li>In exceptional emergency, the Hon. Gen. Secretary is empowered to give an ad-hoc aid not exceeding Rs.500/- in consultation with the President or Hon. Treasurer.</li>
            </ol>
        </ol>
        <ol>
            <ol>
                <ul>
                    <li>If a beneficiary of this fund wants to reimburse in full or part of the amount he/she received in the past under the Scheme, the Committee should accept it as a donation with thanks. And the said person's name should be announced in Annual General Body meeting.</li>
                </ul>
                <ol>
                    <li><strong>EMERGENCY  RELIEF FUND:</strong></li>
                </ol>
                <ol>
                    <ol>
                        <li>A sum of Rupees Three Lakhs shall be kept permanently reserved in the Fixed Deposit Account in any Scheduled Bank as the Executive Committee deems fit. The interest accrued from the F.D. a/c shall be spent only for the payment in any emergency financial situation of a member.</li>
                    </ol>
                </ol>
                <p>b) If any Emergency Relief Fund is not available, the Executive Committee shall have the power to draw upon some other Fund and reimburse the same when the financial situation of the Fund normalizes.</p>
                <ol>
                    <li><strong>CONVEYANCE,&nbsp; SITTING&nbsp; ALLOWANCE&nbsp;  &amp;</strong></li>
                </ol>
                <p><strong>&nbsp;</strong></p>
                <p><strong>REGISTRATION ALLOWANCE</strong></p>
                <ol>
                    <ol>
                        <li>Conveyance as decided by the Executive Committee may be paid to the members attending for Office work as per the instruction of Executive Committee. This amount should not be more than Rs.400/-.</li>
                    </ol>
                </ol>
                <p>&nbsp;</p>
                <ol>
                    <li>Sitting Allowance may be paid to the members as decided by the Executive Committee for attending the Executive Committee meeting &amp; Dispute Settlement Committee meetings.</li>
                </ol>
                <ol>
                    <li>Registration Allowance may be paid to the member as decided by the Executive Committee for attending the registration work.</li>
                </ol>
                <p>&nbsp;</p>
                <ol>
                    <ol>
                        <li>No member shall be entitled to more than one conveyance amount for attending more than one meeting on a single day and at the same place.</li>
                    </ol>
                </ol>
                <p><strong>12.  ANNUAL AWARDS:</strong></p>
                <p>a)&nbsp;&nbsp; Trophies shall be awarded to at least three members on the day of the Annual&nbsp;&nbsp; General Body meeting of the Association.</p>
                <ol>
                    <li>The selection of the members to be awarded shall be decided upon by the Executive Committee taking into consideration, primarily, the long standing services of the member to the art and craft of film writing, so also his, meritorious contribution.</li>
                </ol>
                <p>&nbsp;</p>
                <ol>
                    <li>The Hon. Gen. Secretary shall prepare a list of deserving writers, taking into consideration the names of the writers proposed by members in general, in writing, at any time and present the list before the Executive Committee for final selection by general consensus.</li>
                </ol>
                <ol>
                    <li>In addition to the Trophy, the members selected shall be presented with a Shawl and a Cheque of a token amount, to be decided by the Executive Committee, and a Citation.</li>
                </ol>
                <p>&nbsp;</p>
                <ol>
                    <li>The name, if any, and the design of the Trophy and its cost shall be decided by the Executive Committee.</li>
                </ol>
                <ol>
                    <li>Any elected/nominated member of the Executive Committee of any Producers’ Body will not be eligible to contest the election for any post of Executive Committee or for the post of an Office Bearer.</li>
                </ol>
                <p>&nbsp;</p>
                <ol>
                    <li>Any NRI/Foreign national/s including person/s of Indian Origin and Overseas citizen of India who are writers and are working in India, can be enrolled as a Fellow Member of the Film Writers' Association (FWA) provided such persons satisfy the eligibility criteria prescribed in the Constitution of the FWA. The membership fee for such applicant will be Rs. 10,000/- (Rupees Ten Thousand Only) and that such applicants will have to furnish Writing Proof, Identity Proof and Residence Proof with the completed application form to the FWA.</li>
                </ol>
                <p><strong>TERMS  OF CONTRACT</strong></p>
                <p>Section 19 of the Contract which deals with the Assignment of Rights has also been substituted so as to articulate more clearly the Rights which can be assigned by the holder of the Copyright.</p>
                <ol>
                    <li>There has to be a CONTRACT IN WRITING SIGNED BY THE ASSIGNOR so as to VALIDLY</li>
                </ol>
                <p>ASSIGN ANY RIGHT.</p>
                <ol>
                    <li>Even in such a Contract, the WORK BEING</li>
                </ol>
                <p>&nbsp;</p>
                <p>ASSIGNED HAS TO BE IDENTIFIED.</p>
                <ol>
                    <li>Further, even after identifying the work, the Contract has to SPELL OUT AND SPECIFY THE</li>
                </ol>
                <p>RIGHTS BEING SO ASSIGNED IN SUCH A WORK.</p>
                <ol>
                    <li>The Contract has to SPECIFY THE DURATION OF ASSIGNMENT.</li>
                </ol>
                <p>&nbsp;</p>
                <ol>
                    <li>T h e&nbsp; C o n t r a c t&nbsp; h a s&nbsp; t o&nbsp; S P E C I F Y&nbsp; T H E TERRITORIAL EXTENT OF ASSIGNMENT.</li>
                </ol>
                <ol>
                    <li>Even after assignment, the Contract should also STATE THE AMOUNT OF ROYALTY PAYABLE, IF ANY, TO THE AUTHOR OR HIS/HER LEGAL</li>
                </ol>
                <p>HEIR DURING THE PERIOD OF ASSIGNMENT.</p>
                <ol>
                    <li>The above assignment SHALL BE SUBJECT TO REVISION, EXTENSION OR TERMINATION ON TERMS MUTUALLY AGREED UPON BY BOTH--</li>
                </ol>
                <p>&nbsp;</p>
                <p>THE ASSIGNOR AND THE ASSIGNEE.</p>
                <p>&nbsp;</p>
                <p>Note: Where the Assignment is vague and not specific, the Assignment Clause provides 3 other safeguards in favour of the writer.</p>
                <p>Where, after having taken the Assignment of Rights from the Assignor, the Assignee does not exercise the Rights so assigned WITHIN 1 YEAR FROM THE DATE OF ASSIGNMENT, the Assignment will BE DEEMED TO HAVE LAPSED UNLESS OTHERWISE SPECIFIED IN THE ASSIGNMENT.</p>
                <p>&nbsp;</p>
                <p>Where the PERIOD OF ASSIGNMENT is not STATED, the Assignment SHALL BE DEEMED TO BE FOR 5 YEARS.</p>
                <p>Where the TERRITORIAL EXTENT OF ASSIGNMENT is NOT STATED, it SHALL BE DEEMED THAT THE ASSIGNMENT EXTENDS ONLY WITHIN INDIA.</p>
            </ol>
        </ol>
    </ol>
</td> 
              </div>
           </div>   
         </div>
       </div>
     </div>
   </div>
   <?php include_once('footer.php'); ?>
 </div>
 <script src="js/jquery-1.11.3.min.js"></script>
 <script src="js/bootstrap.min.js"></script>
 <script src="js/jquery.bxslider.min.js"></script>
 <script src="js/owl.carousel.min.js"></script>
 <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
 <script src="js/jquery.counterup.min.js"></script>
 <script src="js/custom.js"></script>
 <script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
 <script type="text/javascript">
  $(function () {

    var filterList = {

      init: function () {

        // MixItUp plugin
        // http://mixitup.io
        $('#portfoliolist').mixItUp({
          selectors: {
            target: '.portfolio',
            filter: '.filter' 
          },
          load: {
            filter: '.exe'  
          }     
        });               

      }

    };
    
    // Run the show!
    filterList.init();
    
    
  }); 
  
</script>
</body>
</html>