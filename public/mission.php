<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href=""/>
  <title>Film Writers Association | Our Mission</title>
  <link href="css/custom.css" rel="stylesheet" type="text/css">
  <link href="css/color.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
  <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>

      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body class="inner-page">
      <div id="wrapper">
       <?php include_once('header.php'); ?>
       <div id="cp-content-wrap">
       <div class="banner_inner">
         <img src="images/banner/aboutus.jpg">
       </div>
        <div class="cp_our-story-section">
         <div class="container">
          <div class="row">
           <div class="col-md-12">
              <div class="cp-acticle-box abt-div">
                <h1 class="title bold text-center">
                  Our Mission
                </h1>
                <p class="text-justify">The Screenwriters Association (SWA, formerly Film Writers' Association - Regd. Under the Trade Union Act 1926, Regd. No. 3726) is a Trade Union of screenwriters and lyricists who work for Films, TV and Digital Media in India. Authors, novelists, playwrights, journalists who aspire for to diversify or join fulltime the mediums of films, TV or Digital entertainment, are also members of the SWA.</p>
                <p class="text-justify">The Screenwriters’ Association (SWA) is a body of the Writers, for the writers and by the Writers. It’s an autonomous organisation having the following aims and objectives:</p>
                <ol>
                  <li>To foster a feeling of fraternity, sorority and unity amongst its members.</li>
                  <li>To regulate the relationship of its members with producer bodies and other assignees through collective bargaining via Minimum Basic Contracts for film and TV and new media (digital).</li>
                  <li>To secure and safeguard the interests, rights, compensation and privileges of its members in all matters relating to their professional engagement and working conditions. (However, the Association is not responsible for securing employment or contracts/assignments for its members.)</li>
                  <li>To promote and encourage high standards of professional conduct and integrity amongst its members.</li>
                  <li>To also provide appropriate learning opportunities to members to upgrade their scriptwriting and lyric-writing skills, by organizing seminars and workshops for writers at subsidized cost.</li>
                  <li>To address, mediate and settle disputes such as breach of contractual obligations, copyright infringement and refutation of credits and/or remuneration, between members and producer bodies.</li>
                  <li>To provide legal assistance to its members in respect of matters arising out of/or incidental to their profession, including legal consultancy.</li>
                  <li>To secure representation of its members on delegations, commissions, committees etc. Set-up by the government or other bodies where issues concerning screenwriters or screenwriting are to be discussed.</li>
                  <li>To promote the aims and objects of the Screenwriters Association that will help SWA members as well as other screenwriters to further their craft and help them build their writing careers.</li>
                </ol>
                <h4>Ancillary Functions of the FWA</h4>
                <ol>
                  <li>The Screenwriters Association also registers, through its office & website - the scripts, lyrics, stories written by its members. This service safeguards the Copyright of a SWA member.</li>
                  <li>SWA also run welfare activities for needy and elderly SWA members in the form of medial-aid, pension and scholarships (to their children).</li>
                </ol>
                <br>
                <h1 class="title bold text-center" id="constitution">
                  SWA Constitution
                </h1>
                <p class="text-justify">SWA’s Executive Committee functions as per the guidelines and authority accorded to it in the Constitution of SWA. The overseeing General Body, comprising of Regular, Life and Associate members of the Association also adheres to the Constitution of SWA.</p>
                <div class="cp-acticle-box ">
                  <p>Read Full Constitution</p>
                   <a target="_blank" href="pdf/swa-constitution.pdf" class="dwn-btn" download><div class="download-box" ><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download Constitution</div></a>
                </div>
              </div>
           </div>
         </div>
       </div>
     </div>
   </div>
   <?php include_once('footer.php'); ?>
 </div>
 <script src="js/jquery-1.11.3.min.js"></script>
 <script src="js/bootstrap.min.js"></script>
 <script src="js/jquery.bxslider.min.js"></script>
 <script src="js/owl.carousel.min.js"></script>
 <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
 <script src="js/jquery.counterup.min.js"></script>
 <script src="js/custom.js"></script>
 <script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
 <script type="text/javascript">
  $(function () {

    var filterList = {

      init: function () {

        // MixItUp plugin
        // http://mixitup.io
        $('#portfoliolist').mixItUp({
          selectors: {
            target: '.portfolio',
            filter: '.filter'
          },
          load: {
            filter: '.exe'
          }
        });

      }

    };

    // Run the show!
    filterList.init();


  });

</script>
</body>
</html>
