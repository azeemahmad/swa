<?php session_start();
//echo "<pre>";print_r($_SESSION);die;
if(isset($_SESSION['is_login']) &&  $_SESSION['is_login'] == 'true')
{
  if($_SESSION['exp_flag'] == 1)
  {
    header('Location: dashboard.php');
  }
  include_once('includes/config.php');
  $sql = "SELECT * FROM fwa_users WHERE reg_no='".$_SESSION['username']."'";
  $result =  mysqli_query($db,$sql);
  ?>

  <!doctype html>
  <html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href=""/>
    <title>Film Writers Association | Change Password</title>
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <link href="css/color.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body class="inner-page">
      <div id="wrapper" class="inside-menu">
       <?php include_once('header.php'); ?>
       <div id="cp-content-wrap" class="page404 cp-login-page">
        <div class="container">
         <div class="row">
          <div class="col-md-8 col-md-offset-2 main-reg-div">
           <div class="cp-reg-box sub-reg-div">
            <div class="col-md-7">
              <div class="cp-acticle-box abt-div cp-contact-form">
                <h4 class="title bold text-center">
                  Change Password
                </h4>
                <form action="javascript:void(0)" id="passwrd_frm">
                  <div class="row">
                    <div class="col-md-12">
                     <input type="password" id="old_pass" name="old_pass" placeholder="Old Password" class="form-control frm-ele">
                   </div>
                   <div class="col-md-12">
                    <input type="password" id="new_pass" name="new_pass" placeholder="New Password" class="form-control frm-ele">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                   <input type="password" id="confirm_pass" name="confirm_pass" placeholder="Confirm Password" class="form-control frm-ele">
                 </div>
               </div>
               <div class="row">
                <div class="col-md-12">
                 <button class="frm-btn" id="login_btn" type="submit">Submit</button>
               </div>
             </div>
             <div class="row">
              <div class="col-md-12">
               <span id="message"></span>
             </div>
           </div>
         </form>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
</div>
<?php include_once('footer.php'); ?>
</div>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/custom.js"></script>
<script>
  $(document).ready(function() {

    $('#login_btn').click(function(e) {
      var old_pass = $('#old_pass').val();
      var new_pass = $('#new_pass').val();
      var confirm_pass = $('#confirm_pass').val();
      $('#message').html('');
      if(old_pass == '' || new_pass == '' || confirm_pass == '' )
      {
        $('#message').html('All fields are Mandatory');
        return false;
      }else{
       $('#message').html('');
     }

     if(new_pass != confirm_pass )
     {
      $('#message').html('Password must match');
      return false;
    }else{
     $('#message').html('');
   }



   var ajax_data = {
    ajax : 3,
    old_pass : old_pass,
    confirm_pass : confirm_pass,
    new_pass : new_pass
  }
  $.ajax({
    type: "POST",
    url: 'process/ajax.php',
    data: ajax_data,
    beforeSend: function() {
     $('#login_btn').attr('disabled',true);
     $('#login_btn').html('Wait');

   },
   success: function(result) {

    $("#passwrd_frm")[0].reset();
    if(result == 1){
      $('#message').html('Password Successfully changed');
      $('#login_btn').attr('disabled',false);
      $('#login_btn').html('Submit');

    }else{
      $('#message').html(result);
      $('#login_btn').attr('disabled',false);
      $('#login_btn').html('Submit');
      return false;
    }

  },
  error: function() {

  }

});

});



  });
</script>
</body>
</html>
<?php }
else{
  header('Location: login.php');
} ?>
