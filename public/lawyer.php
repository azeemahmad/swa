<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href=""/>
  <title>Ask Our Lawyer</title>
  <link href="css/custom.css" rel="stylesheet" type="text/css">
  <link href="css/color.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
  <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>

      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>
    <body class="inner-page">
      <div id="wrapper">
       <?php include_once('header.php'); ?>
       <div id="cp-content-wrap">
        <div class="banner_inner">
          <img src="images/bg.jpg">
        </div>
        <div class="cp_our-story-section">
         <div class="container">
          <div class="row">
           <div class="col-md-12">
            <div class="cp-acticle-box abt-div">
              <h1 class="title bold text-center">
                Ask Our Lawyer
              </h1>
              <p class="text-justify">SWA's full-time Legal Officer is available to answer queries of its members and offer legal advice and guidance for contracts and Copyright related issues. This service is free of charge. SWA Members can also meet our Legal Officer by taking an appointment.<br>(Appointment timings: 3.00 -6.00 PM, Monday to Friday).</p>
              <div class="col-md-8 right_img">
                <h4 class="sub-hd">HEEMA SHIRVAIKAR</h4>
                <h6 class="sub-hd">Legal Officer, SWA</h6>
                <p class="text-justify">Ms. Shirvaikar is a graduate from the Symbiosis Law School, Pune (BBA.L.L.B course). Her area of specialization being Intellectual Property Law and Media & Entertainment Law; she has previously worked at the legal departments of renowned production houses such as Red Chillies Entertainments and Yash Raj Films, as well as the IP departments of law firms like Khaitan and Co., TMT Law Practice, W.S. Kane & Co., and Banana IP Counsels.</p>
              </div>
              <div class="col-md-4 left_img">
                <img class="lawyer-img" src="images/Heema Shirvaikar.jpg">
              </div>
              <div style="clear:both;"></div>

            </div>
          </div>   
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 pd-bt-40 ">
            <h1 class="title bold text-center">Q & A with Legal Officer Heema Shirvaikar</h1><br>
              <span>(In this column, SWA’s Legal Officer addresses some of the recent and most frequent legal queries of SWA members.)</span>
          </div>
          <ul class="cp-extended-gallery gallery">
            <li class="col-md-6">
              <div class="cp-gallery-content ques bg-txt">
                <h3>What are the basic clauses to look out for while signing an agreement/contract with a producer?</h3>
                <ul class="cp-post-meta quest-li">
                  <li><a href="#">15th APRIL 2017</a></li>
                  <li><a href="#">LAW</a></li>
                </ul>
                <div class="col-two">
                <p>
                  <ul class="q-ul">
                    <li><span class="firstcharacter">D</span>o not sign the contract before thoroughly reading and understanding each and every covenant of the agreement. If you are unable to understand any of the clauses in your agreement, consult a legal expert to have it explained. SWA now offers this guidance to its members through its Legal Officer, free of charge.</li>
                    <li>As much as possible, insist on a ‘copyright assignment agreement’ agreement rather than a ‘work-for-hire’ or ‘commissioned work’ agreement. </li>
                    <li>Make sure that the amount of consideration is broken down in various stages/tranches instead of a one-time payment of the bulk amount. This would ensure that if the contract is terminated abruptly, you are at least paid a compensation due to you till the stage of such termination.</li>
                    <li>Section 19(4) of the Copyright Act, 1957 gives you the right to have the rights in your work revert back to you in case of non-use by the producer of such rights within a year. However, such right is waivable through the contract. See if your contract requires you to waive your right under Section 19(4) of the Copyright Act. If it does, ensure that there is at least a clause in your agreement that the producer would have to return your rights to you or that such rights will automatically return to you if not used by the producer for a specific period. This would ensure that if the producer acquires the right in your script and later does not end up making a movie on it, he cannot keep the rights in your script in perpetuity.</li>
                    <li>Termination – make sure that your termination clause is not one-sided and unfair. Ensure that your termination clause address the question of who shall retain the rights of your script upon termination of the contract by either of the parties:<br><br>
                    In case the producer wishes to retain the rights over the script upon termination, ensure that there is a clause to the effect that the rights over the script shall not vest with the producer till the consideration due to you till the stage of termination is paid to you.</li>
                    <li>Credit – Ensure that the credit clause survives termination of your agreement. Even if the agreement is terminated by either of the parties, you shall still be credited for your work.</li>
                    <li>Royalty – Make sure that your contract addresses the percentage of royalty due to you for your copyrighted work. Your royalty is separate from the consideration and any clause which states that royalty is included in the consideration is not valid. Your royalty is and should be separate from your consideration.</li>
                    <li>Make sure that your contract specifically mentions and lists down the particular rights which are being assigned to the producer rather than merely stating ‘all rights.’</li>
                    <li>Union/Guild Jurisdiction – Ensure that the jurisdiction of SWA or any other writer’s union/guild is not excluded from the contract. If there is a dispute at a later date and the writer has waived the right to raise any grievance with or raise the jurisdiction of his or her union/guild, such union including SWA shall not be able to intervene to resolve the dispute on your behalf.</li>
                    <li>These are only the basic clauses that one needs to look out for in the contract. However, other than ensuring the presence of these above clauses, do study your agreement thoroughly for other clauses that might pose a potential danger to you. </li>
                  </ul>
                </p>
                </div>
              </div>
            </li>
            <li class="col-md-6">
              <div class="cp-gallery-content ques bg-txt">
                <h3>The Producer has approved my script and is asking for modifications in my script/screeplay, but we do not have a contract yet. What do I do?</h3>
                <ul class="cp-post-meta quest-li">
                <li><a href="#">15th APRIL 2017</a></li>
                  <li><a href="#">LAW</a></li>
                </ul>
                <div class="col-two">
                <p class="quest-p"><span class="firstcharacter">R</span>equest the producer to enter into an agreement with you for assignment of copyright in your script. Do ensure that the agreement is in conformity with the basic terms and conditions as stated in these FAQs. </p><p class="quest-p">If the producer is unable to enter into a full-fledged agreement at the moment, request the Producer to at least enter into a Memorandum of Understanding (MoU) with you for a copyright assignment/license in your script before you work on any modifications suggested by the producer. An MoU is a preliminary document crystallizing the intention of the parties to enter into a Long Form Agreement at a later date. Please note that <strong>an MoU is not sufficient by itself</strong> and is only an understanding between the parties of the basic terms and conditions of their deal. Do not sign the MoU or any other legal document to that effect without reading and thoroughly understanding it and preferably having it examined by a lawyer. </p>
                </div>
              </div>
            </li>
            <li class="col-md-6">
              <div class="cp-gallery-content ques bg-txt">
                <h3>I have signed a contract with a producer but now I wish to get out of it. Can I do that?</h3>
                <ul class="cp-post-meta quest-li">
                <li><a href="#">15th APRIL 2017</a></li>
                  <li><a href="#">LAW</a></li>
                </ul>
                <div class="col-two">
                <p class="quest-p"><span class="firstcharacter">A</span>n agreement forms a legal relationship between the parties where one party promises to deliver the goods/services (which, in your case, is the script/screenplay) and the other party promises to pay a valuable consideration for the goods/services in return. When such a legal promise is entered into, the breach of such promise (by either parties) would naturally attract a penalty in the form of damages. The party breaching the promise has to compensate the party who suffered a loss (monetary or otherwise). Avoiding a contract or non-performance of a contract amounts to a breach and would entitle the producer for compensation from you for the losses suffered due to your breach. </p><p class="quest-p">The amount for compensation of such losses would vary from case to case and would solely depend upon the contract you have entered into.</p>
                </div>
              </div>
            </li>
            <li class="col-md-6">
              <div class="cp-gallery-content ques bg-txt">
                <h3>I’m a swa member. Can the legal officer draft a contract for me?</h3>
                <ul class="cp-post-meta quest-li">
                <li><a href="#">15th APRIL 2017</a></li>
                  <li><a href="#">LAW</a></li>
                </ul>
                <div class="col-two">
                <p class="quest-p"><span class="firstcharacter">W</span>hile the SWA’s Legal Officer can certainly guide and advise its members on an existing contract, the services of drafting a contract for the writers does not fall in the dominion of the services of Legal Officer. However, we do have a database of competent advocates who can offer these services to you either for free or for a subsidized cost. Drop us an email. </p>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <?php include_once('footer.php'); ?>
</div>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/custom.js"></script>
<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
<script type="text/javascript">
  $(function () {

    var filterList = {

      init: function () {

        // MixItUp plugin
        // http://mixitup.io
        $('#portfoliolist').mixItUp({
          selectors: {
            target: '.portfolio',
            filter: '.filter' 
          },
          load: {
            filter: '.exe'  
          }     
        });               

      }

    };
    
    // Run the show!
    filterList.init();
    
    
  }); 
  
</script>
</body>
</html>