<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href=""/>
  <title>SWA</title>
  <link href="css/custom.css" rel="stylesheet" type="text/css">
  <link href="css/color.css" rel="stylesheet" type="text/css">
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="css/responsive.css" rel="stylesheet" type="text/css">
  <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
  <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>

      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style type="text/css">
        .download-box{
          width: 100%;
          min-height: 84px;
          text-align: center;
          /* display: table; */
        }
      </style>
    </head>
    <body class="inner-page">
      <div id="wrapper">
       <?php include_once('header.php'); ?>
       <div id="cp-content-wrap">
        <div class="banner_inner">
          <img src="images/bg.jpg">
        </div>
        <div class="cp_our-story-section">
         <div class="container">
          <div class="row">
           <div class="col-md-12">
            <div class="cp-acticle-box abt-div">
              <h1 class="title bold text-center">
                Downloads
              </h1>
              <div class="row">

              <div class="col-md-3">
                <div class="cp-acticle-box" id="membership-form">
                <a target="_blank" href="pdf/AGM-Election-2018-Agenda-General-Secretarys-Report-2017-2018.pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;AGM Election Letter, General Secretary's Report 2017-2018</div></a>
                </div>
              </div>

              <div class="col-md-3">
                <div class="cp-acticle-box" id="membership-form">
                <a target="_blank" href="pdf/(R-L)_NOMINATION FORM.pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;R-L NOMINATION FORM</div></a>
                </div>
              </div>

              <div class="col-md-3">
                  <div class="cp-acticle-box" id="membership-form">
                  <a target="_blank" href="pdf/(A)_NOMINATION FORM.pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;A NOMINATION FORM</div></a>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="cp-acticle-box" id="membership-form">
                  <a target="_blank" href="pdf/Constitution of SWA.pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Constitution of SWA</div></a>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="cp-acticle-box" id="membership-form">
                  <a target="_blank" href="pdf/Copyright Amendment Act 2012 (GoI).pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Copyright Amendment Act 2012 (GoI)</div></a>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="cp-acticle-box" id="membership-form">
                  <a target="_blank" href="pdf/Copyright Rules 2013 (GoI).pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Copyright Rules 2013 (GoI)</div></a>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="cp-acticle-box" id="membership-form">
                  <a target="_blank" href="pdf/ISC 4 Magazine.pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;ISC 4 Magazine</div></a>
                  </div>
                </div>


                <div class="col-md-3">
                  <div class="cp-acticle-box" id="membership-form">
                  <a target="_blank" href="pdf/Minimum Rate Card 2017 (recommended by SWA) .pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Minimum Rate Card 2017 (recommended by SWA)</div></a>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="cp-acticle-box" id="membership-form">
                  <a target="_blank" href="pdf/Model NDA (recommended by SWA).pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Model NDA (recommended by SWA)</div></a>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="cp-acticle-box" id="membership-form">
                  <a target="_blank" href="pdf/Proposed Film MBC.pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Proposed Film MBC</div></a>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="cp-acticle-box" id="membership-form">
                  <a target="_blank" href="pdf/Proposed Lyricist MBC.pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Proposed Lyricist MBC</div></a>
                  </div>
                </div>


                <div class="col-md-3">
                  <div class="cp-acticle-box" id="membership-form">
                  <a target="_blank" href="pdf/Proposed TV MBC.pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;Proposed TV MBC</div></a>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="cp-acticle-box" id="membership-form">
                  <a target="_blank" href="pdf/SWA DSC Complaint Form.pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;SWA DSC Complaint Form</div></a>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="cp-acticle-box" id="membership-form">
                  <a target="_blank" href="pdf/SWA DSC Undertaking Form.pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;SWA DSC Undertaking Form</div></a>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="cp-acticle-box" id="membership-form">
                  <a target="_blank" href="pdf/SWA Membership Form.pdf" class="dwn-btn" download=""><div class="download-box"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;SWA Membership Form</div></a>
                  </div>
                </div>

              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include_once('footer.php'); ?>
</div>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/custom.js"></script>
<!-- <script type="text/javascript" src="js/jquery.mixitup.min.js"></script> -->
<!-- <script type="text/javascript">
  $(function () {

    var filterList = {

      init: function () {

        // MixItUp plugin
        // http://mixitup.io
        $('#portfoliolist').mixItUp({
          selectors: {
            target: '.portfolio',
            filter: '.filter'
          },
          load: {
            filter: '.exe'
          }
        });

      }

    };

    // Run the show!
    filterList.init();


  });

</script> -->
</body>
</html>
