<?php session_start();
// echo "<pre>";print_r($_SESSION);die;
date_default_timezone_set('Asia/Calcutta');

if(isset($_SESSION['is_login']) &&  $_SESSION['is_login'] == 'true')
{
  include_once('includes/config.php');
  ?>

  <!doctype html>
  <html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href=""/>
    <title>Film Writers Association | My Creations</title>
    <link href="css/custom.css" rel="stylesheet" type="text/css">
    <link href="css/color.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/> -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    .input-ed{
      width:100%;
      border: #dddddd 1px solid;
    }
    .input-trns{
      background: transparent;
      border: none;
      padding: 0px 3px;
    }
    .input-ed::-webkit-inner-spin-button,
    .input-ed::-webkit-outer-spin-button {
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
      margin: 0;
    }
.mem{margin-left: 35px;}
.nerror{border: 1px solid red!important;}
</style>
</head>
<body class="inner-page">
  <div id="wrapper" class="inside-menu">
    <?php include_once('header.php');
      // echo '<pre>';
      // var_dump($_SESSION);die;
      if(isset($_SESSION['t_row_id']))
      {
        unset($_SESSION['t_row_id']);
      }

      if(!empty($_POST['reg_no']))
      {
        $reg_no = $_POST['reg_no'];
      }
      else {
        $reg_no = '';
      }

      if(!empty($_POST['current_exp_date']))
      {
        $current_exp_date = $_POST['current_exp_date'];
      }
      else {
        $current_exp_date = '';
      }

      if(!empty($_POST['renew_period']))
      {
        $renew_period = $_POST['renew_period'];
      }
      else {
        $renew_period = '';
      }

      if(!empty($_POST['total_mem_renew_charge']))
      {
        $total_mem_renew_charge = $_POST['total_mem_renew_charge'];
      }
      else {
        $total_mem_renew_charge = '';
      }

      if(!empty($_POST['new_exp_date']))
      {
        $new_exp_date = $_POST['new_exp_date'];
      }
      else {
        $new_exp_date = '';
      }

      if(!empty($_POST['mem_renew_charge']))
      {
        $mem_renew_charge = $_POST['mem_renew_charge'];
      }
      else {
        $mem_renew_charge = '';
      }

      if(!empty($_POST['renew_period']))
      {
        $duration_select = $_POST['renew_period'];
      }
      else {
        $duration_select = '';
      }

      if(!empty($_POST['mem_renew_penelty']))
      {
        $mem_renew_penelty = $_POST['mem_renew_penelty'];
      }
      else {
        $mem_renew_penelty = '0';
      }

      if(!empty($_POST['new_card']))
      {
        $new_card = 1;
      }
      else {
        $new_card = 0;
      }

      if(!empty($_POST['new_card_charges']))
      {
        $new_card_charges = $_POST['new_card_charges'];
      }
      else {
        $new_card_charges = 0;
      }

      if(!empty($_POST['new_card_post']))
      {
        $new_card_post = 1;
      }
      else {
        $new_card_post = 0;
      }

      if(!empty($_POST['new_card_postage']))
      {
        $new_card_postage = $_POST['new_card_postage'];
      }
      else {
        $new_card_postage = 0;
      }

      if(!empty($_POST['grand_total']))
      {
        $grand_total = $_POST['grand_total'];
      }
      else {
        $grand_total = '';
      }

      if(!empty($_POST['monthly_pay']))
      {
        $monthly_pay = $_POST['monthly_pay'];
      }
      else {
        $monthly_pay = 0;
      }

      if($monthly_pay == 0)
      {
        $renew_time = $duration_select .' Year(s)';
      }
      else {
        $renew_time = $duration_select .' Month(s)';
      }

      // if(!isset($_SESSION['order_id']))
      // {
        $order_id = 'RN_'.$_SESSION['username'].'_'.date("j_n_Y_H_i_s");
        $_SESSION['order_id'] = $order_id;
      // }
      // print_r($_SESSION['order_id']);
      // var_dump($new_card_post);die;

    ?>
    <div id="cp-content-wrap" class="page404 cp-login-page">
      <div class="container">
        <div class="row">
          <div class="col-md-12 main-reg-div">
            <div class="cp-reg-box sub-reg-div">
              <h4>Membership Renewal Confirm</h4>
              <form method="post" name="customerData" action="ccavRequestHandlerif_renew.php">
                <table class='table table-hover'>
                  <tr>
                    <th>Membership No.</th>
                    <td><input value="<?php echo $reg_no; ?>" class="input-trns" name="reg_no" readonly></td>
                  </tr>
                  <tr>
                    <th>Membership expiry date (Current data)</th>
                    <td><input name="current_exp_date"  class="input-trns" id="current_exp_date" value="<?php echo $current_exp_date ;?>" readonly/>
                    </td>
                  </tr>

                  <tr>
                    <th>Annual Membership renew charge</th>
                    <td>
                      <label>Rs. </label>&nbsp;<input name="mem_renew_charge"  class="input-trns" id="mem_renew_charge" value="<?php echo $mem_renew_charge; ?>" readonly>
                    </td>
                  </tr>
                  <tr>
                    <th>Renew Membership Time</th>
                    <td>
                    <input name="renew_period"  class="input-trns" id="duration_select" value="<?php echo $renew_time; ?>" readonly>
                    </td>
                  </tr>
                  <tr>
                    <th>Total Membership renew charge</th>
                    <td>
                      <label>Rs. </label>&nbsp;<input name="total_mem_renew_charge"  id="total_mem_renew_charge"  class="input-trns" value="<?php echo $total_mem_renew_charge; ?>" readonly>
                    </td>
                  </tr>
                  <tr>
                    <th>New membership expiry date</th>
                    <td><input name="new_exp_date" id="new_exp_date" value="<?php echo $new_exp_date; ?>" class="input-trns" readonly>
                    </td>
                  </tr>
                  <tr>
                    <th>Membership renew late penelty (if any)</th>
                    <td><label>Rs. </label>&nbsp;<input name="mem_renew_penelty" id="mem_renew_penelty" class="input-trns" value="<?php echo $mem_renew_penelty; ?>" readonly>
                    </td>
                  </tr>
                  <tr>
                    <th>New membership card charges</th>
                    <td><label>Rs. </label>&nbsp;<input name="new_card_charges" id="new_card_charges" class="input-trns" value="<?php echo $new_card_charges; ?>" readonly>
                      <input type="hidden" name="new_card_status" id="new_card_status" value="<?php echo $new_card; ?>" readonly>
                    </td>
                  </tr>
                  <tr>
                    <th>New membership card postage charges</th>
                    <td><label>Rs. </label>&nbsp;<input name="new_card_postage" id="new_card_postage" class="input-trns" value="<?php echo $new_card_postage; ?>" readonly>
                      <input type="hidden" name="new_card_postage_status" id="new_card_postage_status" value="<?php echo $new_card_post; ?>" readonly>
                    </td>
                  </tr>
                  <tr>
                    <th>Total Amount Payable</th>
                    <td><label>Rs. </label>&nbsp;<input name="grand_total" id="grand_total" class="input-trns" value="<?php echo $grand_total; ?>" readonly>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" class="text-center">
                    									<input type="hidden" name="tid" id="tid" readonly="">
                    									<input type="hidden" name="merchant_id" value="2923">
                    									<input type="hidden" name="order_id" value="<?php echo $_SESSION['order_id']; ?>">
                    									<input type="hidden" name="amount" value="<?php echo $grand_total; ?>">
                    									<input type="hidden" name="currency" value="INR">
                    									<input type="hidden" name="redirect_url" value="http://swaindia.org/ccavResponseHandlerif_renew.php">
                    									<input type="hidden" name="cancel_url" value="http://swaindia.org/ccavResponseHandlerif_renew.php">
                    									<input type="hidden" name="language" value="EN">
                    									<input type="hidden" name="billing_address" value="Andheri (West)">
                    									<input type="hidden" name="billing_city" value="Mumbai">
                    									<input type="hidden" name="billing_state" value="MS">
                    									<input type="hidden" name="billing_zip" value="400053">
                    									<input type="hidden" name="billing_country" value="India">
                    									<input type="hidden" name="billing_tel" value="02226733027">
                    									<input type="hidden" name="billing_email" value="<?php echo $_SESSION['User_Email'] ;?>"/>
                    									<input type="hidden" name="billing_name" value="<?php echo $_SESSION['name'];?>"/>
                    									<input type="hidden" name="delivery_address" value="Andheri (West)">
                    									<input type="hidden" name="delivery_city" value="Mumbai">
                    									<input type="hidden" name="delivery_state" value="MH">
                    									<input type="hidden" name="delivery_zip" value="400053">
                    									<input type="hidden" name="delivery_country" value="India">
                    									<input type="hidden" name="delivery_tel" value="123456">
                    									<input type="hidden" name="merchant_param1" value="<?php echo $current_exp_date.'#'.$new_exp_date; ?>">
                    									<input type="hidden" name="merchant_param2" value="<?php echo $renew_time; ?>">
                    									<input type="hidden" name="merchant_param3" value="<?php echo $mem_renew_penelty; ?>">
                    									<input type="hidden" name="merchant_param4" value="<?php echo $new_card_charges.'#'.$new_card_postage; ?>">
                    									<input type="hidden" name="merchant_param5" value="<?php echo $reg_no; ?>">
                    									<input type="hidden" name="promo_code" value="">
                    									<input type="hidden" name="customer_identifier" value="">
                    									<input type="hidden" name="integration_type" value="iframe_normal">
                    									<input type="submit" class="btn btn-default proced-btn" value="Proceed to Payment">
                    							</td>
                  </tr>
                </table>
                </form>
                <div id="data_msg"></div>

              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include_once('footer.php'); ?>
    </div>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <!-- <script src="js/jquery.prettyPhoto.js"></script> -->
    <script src="js/custom.js"></script>
  </body>
  </html>
<?php }
else{
  header('Location: login.php');
} ?>
