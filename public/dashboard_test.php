<?php session_start();
   if(isset($_SESSION['is_login']) &&  $_SESSION['is_login'] == 'true')
   {
       include_once('includes/config.php');
       $sql_check = "SELECT * FROM `fwa_users` WHERE `reg_no` = '".$_SESSION['username']."'";
       $result_check = mysqli_query($db,$sql_check);
       $row_check = mysqli_fetch_assoc($result_check);

 ?>
<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href=""/>
      <title>Dashboard</title>
      <link href="css/custom.css" rel="stylesheet" type="text/css">
      <link href="css/color.css" rel="stylesheet" type="text/css">
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="css/responsive.css" rel="stylesheet" type="text/css">
      <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
      <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
      <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style>
      .sml-fnt{
        font-size: 11px;
      }
      .disab{
          background: rgba(84, 84, 84, 0.7)!important;
          border: none!important;
      }
      </style>
   </head>
   <body class="inner-page">
     <?php //var_dump($_SESSION); ?>
      <div id="wrapper">
         <?php include_once('header.php'); ?>
         <div id="cp-content-wrap" class="page404 cp-login-page">
            <div class="container">
              <div class="row writer-div">
                <div class="col-md-8 col-md-offset-2">
                  <?php if($_SESSION['type_id'] != 1 && $_SESSION['type_id'] != 4 && $_SESSION['type_id'] != 7) {
                          if($_SESSION['exp_flag'] == 2) { ?>
                  <h3 class="writer-txt label label-warning">
                    Hi <?php echo $_SESSION['name']; ?>, your SWA membership will expire soon on <?php echo $_SESSION['exp_date']; ?>
                  </h3>
                <?php } else  if($_SESSION['exp_flag'] == 1){ ?>
                  <h3 class="writer-txt label label-danger">
                    Hi <?php echo $_SESSION['name']; ?>, your SWA membership has expired on <?php echo $_SESSION['exp_date']; ?>
                  </h3>
                <?php } } ?>

                <?php if(isset($_SESSION['renew']['msg'])) { ?>
                  <h3 class="writer-txt label label-info">
                    <?php echo $_SESSION['renew']['msg']; ?>
                  </h3>
                <?php } ?>

                <?php if(isset($_SESSION['evote']['msg'])) { ?>
                  <h3 class="writer-txt label label-info">
                    <?php echo $_SESSION['evote']['msg'];
                            unset($_SESSION['evote']['msg']);
                    ?>
                  </h3>
                <?php } ?>

                </div>
              </div>
               <div class="row">
                  <div class="col-md-3">
                     <div class="box-div" onclick='window.location = "register_script.php"'><span class="icn"><i class="fa fa-registered" aria-hidden="true"></i></span>Register Your Work</div>
                  </div>
                  <div class="col-md-3">
                     <div class="box-div" onclick='window.location = "mycreation.php"'><span class="icn"><i class="fa fa-book" aria-hidden="true"></i></span>Your Registered Work</div>
                  </div>
                   <div class="col-md-3">
                     <div class="box-div" onclick='window.location = "details.php"'><span class="icn"><i class="fa fa-comments-o" aria-hidden="true"></i></span>Your Details</div>
                  </div>
                   <!-- <div class="col-md-3">
                     <div class="box-div"><span class="icn"><i class="fa fa-comments-o" aria-hidden="true"></i></span>Update Your Details <small>(Coming Soon)</small></div>
                  </div> -->
                   <div class="col-md-3">
                     <div class="box-div sml-fnt" onclick='window.location = "renew.php"'><span class="icn"><i class="fa fa-refresh" aria-hidden="true"></i></span>Renew Your Membership</div>
                  </div>
                  <div class="col-md-3">
                     <div class="box-div" onclick='window.location = "changepass.php"'><span class="icn"><i class="fa fa-key" aria-hidden="true"></i></span>Change Password</div>
                  </div>

                  <div class="col-md-3">
                     <div class="box-div" onclick='window.location = "contact.php"'><span class="icn"><i class="fa fa-key" aria-hidden="true"></i></span>Technical Support</div>
                  </div>
                  <?php if( in_array( $_SESSION['type_id'] , [1,4,5,7,8] )  && time() < strtotime($evoting_disable_time)  ) {   //if member of type regular or associate  and time is before 2 ?>
                  <div class="col-md-3">
                     <?php if($row_check['evote_flag'] == 0 ) { ?>
                     <div class="box-div" onclick='window.location = "register_evote1.php"'><span class="icn"><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                         Register for e-Voting
                     </div>
                    <?php } else { ?>

                        <div class="disab box-div"><span class="icn"><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                            Register for e-Voting
                        </div>

                    <?php } ?>
                  </div>
                <?php } ?>

               </div>
            </div>
         </div>

         <?php include_once('footer.php'); ?>
      </div>
      <script src="js/jquery-1.11.3.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/jquery.bxslider.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/custom.js"></script>
   </body>
</html>
<?php
}
else
{
   header('Location: login.php');
}
?>
