<?php session_start();
      if(isset($_SESSION['is_login']) &&  $_SESSION['is_login'] == 'true')
      {
         header('Location: dashboard.php');
      }
      else{
?>
<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/x-icon" href=""/>
      <title>Reset Password</title>
      <link href="css/custom.css" rel="stylesheet" type="text/css">
      <link href="css/color.css" rel="stylesheet" type="text/css">
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="css/responsive.css" rel="stylesheet" type="text/css">
      <link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
      <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
      <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style>
      .page404, .comming-soon {
          background: url(images/loginbanner.jpg) no-repeat top center;
          background-size: cover;
          padding: 60px 0;
          width: 100%;
          float: left;
          text-align: center;
      }
      .cp-login-box, .cp-reg-box {
    margin: 0 auto;
    background: #fff;
    border-radius: 2px;
    border: 1px solid #eeeeee;
    text-align: left;
    padding: 50px;
}
.rule-list{
   color:#fff;
}
      </style>
   </head>
   <body class="inner-page">
      <div id="wrapper">
         <?php include_once('header.php'); ?>
         <div id="cp-content-wrap" class="page404 cp-login-page">
            <div class="container">
               <div class="row">
                  <div class="col-md-6 col-md-offset-3 main-login-div">
                     <div class="cp-login-box sub-login-div">
                        <form action="javascript:void(0)">
                           <ul>
                              <li>
                                 <input type="text" id="username" class="form-control" placeholder="Membership Number">
                              </li>
                              <li>
                                 <input type="text" id="email" class="form-control" placeholder="Email Id">
                              </li>
                              <li class="cp-login-buttons">
                                 <button id="reset_btn" type="submit" style="background: #050505;">Reset Password</button>
                              </li>
                              <li id="reset_message"></li>
                           </ul>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <?php include_once('footer.php'); ?>
      </div>
      <script src="js/jquery-1.11.3.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/jquery.bxslider.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="js/jquery.prettyPhoto.js"></script>
      <script src="js/custom.js"></script>
   </body>
</html>
<?php } ?>