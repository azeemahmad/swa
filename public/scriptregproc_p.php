<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
// ******************************************************************************************
//
if (!isset($_SESSION))
session_start();
include("includes/config.php");

// echo '<pre>';
// var_dump($_REQUEST);
// var_dump($_FILES);
// echo '</pre>';
// die;
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/x-icon" href=""/>
	<title> Instruction</title>
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<link href="css/color.css" rel="stylesheet" type="text/css">
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="css/responsive.css" rel="stylesheet" type="text/css">
	<link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen"/>
	<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Domine:400,700' rel='stylesheet' type='text/css'>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="inner-page">
	<div id="wrapper" class="inside-menu">
		<?php include_once('header.php'); ?>
		<div id="cp-content-wrap" class="page404 cp-login-page">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 main-reg-div">
						<div class="cp-reg-box sub-reg-div">
							<h4>Online Script Registration</h4>
							<?php
							function generateRandomsesidString($length = 25)
							{
								return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
							}

							function getRegCount()
							{
								//finding regno entries
								include("includes/config.php");
								$sql1= "select * from scr_reg where reg_no = '".$_SESSION['username']."' and `online_flag` = 1";
								$result1 = mysqli_query($db,$sql1);
								if($result1 == false)
								{
									$reg_ord = 1;
								}
								else
								{
									$row1 = mysqli_fetch_array($result1);
									$reg_ord=mysqli_num_rows($result1)+1;
								}
								return $reg_ord;
							}
							// Echo the random string.
							// Optionally, you can give it a desired string length up to 62 characters.
							$ses_id = generateRandomsesidString();
							$pagecount_arr = array();
							$msg = "";
							if($_SESSION['is_login'] != "true" or $_SESSION['username'] == "")
							{
								echo " <p class='content' align='center'>This service is only for registered members. Please <a href='login.php'>log in</a> to view this page</p>";
							}
							else if($_SESSION['exp_flag'] == 1)
							{
								header('Location: dashboard.php');
							}
							else
							{
								$_SESSION['stype'] = $_POST['rtsstype'];
								$_SESSION['title'] = $_POST['rtstitle'];
								// $_SESSION['dcl']=  $_POST['dcl'];
								$reg_ord = getRegCount();
								// var_dump($reg_ord);die;
								$file_count = sizeof($_REQUEST['rtsstype']);

								// $_SESSION['file_count'] = $file_count;
								// var_dump( $_SESSION['file_count']);//die;
								define ("filesplace","files/".$ses_id."/");
								$amt = 0;
								$total_pages=0;
								$arr_order_id = array();
								$arr_title = $_SESSION['title'];

								for($i = 0;$i < $file_count ; $i++)
								{
									$reg_ord = $reg_ord+$i;
									$order_id = $_SESSION['username']."-".$reg_ord ;
									array_push($arr_order_id, $order_id);
									//storing file and finding pages and amt

									$fs="files/".$ses_id;
									if (!file_exists($fs))
									{
										mkdir($fs);
									}

									if(is_uploaded_file($_FILES['pdffile']['tmp_name'][$i]))
									{///is upload

										if ($_FILES['pdffile']['type'][$i] != "application/pdf")
										{
											echo "<p>Script must be uploaded in PDF format.</p>";
										}
										else
										{ //if pdf
											$date= date("d-m-Y");
											$temp_file = rand();
											$name = $order_id;
											$result = move_uploaded_file($_FILES['pdffile']['tmp_name'][$i], filesplace."/$temp_file.pdf");
											// $result = move_uploaded_file($_FILES['pdffile']['tmp_name'][$i], filesplace."/$name.pdf");
											if ($result == 1)
											{ //if moved
												$result_gs = exec('gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dBATCH -sOutputFile='.filesplace.'/'.$name.'.pdf'.'  '.filesplace.'/'.$temp_file.'.pdf');
												// var_dump($result_gs);die;
												//Page
												$pdf_success = preg_match("|Page [0-9]+|i ",$result_gs);
												if($pdf_success == 0)
												{
													// echo "PDF Error: Sorry, but your PDF file is not compatible with our software. Please, try again with a different PDF file OR choose a different method to convert your document to PDF.";
													echo "PDF Error: Sorry, but your PDF file is not compatible with our software. Please, use one of the following websites to convert your word document to PDF<br><br> <a target='_blank' href='https://pdfcandy.com/word-to-pdf.html'>https://pdfcandy.com/word-to-pdf.html</a> <br> <a target='_blank' href='http://topdf.com/'>http://topdf.com/</a> <br> <a target='_blank' href='https://online2pdf.com/doc-to-pdf'>https://online2pdf.com/doc-to-pdf</a>
													<br><br><a href='register_script.php'>Back to Script Registration Page.</a>";
													exit;
												}

												// filesplace."/$name.pdf"
												require_once('fpdf/fpdf.php');
												require_once('fpdi/fpdi.php');
												$pdf = new FPDI();
												//Set the source PDF file
												$pagecount = $pdf->setSourceFile(filesplace."/$name.pdf");
												$total_pages = $total_pages + $pagecount;
												$title = $arr_title[$i];
												$pagecount_arr[$title] = $pagecount;
											}
											else
											{
												echo "<p>Sorry, Error happened while uploading . </p>";
											} //if moved
										}
									}
									else
									{
										echo 'File uploading Failed..!!';
										exit();
									}
									if( $pagecount <=1)
									{
										$amt = $amt + 40.00;
									}
									else
									{
										$amt = $amt + 40.00 +(($pagecount -1 )*2 );
									}
								}
								$amt = $amt + 5; // welfare fund
								/*	echo "Name              : ".$_SESSION['name']."<br>";
								echo "No of Pages       : ".$pagecount."<br>";
								echo "Registration Fees : ".$amt."<br>"; */
								?>

								<table class="table table-hover">
									<tbody>
										<tr>
											<td scope="row">Title</td>
											<td scope="row">Pages</td>
											<td scope="row">Cost</td>
										</tr>
										<?php foreach ($pagecount_arr as $titl => $pagecnt) { ?>
											<tr>
												<td scope="row"><?php echo $titl; ?></td>
												<td scope="row"><?php echo $pagecnt; ?></td>
												<td scope="row"><?php echo 40.00 + (($pagecnt-1) * 2) ?> INR</td>
											</tr>
										<?php } ?>
											<tr>
												<td scope="row">WELFARE FUND</td>
												<td scope="row">-</td>
												<td scope="row">5 INR</td>
											</tr>
											<tr>
												<td scope="row">Total</td>
												<td scope="row">-</td>
												<td scope="row"><?php echo $amt.' INR'; ?></td>
											</tr>

										<!-- <tr>
											<th scope="row">Author Name</th>
											<td><?php // echo  $_SESSION['name']; ?></td>
										</tr> -->
										<!-- <tr>
											<th scope="row">No of PDF(s)</th>
											<td><?php // echo $file_count; ?></td>
										</tr>
										<tr>
											<th scope="row">No of Pages</th>
											<td><?php // echo $total_pages; ?></td>
										</tr>
										<tr>
											<th scope="row">Registration Fees</th>
											<td><?php // echo $amt.' INR'; ?></td>
										</tr> -->
									</tbody>
								</table>

								<?php

								$_SESSION['pagecount'] =$pagecount;
								$_SESSION['amt'] =$amt;
								$_SESSION['orderid']= $order_id;
								$_SESSION['dcl'] = '123456'; // unknow data
								$stypes_csv = implode('#', $_SESSION['stype']);
								$order_id_csv = implode('#', $arr_order_id);
								$title_csv = implode('#', $_SESSION['title']);
								?>
								<form method="post" name="customerData" action="ccavRequestHandlerif_p.php">
									<input type="hidden" name="tid" id="tid" readonly />
									<input type="hidden" name="merchant_id" value="2923"/>
									<input type="hidden" name="order_id" value="<?php echo $order_id_csv ;?>"/>
									<input type="hidden" name="amount" value="<?php echo $amt ;?>"/>
									<input type="hidden" name="currency" value="INR"/>
									<input type="hidden" name="redirect_url" value="http://swaindia.org/ccavResponseHandlerif_p.php"/>
									<input type="hidden" name="cancel_url" value="http://swaindia.org/ccavResponseHandlerif_p.php"/>
									<input type="hidden" name="language" value="EN"/>
									<input type="hidden" name="billing_address" value="Andheri (West)"/>
									<input type="hidden" name="billing_city" value="Mumbai"/>
									<input type="hidden" name="billing_state" value="MS"/>
									<input type="hidden" name="billing_zip" value="400053"/>
									<input type="hidden" name="billing_country" value="India"/>
									<input type="hidden" name="billing_tel" value="02226733027"/>
									<input type="hidden" name="billing_email" value="<?php echo $_SESSION['User_Email'] ;?>"/>
									<input type="hidden" name="billing_name" value="<?php echo $_SESSION['name'];?>"/>
									<input type="hidden" name="delivery_address" value="Andheri (West)"/>
									<input type="hidden" name="delivery_city" value="Mumbai"/>
									<input type="hidden" name="delivery_state" value="MH"/>
									<input type="hidden" name="delivery_zip" value="400053"/>
									<input type="hidden" name="delivery_country" value="India"/>
									<input type="hidden" name="delivery_tel" value="<?php echo $_SESSION['dcl']?>">
									<input type="hidden" name="merchant_param1" value="<?php echo $stypes_csv?> "/>
									<input type="hidden" name="merchant_param2" value="<?php echo $_SESSION['username'];?>"/>
									<input type="hidden" name="merchant_param3" value="<?php echo $title_csv;?>"/>
									<input type="hidden" name="merchant_param4" value="<?php echo $ses_id;?>"/>
									<input type="hidden" name="merchant_param5" value="<?php echo $order_id_csv;?>"/>
									<input type="hidden" name="promo_code" value=""/>
									<input type="hidden" name="customer_identifier" value=""/>
									<input type="hidden" name="integration_type" value="iframe_normal"/>
									<input type="submit" class="btn btn-default proced-btn" value="Proceed to Payment">
								</table>
							</form>


						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include_once('footer.php'); ?>
	</div>
	<script src="js/jquery-1.11.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.bxslider.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>
<?php
}

?>
